alter table demographic
    add genderId int null;

alter table demographic
    add pronoun varchar(25) null;

alter table demographic
    add pronounId int null;

alter table demographic
    add gender varchar(25) null;

alter table demographic
    add middleNames varchar(25) null;