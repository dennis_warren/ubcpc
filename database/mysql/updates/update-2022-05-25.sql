CREATE TABLE IF NOT EXISTS `drugTherapyProblemEvent` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `event` varchar(255),
    PRIMARY KEY (`id`)
);

ALTER TABLE `DrugTherapyProblem` ADD COLUMN event_id INT(11) AFTER issue_id;

ALTER TABLE `DrugTherapyProblem`
    ADD CONSTRAINT `fk_event` FOREIGN KEY (`event_id`) REFERENCES `drugTherapyProblemEvent` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE ctl_drugTherapyProblemPharmacistAction  (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `dtp_id` int(11) NULL,
      `dtp_action_id` int(11) NULL,
      PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemStatus`(`id`, `name`, `numericValue`, `active`) VALUES (5, 'New', NULL, b'1');

-- Move all previous pharmacist action values into new table.
INSERT INTO ctl_drugTherapyProblemPharmacistAction (`dtp_id`,`dtp_action_id`)
SELECT dtp.id, dtp.pharmacist_action_id
FROM DrugTherapyProblem dtp
WHERE pharmacist_action_id IS NOT NULL;