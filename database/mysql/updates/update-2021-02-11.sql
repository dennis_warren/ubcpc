  alter table appointment add column referralSource varchar(80);
  alter table appointment add column referralSourceCode int(11);
  alter table appointmentArchive add column referralSource varchar(80);
  alter table appointmentArchive add column referralSourceCode int(11);
  update appointment set referralSourceCode = 0 where referralSourceCode is null;
  
  INSERT INTO `LookupList`(`name`, `listTitle`, `description`, `categoryId`, `active`, `createdBy`, `dateCreated`) VALUES ('appointmentReferralSource', 'Appointment Referral Source', 'A select list of referral sources for a specific appointment. ', NULL, 1, 'oscar', '2021-02-11 19:49:52');