INSERT INTO `LookupList`(`name`, `listTitle`, `description`, `categoryId`, `active`, `createdBy`, `dateCreated`) VALUES ('appointmentLocationCode', 'Appointment Locations', 'Appointment location list displayed in the add/edit appointment dialog', NULL, 1, 'oscar', '2020-07-10 12:41:04');
ALTER TABLE appointment MODIFY location VARCHAR(80);
ALTER TABLE `appointment` ADD COLUMN `locationCode` int(11);
UPDATE appointment SET locationCode = 0 WHERE locationCode IS NULL;

INSERT INTO `LookupList`(`name`, `listTitle`, `description`, `categoryId`, `active`, `createdBy`, `dateCreated`) VALUES ('appointmentServiceMode', 'Appointment Service Mode', 'Select list of the service modes of an appointment. ie: telephone, in-person, video', NULL, 1, 'oscar', '2020-07-12 12:41:04');
ALTER TABLE `appointment` ADD COLUMN `serviceMode` int(11);
UPDATE appointment SET serviceMode = 0 WHERE serviceMode IS NULL;

ALTER TABLE `appointmentArchive` 
ADD COLUMN `reasonCode` int(11) NULL AFTER `programType`,
ADD COLUMN `locationCode` int(11) NULL AFTER `reasonCode`,
ADD COLUMN `serviceMode` int(11) NULL AFTER `locationCode`;

ALTER TABLE `LookupListItem` 
ADD COLUMN `icon` varchar(255) NULL AFTER `label`,
ADD COLUMN `colour` varchar(6) NULL AFTER `icon`;

INSERT INTO `LookupList`(`name`, `listTitle`, `description`, `categoryId`, `active`, `createdBy`, `dateCreated`) VALUES ('demographicReferralSource', 'Demographic Referral Source', 'Select list of potential sources from where a new patient can be referred from', NULL, 1, 'oscar', '2020-07-12 12:41:04');
ALTER TABLE `demographic` ADD COLUMN `referralSourceCode` int(11);
