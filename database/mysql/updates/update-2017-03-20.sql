
CREATE TABLE `lu_drugTherapyProblemIssue` (
  `id` int(11) NOT NULL,
  `name` varchar(50) ,
  `numericValue` int(5) ,
  `description` varchar(125) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemIssue` VALUES ('1', 'Unnecessary drug', null, null, b'1'), ('2', 'Needs additional drug', null, null, b'1'), ('3', 'Adverse drug reaction', null, null, b'1'), ('4', 'Non compliance', null, null, b'1'), ('5', 'Different drug needed', null, null, b'1'), ('6', 'Dose too high', null, null, b'1'), ('7', 'Dose too low', null, null, b'1');

CREATE TABLE `lu_drugTherapyProblemProbability` (
  `id` int(11) NOT NULL,
  `name` varchar(50) ,
  `numericValue` int(5) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemProbability` VALUES ('0', null, null, b'1'), ('1', 'rare', null, b'1'), ('2', 'unlikely', null, b'1'), ('3', 'possible', null, b'1'), ('4', 'likely', null, b'1'), ('5', 'almost certain', null, b'1');

CREATE TABLE `lu_drugTherapyProblemSeverity` (
  `id` int(11) NOT NULL,
  `name` varchar(50) ,
  `numericValue` int(5) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemSeverity` VALUES ('0', null, null, b'1'), ('1', 'insignificant', null, b'1'), ('2', 'minor', null, b'1'), ('3', 'moderate', null, b'1'), ('4', 'major', null, b'1'), ('5', 'catastrophic', null, b'1');

CREATE TABLE `lu_drugTherapyProblemPriority` (
  `id` int(11) NOT NULL,
  `name` varchar(50) ,
  `numericValue` int(5) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemPriority` VALUES ('0', null, null, b'1'), ('1', 'High', null, b'1'), ('2', 'Low', null, b'1');

CREATE TABLE `lu_drugTherapyProblemPharmacistAction` (
  `id` int(11) NOT NULL,
  `name` varchar(50) ,
  `numericValue` int(5) ,
  `description` varchar(125) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemPharmacistAction` VALUES ('0', null, null, null, b'1'), ('1', 'Eliminate patient barrier', null, null, b'1'), ('2', 'Initiate monitoring', null, null, b'1'), ('3', 'Refer to MD', null, null, b'1'), ('4', 'Refer to other', null, null, b'1'), ('5', 'Change prescription', null, null, b'1'), ('6', 'Stop prescription', null, null, b'1'), ('7', 'Start prescription', null, null, b'1'), ('8', 'Change non-prescription', null, null, b'1'), ('9', 'Stop non-prescription', null, null, b'1'), ('10', 'Start non-prescription', null, null, b'1');

CREATE TABLE `lu_drugTherapyProblemStatus` (
  `id` int(11) NOT NULL,
  `name` varchar(15) ,
  `numericValue` int(5) ,
  `active` bit(1) ,
  PRIMARY KEY (`id`)
);

INSERT INTO `lu_drugTherapyProblemStatus` VALUES ('0', null, null, b'1'), ('1', 'Resolved', null, b'1'), ('2', 'Improved', null, b'1'), ('3', 'Worsened', null, b'1'), ('4', 'Unchanged', null, b'1');

CREATE TABLE `drugTherapyProblemComment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  PRIMARY KEY (`id`)
);

INSERT INTO `drugTherapyProblemComment` VALUES ('0', '');

CREATE TABLE `DrugTherapyProblem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_id` char(36) NOT NULL,
  `demographic_no` int(11) NOT NULL,
  `comment_id` int(11) ,
  `issue_id` int(11) NOT NULL,
  `drug_id` int(11) DEFAULT NULL,
  `DIN` varchar(20) ,
  `priority_id` int(2) ,
  `severity_id` int(2) ,
  `pharmacist_action_id` int(2) ,
  `probability_id` int(2) ,
  `status_id` int(2) ,
  `date_entered` date,
  `date_identified` datetime ,
  `date_updated` datetime ,
  `date_response` datetime ,
  PRIMARY KEY (`id`),
  KEY `drugTherapyProblem_id` (`issue_id`),
  KEY `status_id` (`status_id`),
  KEY `priority_id` (`priority_id`),
  KEY `probability_id` (`probability_id`),
  KEY `pharmacist_action_id` (`pharmacist_action_id`),
  KEY `severity_id` (`severity_id`),
  KEY `drug_id` (`drug_id`),
  KEY `fk_demographic` (`demographic_no`),
  KEY `comment_id` (`comment_id`),
  CONSTRAINT `fk_action` FOREIGN KEY (`pharmacist_action_id`) REFERENCES `lu_drugTherapyProblemPharmacistAction` (`id`),
  CONSTRAINT `fk_comment` FOREIGN KEY (`comment_id`) REFERENCES `drugTherapyProblemComment` (`id`),
  CONSTRAINT `fk_demographic` FOREIGN KEY (`demographic_no`) REFERENCES `demographic` (`demographic_no`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_drug` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`drugid`),
  CONSTRAINT `fk_issue` FOREIGN KEY (`issue_id`) REFERENCES `lu_drugTherapyProblemIssue` (`id`),
  CONSTRAINT `fk_priority` FOREIGN KEY (`priority_id`) REFERENCES `lu_drugTherapyProblemPriority` (`id`),
  CONSTRAINT `fk_probability` FOREIGN KEY (`probability_id`) REFERENCES `lu_drugTherapyProblemProbability` (`id`),
  CONSTRAINT `fk_severity` FOREIGN KEY (`severity_id`) REFERENCES `lu_drugTherapyProblemSeverity` (`id`),
  CONSTRAINT `fk_status` FOREIGN KEY (`status_id`) REFERENCES `lu_drugTherapyProblemStatus` (`id`)
);



