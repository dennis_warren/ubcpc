<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<security:oscarSec roleName='${ sessionScope[userrole] }, ${ sessionScope[user] }' rights="x" objectName="_admin,_admin.userAdmin">
	<c:redirect url="${pageContext.request.contextPath}/securityError.jsp?type=_admintype=_admin.userAdmin" />
</security:oscarSec>

<div class="lookupListItemsWrapper well well-small" >

	<div class="lookupListName header">
		<h4>
			<c:out value="${ lookuplist.listTitle }" /><br />
			<small>
				<c:out value="${ lookuplist.description }" />
			</small>
		</h4>
	</div>
	
	<div class="row-fluid" id="lookupListItems_${ lookuplist.id }"  >		
		<ul class="unstyled lookupListItems">						
			<c:forEach var="lookupListItem" items="${ lookuplist.items }" >	
				<c:if test="${ lookupListItem.active }" >
					<li class="lookupListItem" id="lookupListItem_${ lookupListItem.displayOrder }">
						<c:if test="${ lookuplist.items.size() gt 1 }">							
							<i class="icon-move"></i> 
						</c:if>
						<button type="button" id="removeLookupListItem_${ lookupListItem.id }_${ lookuplist.id }" class="close removeLookupListItem" >
							&times;
						</button>
						<span>
							<c:out value="${ lookupListItem.label }" />
						</span>
					</li>
				</c:if>
			</c:forEach>			
		</ul>
	</div>

	<div class="input-append">		
		<input type="text" class="lookupListItemLabel span11" id="lookupListItemLabel_${ lookuplist.id }" name="lookupListItemLabel_${ lookuplist.id }" value="" />
		<input type="button" class="btn addLookupListItemButton" id="addLookupListItemButton_${ lookuplist.id }" value="add" />
	</div>

</div>		