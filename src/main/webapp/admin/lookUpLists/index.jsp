<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<title>Look-Up List Manager</title>
	<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/DT_bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/library/jquery/jquery-ui.structure.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/jquery-1.9.1.min.js"></script>  
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.2.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" >
	$.fn.bindEvents = function() {

		$(".addLookupListItemButton").unbind("click");
		$(".removeLookupListItem").unbind("click");
		$(".showHideEdit").unbind( "click");
		
		$(".addLookupListItemButton").bind("click", function(){
			var data = new Object();
			data.lookupListId = this.id.split("_")[1];
			data.lookupListItemLabel = $( "#lookupListItemLabel_" + data.lookupListId ).val();
			data.method = "add";
			postData( data, "#lookupListItems_" + data.lookupListId );
			$( "#lookupListItemLabel_" + data.lookupListId ).val('');
		});
	
		$(".removeLookupListItem").bind("click", function(){
			var lookupListId = this.id.split("_")[2];
			var data = new Object();
			data.lookupListItemId = this.id.split("_")[1];
			data.method = "remove";
			postData( data, "#lookupListItems_" + lookupListId );
		});
	
		$(".showHideEdit").bind( "click", function(){		
			var lookupListId = this.id.split("_")[1];
			var showId = "#lookupListItemWrapper_" + lookupListId;
			var cancel = "#cancel_" + lookupListId; 
			var edit = "#edit_" + lookupListId;
	
			$(showId).toggle();
			$(cancel).toggle();
			$(edit).toggle();		
		});
	}
	
 	function postData( data, target ) {		
		$(target).load('${ pageContext.request.contextPath }/lookupListManagerAction.do ' + target, data, function(){			
			$().bindEvents();
		});
	} 
	
	$(document).ready(function(){	
		$().bindEvents();	
		$(".lookupListItemWrapper").hide();
	});
	
	$(function() {
	    $( ".lookupListItems" ).sortable({
	    	 placeholder: "ui-state-highlight"
	    });
	    $( ".lookupListItems" ).disableSelection();
	});

</script>
<style type="text/css">

li.lookupListItem {
	min-height: 20px;
}

  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; height: 1.5em; }
  html>body #sortable li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }
  
	li.lookupListItem {
		list-style-type: none;
		padding: 5px;
		background-color: white;
		border: 2px double #CCCCCC;
		display: block;
	}
</style>
</head>
<body id="lookUpListManager">

<div class="navbar navbar-fixed noprint">
    <div class="navbar-inner">
	    <a class="brand" href="#"><bean:message key="admin.admin.lookuplists.title" /></a>
	    <ul class="nav">   		
	    </ul>
   	</div>
</div>

<security:oscarSec roleName="${ sessionScope.userrole },${ sessionScope.user }" 
	objectName="_admin.*" rights="x" reverse="${ true }">
	
	<div id="lookUpListWrapper">
		<c:import url="./manageLookUpLists.jsp" />	
	</div>	
</security:oscarSec>

<footer></footer>

</body>
</html>