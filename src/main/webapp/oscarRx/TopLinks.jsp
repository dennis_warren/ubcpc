<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%--
	<jsp:include page="../oscarRx/TopLinks.jsp" >
		<jsp:param value="<bean:message key="demographic.demographiceditdemographic.msgPatientDetailRecord" />" name="title" />
		<jsp:param value="TopStatusBar" name="tableClass"/>
		<jsp:param value='<%= (String)session.getAttribute("demographicNo") %>' name="demographicNo" />
		<jsp:param value="<%=demographic.getLastName()%>, <%=demographic.getFirstName()%>" name="patientName" />
		<jsp:param value="<%=demographic.getSex()%>" name="sex" />
		<jsp:param value="<%=age%>" name="age" />
		<jsp:param value="<%= roleName2$ %>" name="security" /> 
		<jsp:param value="true" name="showNextAppt"/>
		<jsp:param value="true" name="showPhrVerification"/>
		<jsp:param value="<%= loggedInInfo.getCurrentFacility().isIntegratorEnabled() %>" name="showIntegrator" /> 
		<jsp:param value="true" name="showDrugRefNfo"/>
		<jsp:param value="" name="pharmacyList"/>
	</jsp:include> 
 --%>
 
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<c:set var="ctx" value="${ pageContext.servletContext.contextPath }" />
<c:set var="url" value="${ ctx }/demographic/demographiccontrol.jsp?demographic_no=${ param.demographicNo }&displaymode=edit&dboperation=search_detail&appointment=" />

<table id="${ not empty param.tableId ? param.tableId : 'topLink' }" class="${ param.tableClass } topLink" >
<tr>
	<td id="topLinkLeftColumn"  > 
		<h1><c:out value="${ param.title }" /></h1> 	
	</td>
		
	<td id="topLinkCenterColumn" >

		${param.demographic}

        <c:if test="${ not empty param.mrp }" >
			<security:oscarSec roleName="${ security }" objectName="_newCasemgmt.doctorName" rights="r">
		    	<span class="label">	
		    		  <bean:message key="oscarEncounter.Index.msgMRP"/>  			   
			    </span>
			    <span>
			     	<c:out value="${ param.mrp }" />
			    </span>
			</security:oscarSec>
		</c:if>
		
		<c:if test="${ param.showNextAppt eq true and not empty param.demographicNo }" >
			<span class="label">	
	    		  <bean:message key="demographic.demographiceditdemographic.msgNextAppt"/> 			   
		    </span>
		    <span>
		    	<oscar:nextAppt demographicNo='${ param.demographicNo }' />
		    </span>
		</c:if>
		
		<c:if test="${ param.showIntegrator eq true }" >
    		<jsp:include page="../admin/IntegratorStatus.jspf" />
		</c:if>

	</td>

	<td id="topLinkRightColumn" >
	 		<span class="HelpAboutLogout" style="color:white;">
	 			<c:if test="${ showDrugRefNfo }">
	 				<a style="color:white;" href="javascript: popupFocusPage(200,700,'./drugrefInfo.jsp','Drugref Info');" >
	 					Drugref Info
	 				</a>
	 			</c:if>
                 <oscar:help keywords="2.2.4" key="app.top1" style="color:white;" />
                 
                 <a style="color:white;" href="${ ctx }/oscarEncounter/About.jsp" target="_new">About</a>
            </span>
	</td>
</tr>
</table>
