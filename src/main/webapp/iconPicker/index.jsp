<%--

    Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    The Pharmacists Clinic
    Faculty of Pharmaceutical Sciences
    University of British Columbia
    Vancouver, British Columbia, Canada

--%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%
  if(session.getValue("user") == null) response.sendRedirect(request.getContextPath() + "/logout.jsp");
   String roleName$ = (String)session.getAttribute("userrole") + "," + (String)session.getAttribute("user");
%>
<security:oscarSec roleName="<%=roleName$%>"
	objectName="_admin"	rights="r" reverse="<%=true%>">
	<%
	response.sendRedirect(request.getContextPath() + "/logout.jsp");
	%>
</security:oscarSec>

<html>
<head>
	<link href="${pageContext.request.contextPath}/library/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>  	
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	
	<script type="text/javascript" >

	    	 var icons = [];
	    	 var cssRules = document.styleSheets[0].cssRules;
	    	 var icon = "";

	    	 for (var i=0; i<cssRules.length; i++) {
	 
		    	   var selectorText = cssRules[i].selectorText;
	
		    	   if (selectorText && selectorText.match(/^\.glyphicon-[a-z]+[-]{0,1}[a-z]+[-]{0,1}[a-z]+[:]{2}[a-z]+$/)) {

		    		   	selectorText = selectorText.replace(".", "");
		    		   	if(selectorText.includes(":"))
		    		   	{
		    			   selectorText = selectorText.split(":")[0];
		    			}
		    		   	
		    		   	icon = document.createElement("span");
		    		   	icon.setAttribute("class", "glyphicon " + selectorText);
		    		   	icon.setAttribute("aria-hidden", "true");
		    		   
		    		   	icons.push(icon);
		    	   }
	    	 }

	    	 jQuery(document).ready(function(){	    		 
	    		 var paletteElement = document.createElement("ul");
	    		 paletteElement.setAttribute("class", "bootstrap-glyphicon-list");
	    		 
	    		 var listItem = "";
	    		 var title = "";
	    		 var titleElement = {};
	    		 var icon = {};
	    		 var iconClass = "";
	    		 
		    	 for (var i=0; i<icons.length; i++) {
		    		 
		    		 icon = icons[i];		    		 
		    		 iconClass = icon.getAttribute("class");
		    		 title = iconClass.split(" ")[1];
		    		 
		    		 if(title.includes("-"))
		    		 {
			    		 var titleArray = title.split("-");
		    			 title = "";
			    		 for(var j=1; j<titleArray.length; j++) 
			    		 {
			    			 title += titleArray[j].charAt(0).toUpperCase() + titleArray[j].slice(1) + " ";
			    		 }
		    		 }
		    		 else
		    		 {
		    			 title = title.charAt(0).toUpperCase() + title.slice(1);
		    		 }
		    		 
		    		 title = title.trim();
		    		 
		    		 titleElement = document.createElement("span");
		    		 titleElement.setAttribute("class", "bootstrap-glyphicon-title");
		    		 titleElement.innerHTML = title;
		    		 
		    		 listItem = document.createElement("li");
		    		 listItem.setAttribute("title", title);
		    		 listItem.setAttribute("id", iconClass.split(" ")[1]);
		    		 listItem.setAttribute("class", "glyphicon-item");
		    		 listItem.appendChild(icon);
		    		 listItem.appendChild(titleElement);
		    		 
		    		 paletteElement.appendChild(listItem);
		    	 }
		    	 
		    	 var selectedElement = document.createElement("input");
		    	 selectedElement.setAttribute("type", "hidden");
		    	 selectedElement.setAttribute("id", "selectedIcon");
		    	 selectedElement.setAttribute("name", "selectedIcon");
		    	 
		    	 document.getElementById("icon-palette").appendChild(paletteElement).appendChild(selectedElement);

	    		jQuery(".glyphicon-item").click(function(){
	    			jQuery("#icon-palette li[class~=selected]").removeClass("selected");
	    			jQuery(this).addClass("selected");
	    			jQuery("#selectedIcon").val(this.id);
				});
	    	 });

	</script>
	<style type="text/css" media="all">
		ul.bootstrap-glyphicon-list {
			padding-left: 0;
		  	list-style: none;
			margin-top: 0;
		  	margin-bottom: 10px;
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: left;
		}
		ul.bootstrap-glyphicon-list li {
			line-height: 1.4;
		  	text-align: center;
		  	background-color: #f9f9f9;
		  	border: 1px solid #fff;
			padding:5px;
			width: 100px;
		}
		ul.bootstrap-glyphicon-list li.selected {
			color:blue;
		}
		ul.bootstrap-glyphicon-list li span.glyphicon:hover, ul.bootstrap-glyphicon-list li:hover {
			color:blue;
		}
		ul.bootstrap-glyphicon-list li span
		{
			display: block;
		}

		ul.bootstrap-glyphicon-list li span.glyphicon
		{
			font-size: large;
		}
		ul.bootstrap-glyphicon-list li span.bootstrap-glyphicon-title
		{
			font-size: small;
			margin-top:5px;
		}
	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid" id="icon-palette"></div>
</div>
</body>
</html>