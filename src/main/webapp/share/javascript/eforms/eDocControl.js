/* printControl - Changes eform to add a server side generated eDocument with respective eDocument Button
 *            
 */

if (typeof jQuery == "undefined") { alert("The eDocControl library requires jQuery. Please ensure that it is loaded first"); }
var eDocControl = {
	initialize: function () {

		var submit = jQuery("input[name='SubmitButton']");
		//submit.append("<input value="false" name="saveAsEdoc" type="hidden" id="saveAsEdoc" value="false">");
		submit.append("<input id='saveAsEdoc' type='hidden' name='saveAsEdoc' value='false'>\n");
		submit.append("<input id='eDocument' name='eDocument' type='button' value='eDocument'>");
		
		//submit.append("\t\t &lt;input id=&quot;saveAsEdoc&quot; type=&quot;hidden&quot; name=&quot;saveAsEdoc&quot; value=&quot;false&quot;&gt; \n");
		//submit.append("\t\t &lt;input id=&quot;eDocument&quot; name=&quot;eDocument&quot; type=&quot;button&quot; value=&quot;eDocument&quot;&gt; \n");
		
		//"\t\t &lt;input value=&quot;Reset&quot; name=&quot;ResetButton&quot; id=&quot;ResetButton&quot; type=&quot;reset&quot;&gt; \n"
		
		var eDocHolder = jQuery("input[name='saveAsEdoc']");
		var eDoc = jQuery("input[name='eDocument']");
		
		if (eDocHolder.size() == 0) { eDocHolder = jQuery("input[name='saveAsEdoc']"); }
		if (eDoc.size() == 0) { eDoc = jQuery("input[name='eDocument']"); }
		
		eDocHolder.insertAfter(submit);
		eDoc.insertAfter(submit);

		
		if (eDoc.size() !=0) {
			eDoc.attr("onclick", "").unbind("click");
			//eDoc.attr("value", "eDocument");
			eDoc.click(function(){eDocHolder.val('true'); needToConfirm=false; jQuery("form").submit();});
		}
	}
};

jQuery(document).ready(function(){eDocControl.initialize();});
