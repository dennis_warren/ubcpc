<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ include file="/casemgmt/taglibs.jsp"%>
<jsp:useBean id="DrugTherapyProblemForm" scope="request" type="org.oscarehr.casemgmt.web.formbeans.DrugTherapyProblemFormBean"/>

<div id="dtpPanel" class="table-responsive" >

<html:form styleId="drugTherapyProblemForm" action="pharmaDTPPanel">

<input type="hidden" name="demographicNo" id="demographicNo" value="${ DrugTherapyProblemForm.demographicNo }" />
<input type="hidden" name="update" id="update" value="${ param.dtpUpdate }" />
<input type="hidden" name="dtpId" id="dtpId" value="${ DrugTherapyProblemForm.dtpId }" />
<input type="hidden" name="DIN" id="DIN" value="${ DrugTherapyProblemForm.DIN }" />
<input type="hidden" name="trackerId" id="trackerId" value="${ DrugTherapyProblemForm.trackerId }" />

<script type="text/javascript">

	const ctx = '${ pageContext.request.contextPath }';

	/*
	 * Show/hide the event comment when the yes/no
	 * radio selection is made.
	 */
	jQuery(".event-radio").change(function(){
		if(this.value === "1") {
			jQuery("#event").show();
		} else {
			jQuery("#event").val('').hide();
		}
	})

	/*
	 * Control the mandatory flags in certain cases.
	 * value 2 = "Needs additional drug"
	 */
	const flag = jQuery("#medication_title span.mandatory");
	const medicationElement = jQuery("#drugId");
	const commentElement = jQuery("#comment");

	jQuery("select[name *= 'issueId']").change(function(){
		toggleMedicationMandatoryFlag(this.value === "2")
		toggleCommentMandatoryFlag(this.value === "2")
	})

	jQuery("select[name *= 'drugId']").change(function(){
		toggleMedicationMandatoryFlag(this.value === "0")
		toggleCommentMandatoryFlag(this.value === "0")
	})

	/*
	 * also disable the mandatory requirement for medications when
	 * issue value #2 = "Needs additional drug"
	 */
	jQuery(document).ready(
			toggleMedicationMandatoryFlag(
					jQuery("#update").val() && jQuery("#issueId").val() === "2"
			),
			toggleCommentMandatoryFlag(
					jQuery("#update").val() && jQuery("#issueId").val() === "2" && medicationElement.val() === "0"
			)
	)

	function toggleMedicationMandatoryFlag(toggle) {
		if(toggle) {
			medicationElement.prop('required',false);
			flag.hide();
		} else {
			medicationElement.prop('required',true);
			flag.show();
		}
	}

	function toggleCommentMandatoryFlag(toggle) {
		if(toggle) {
			commentElement.prop('required', true);
			jQuery("#comment_title span.mandatory").show();
		} else {
			commentElement.prop('required',false);
			jQuery("#comment_title span.mandatory").hide();
		}
	}

	if( document.getElementById("dateResponse") ) {
		Calendar.setup({ inputField : "dateResponse", ifFormat : "%Y-%m-%d", showsTime :false, button : "dateResponse", singleClick : true, step : 1 });
	}
	if( document.getElementById("dateIdentified") ) {
		Calendar.setup({ inputField : "dateIdentified", ifFormat : "%Y-%m-%d", showsTime :false, button : "dateIdentified", singleClick : true, step : 1 });
	}
	if( document.getElementById("dateEntered") ) {
		Calendar.setup({ inputField : "dateEntered", ifFormat : "%Y-%m-%d", showsTime :false, button : "dateEntered", singleClick : true, step : 1 });
	}

	jQuery("#event").autocomplete({
		source: function(request, response) {
			jQuery.ajax({
				url: ctx + "/pharmaDTPPanel.do?method=searchEvent",
				dataType: "json",
				data: {
					term: request.term,
				},
				success: function(data) {
					response(jQuery.map( data, function(item) {
						return {
							label: item.event
						}
					}))
				}
			})
		},
		minLength: 2,
		select: function( event, ui ) {
			jQuery(this).val(ui.item.event);
		}
	});

</script>

<c:if test="${ param.dtpUpdate }">
	<div id="dtpHistoryHeading">
		<div>
			<strong>Issue:</strong> <c:out value="${ DrugTherapyProblemForm.drugTherapyProblem.issue }" />
			<input type="hidden" name="issueId" id="issueId" value="${ DrugTherapyProblemForm.issueId }" />
		</div>
		<div>
			<strong>Date Identified:</strong>
			<fmt:formatDate value="${ DrugTherapyProblemForm.drugTherapyProblem.dateIdentified }"  pattern="yyyy-MM-dd"/>
			<input type="hidden" value="${ DrugTherapyProblemForm.dateIdentified }" id="dateIdentified" name="dateIdentified" />
		</div>
	</div>
</c:if>

<table class="dtp-table table table-condensed table-striped" >
		<tr>				
			<c:if test="${ param.dtpUpdate }">
				<th>Entry Date<span class="mandatory" title="Mandatory">*</span></th>
				<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="entered">
					<c:if test="${ entered.id != DrugTherapyProblemForm.dtpId }">
						<td>
							<fmt:formatDate value="${ entered.dateEntered }" pattern="yyyy-MM-dd"/>
						</td>
					</c:if>
				</c:forEach>
				<td>
					<input type="text" id="dateEntered" name="dateEntered" placeholder="${ DrugTherapyProblemForm.dateEntered } (yyyy-mm-dd)"
						alt="calendar" class="form-control casenote" value="" required/>
					<input type="hidden" id="lastDateEntered" value="${ DrugTherapyProblemForm.dateEntered }" />
				</td>
			</c:if>
			<c:if test="${ not param.dtpUpdate }">
				<th>Identified<span class="mandatory" title="Mandatory">*</span></th>
				<td>
					<input type='text' alt="calendar" class="form-control casenote" placeholder="(yyyy-mm-dd)" value="${ DrugTherapyProblemForm.dateIdentified }"
		        	name="dateIdentified" id="dateIdentified" ${ param.dtpUpdate ? 'readonly="readonly"' : '' } required/>
				</td>
			</c:if>
		</tr>

		<c:if test="${ not param.dtpUpdate }">
			<tr>
				<th>Issue<span class="mandatory" title="Mandatory">*</span></th>
				<c:if test="${ param.dtpUpdate }">	
					<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="issue">	
						<c:if test="${ issue.id != DrugTherapyProblemForm.dtpId }">		
							<td><c:out value="${ issue.issue }" /></td>
						</c:if>
					</c:forEach>
				</c:if>
	
					<td>		
						<select class="form-control"  name="issueId" ${ param.dtpUpdate ? 'readonly="readonly"' : '' } required>
							<option value="0" ></option>			
							<c:forEach items="${ DrugTherapyProblemForm.issueList }" var="issue">
							<c:if test="${ issue.active and issue.id gt 0 }">
								<option value="${ issue.id }" 
									${ issue.id eq DrugTherapyProblemForm.issueId ? "selected='selected'" : "" } >		
									<c:out value="${ issue.name }" />
								</option>
							</c:if>		
							</c:forEach>
						</select>
					</td>			
			</tr>
		</c:if>

			<tr>
				<th id="medication_title">Medication<span class="mandatory" title="Mandatory">*</span></th>
				<c:if test="${ param.dtpUpdate }">
					<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="drugName">
						<c:if test="${ drugName.id != DrugTherapyProblemForm.dtpId }">
							<td><c:out value="${ drugName.drugName }" /></td>
						</c:if>
					</c:forEach>
				</c:if>
				<td>
					<select class="form-control"  name="drugId" id="drugId" required>
						<option value="0" selected></option>
						<c:forEach items="${ DrugTherapyProblemForm.medications }" var="medication">
							<c:if test="${ not medication.archived or medication.id eq DrugTherapyProblemForm.drugId }">
								<option value="${ medication.id }"
									${ medication.id eq DrugTherapyProblemForm.drugId ? "selected='selected'" : "" } >
									<c:choose>
										<c:when test="${ not empty medication.genericName }" >
											<c:out value="${ medication.genericName }${ medication.archived ? '(discontinued)' : '' }" />
										</c:when>
										<c:when test="${ not empty medication.brandName }" >
											<c:out value="${ medication.brandName }${ medication.archived ? '(discontinued)' : '' }" />
										</c:when>
										<c:otherwise>
											<c:out value="${ medication.customName }${ medication.archived ? '(discontinued)' : '' }" />
										</c:otherwise>
									</c:choose>
								</option>
							</c:if>
						</c:forEach>
					</select>
				</td>
			</tr>

	<%-- Place Event here --%>

			<tr>
				<th>Event<span class="mandatory" title="Mandatory">*</span></th>
				<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="event">
					<c:if test="${ event.id != DrugTherapyProblemForm.dtpId }">
						<td>
							<div>
								<c:out value="${ not empty event.event ? event.event : 'No event' }" />
							</div>
						</td>
					</c:if>
				</c:forEach>
				<td>
					<div id="event-radio-select" style="display:flex;align-items:flex-end;">
						<input type="radio" name="hasEvent" id="hasEventYes" class="event-radio" value="1"/>
						<label for="hasEventYes" class="radio-inline">Yes</label>
						<input type="radio" name="hasEvent" id="hasEventNo" class="event-radio" value="0"/>
						<label for="hasEventNo" class="radio-inline">No</label>
					</div>
					<input type="text" placeholder="event comment" style="display:none;" class="form-control casenote" placeholder="${ DrugTherapyProblemForm.event }" name="event" id="event" required/>
				</td>
				<td class="tooltip-column" >
					 <div class="tooltip">
						<img src="../images/Info_information_icon.png" width="15" height="15"/>
						<div class="tooltiptext">
							Event: The patient is experiencing any unwanted harm, discomfort, or symptom possibly being caused by a
							drug or lack of a drug for the documented DTP. For examples, please see the written documentation.
						</div>
					</div>
				</td>
			</tr>
		<tr>
			<th>Pharmacist Action<span class="mandatory" title="Mandatory">*</span></th>
			<c:if test="${ param.dtpUpdate }">
				<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="action">
					<c:if test="${ action.id != DrugTherapyProblemForm.dtpId }">
						<td><ul>
						<c:forEach items="${ action.drugTherapyProblemPharmacistAction }" var="pharmacistAction" >
							<li><c:out value="${ pharmacistAction.drugTherapyProblemPharmacistAction.name }" /></li>
						</c:forEach>
						</ul></td>
					</c:if>
				</c:forEach>
			</c:if>
			<td>		
				<select class="form-control"  name="pharmacistActionId" multiple required>
					<c:forEach items="${ DrugTherapyProblemForm.pharmacistActionList }" var="pharmacistAction">
					<c:if test="${ pharmacistAction.active }">									
						<option value="${pharmacistAction.id}"
							<c:if test="${ not empty DrugTherapyProblemForm.pharmacistActionId }">
								<c:forEach items="${ DrugTherapyProblemForm.pharmacistActionId }" var="pharmacistActionId">
									${ pharmacistAction.id eq pharmacistActionId ? "selected='selected'" : "" }
								</c:forEach>
							</c:if>
						>
							<c:out value="${ pharmacistAction.name }" />
						</option>
					</c:if>
					</c:forEach>
				</select>		
			</td>
		</tr>
		<tr>
			<th>Status<span class="mandatory" title="Mandatory">*</span></th>
			<c:if test="${ param.dtpUpdate }">
				<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="status">
					<c:if test="${ status.id != DrugTherapyProblemForm.dtpId }">				
						<td><c:out value="${ status.status }" /></td>
					</c:if>
				</c:forEach>
			</c:if>
			<td>
				<select class="form-control"  name="statusId" required>
					<c:forEach items="${ DrugTherapyProblemForm.statusList }" var="status">
					<c:if test="${ status.active }">								
						<option value="${ status.id }" 
							${ status.id eq DrugTherapyProblemForm.statusId ? "selected='selected'" : "" }>		
							<c:out value="${ status.name }" />
						</option>
					</c:if>		
					</c:forEach>
				</select>			
			</td>
		</tr>
		
		<tr>
			<th id="comment_title">Comment<span class="mandatory" title="Mandatory">*</span></th>
			<c:if test="${ param.dtpUpdate }">
				<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="comment">
					<c:if test="${ comment.id != DrugTherapyProblemForm.dtpId }">							
						<td><c:out value="${ not empty comment.comment ? comment.comment : 'No Comment' }" /></td>
					</c:if>	
				</c:forEach>
			</c:if>
			<td>
				<textarea class="form-control" placeholder="${ DrugTherapyProblemForm.comment }" name="comment" id="comment" class="casenote" required></textarea>
			</td>
		</tr>

</table>
</html:form>
</div>