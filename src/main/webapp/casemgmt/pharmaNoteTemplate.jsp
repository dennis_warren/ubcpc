<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@include file="/casemgmt/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"	scope="request" />

<div id="pharmaNoteTemplate" class="col-md-12 encounterTemplate">

	<div class="row" id="pharmaNoteTemplateHeader" >Pharmacists SOAP Template</div>
	
	<html:form styleId="pharmaNoteTemplateForm" action="pharmaNoteTemplate"  >

		<html:hidden name="PharmaNoteTemplateForm" property="demographicNo"  />
		
			<div class="row" >
				<p>Chief complaint/Reason for referral</p>
				<html:hidden name="PharmaNoteTemplateForm" property="chiefComplaintHeading" value="CHIEF COMPLAINT: "  />
				<html:textarea styleClass="casenote" property="chiefComplaint" name="PharmaNoteTemplateForm"></html:textarea>
			</div>
			
			<div class="row" >
					<p>Known Medical Conditions 
					<a href="javascript:void(0);" onclick="popupPage(580,900,'Disease${ PharmaNoteTemplateForm.demographicNo }','${ ctx }/oscarResearch/oscarDxResearch/setupDxResearch.do?demographicNo=${ PharmaNoteTemplateForm.demographicNo }&providerNo=${ PharmaNoteTemplateForm.provider.providerNo }&quickList='); return false;" >
						add
					</a>
					</p>
					<html:hidden name="PharmaNoteTemplateForm" property="knownMedicalConditionsHeading" value="KNOWN MEDICAL CONDITIONS: "  />
					<dl id="medicalConditionList">
						<c:forEach items="${ PharmaNoteTemplateForm.dxResearchComments }" varStatus="count" var="dxResearchComment">
	
							<dd>
								<span class="capitalize" ><c:out value="${ dxResearchComment.description }" /></span>
								<a href="javascript:void(0);" class="additionalCommentBtn" id="dx_${ count.index }" >comment</a>
							</dd>
							<dt class="additionalAnnotation" id="additionalAnnotation_dx_${ count.index }" >
								<html:hidden indexed="true" property="description" name="dxResearchComment"  />					
								<html:textarea property="note" indexed="true" name="dxResearchComment" styleClass="casenote" ></html:textarea>
							</dt>

						</c:forEach>
					</dl>
				
			</div>
			
			<div class="row" >
				<p>Current Medications 
					<a href="javascript:void(0);" onclick="popupPage(580,1027,'Rx${ PharmaNoteTemplateForm.demographicNo }','${ ctx }/oscarRx/choosePatient.do?providerNo=${ PharmaNoteTemplateForm.provider.providerNo }&demographicNo=${ PharmaNoteTemplateForm.demographicNo }'); return false;">
					add
					</a>				
				</p>
				<html:hidden name="PharmaNoteTemplateForm" property="currentMedicationsHeading" value="CURRENT MEDICATIONS: "  />
				<dl id="medicationList">
					<c:forEach items="${ PharmaNoteTemplateForm.medications }" var="medication">
						<dd>
							<span class="capitalize" >
								<c:out value="${ medication.genericName }" />
							</span>
							<a href="javascript:void(0);" class="additionalCommentBtn" id="rx_${ medication.id }" >comment</a>
						</dd>
						<dt class="additionalAnnotation" id="additionalAnnotation_rx_${ medication.id }" >
							<html:hidden property="genericName" name="medication" indexed="true" />
							<html:hidden property="id" name="medication" indexed="true" />
							<html:textarea property="comment" indexed="true" name="medication" styleClass="casenote" ></html:textarea>
						</dt>
					</c:forEach>
				</dl>	
			</div>
			
<%-- 			<div>
				<p>Allergies <a href="javascript:void(0);" >add</a></p>
				<dl id="allergyList">
					<c:forEach items="${ PharmaNoteTemplateForm.allergies }" var="allergy" >
						<dd>
							<span class="capitalize" ><c:out value="${ allergy.description }" /></span>
							<a href="javascript:void(0);" class="additionalCommentBtn" id="allergy_${ allergy.id }" >comment</a>
						</dd>
						<dt class="additionalAnnotation" id="additionalAnnotation_allergy_${ allergy.id }" >
							<html:textarea property="reaction" name="allergy" styleClass="casenote" ></html:textarea>
						</dt>	
					</c:forEach>			
				</dl>
			</div> --%>
			
			<div class="row" >
				<p>Patient Barriers</p>
				<html:hidden name="PharmaNoteTemplateForm" property="patientBarriersHeading" value="PATIENT BARRIERS: "  />
				<html:textarea styleClass="casenote" property="patientBarriers" name="PharmaNoteTemplateForm"></html:textarea>
			</div>

			<div  class="row" >
				<p>Assessment Summary</p>
				<html:hidden name="PharmaNoteTemplateForm" property="assessmentSummaryHeading" value="ASSESSMENT SUMMARY: "  />
				<html:textarea styleClass="casenote" property="assessmentSummary" name="PharmaNoteTemplateForm"></html:textarea>
			</div>

			<div class="row" >
				<p>DTP's Addressed</p>

				<html:hidden name="PharmaNoteTemplateForm" property="dtpAddressedHeading" value="DTP'S ADDRESSED: "  />
				<html:textarea styleClass="casenote" property="dtpAddressed" name="PharmaNoteTemplateForm"></html:textarea>
			</div>
			
			<div class="row" >
				<p>DTP's to Complete</p>

				<html:hidden name="PharmaNoteTemplateForm" property="dtpToCompleteHeading" value="DTP'S TO COMPLETE: "  />
				<html:textarea styleClass="casenote" property="dtpToComplete" name="PharmaNoteTemplateForm"></html:textarea>
			</div>
			
			<div class="row" >
				<p>Care Plan</p>
				<html:hidden name="PharmaNoteTemplateForm" property="followUpPlanHeading" value="CARE PLAN: "  />
				<html:textarea styleClass="casenote" property="followUpPlan" name="PharmaNoteTemplateForm"></html:textarea>				
			</div>

<!-- 		<fieldset id="studentParticipationFieldset" >
			<legend>Student Participation</legend>
			
			<input type="checkbox" id="" name="" />
			<label for="" >1st Year</label>
			<input type="checkbox" id="" name="" />
			<label for="" >2nd Year</label>
			<input type="checkbox" id="" name="" />
			<label for="" >3rd Year</label>
			<input type="checkbox" id="" name="" />
			<label for="" >4th Year</label>
			<input type="checkbox" id="" name="" />
			<label for="" >Community Resident</label>
			<input type="checkbox" id="" name="" />
			<label for="" >PharmD Residents</label>
			<input type="checkbox" id="" name="" />
			<label for="" >Medication Management Certificate</label>
			<input type="checkbox" id="" name="" />
			<label for="" >Dentistry</label>
			<input type="checkbox" id="" name="" />
			<label for="" >Nursing</label>
			<input type="checkbox" id="" name="" />
			<label for="" >Other</label>
			<input type="text" />
			
	</fieldset> -->

	
	<div class="row" id="pharmaNoteTemplateFooter" >

 		<button type="button" class="btn_pharmaTemplate" id="btn_pharmaTemplate_save" >Save</button> 		
		<button type="button" class="btn_pharmaTemplate" id="btn_pharmaTemplate_save_sign" >Sign &amp; Save</button>
		
<!-- 		<button type="button" class="btn_pharmaTemplate" id="btn_pharmaTemplate_print" >Print</button>
		<button type="button" class="btn_pharmaTemplate" id="btn_pharmaTemplate_bpmh" >BPMH</button> -->
		
		<button type="button" class="btn_pharmaTemplate" id="btn_pharmaTemplate_close" >Close</button>
		
		<span id="statusMessage"></span>
	</div>
	
</html:form>
</div> <!--  end pharma template columns -->

<script type="text/javascript">
//<!--

//--> On Load functions and global varialbles.
var ctx = "${pageContext.request.contextPath}";

//--> capitalize selected text.
jQuery.fn.capitalize = function () {
    jQuery.each(this, function () {
    	var phrase = this.innerHTML.toLowerCase().trim();
        var split = phrase.split(' ');
        var word;
        for (var i = 0, len = split.length; i < len; i++) {
        	word = split[i].trim();
            split[i] = word.charAt(0).toUpperCase() + split[i].slice(1);
        }
        this.innerHTML = split.join(' ');
    });
    return this;
};

//--> General ajax function
function pharmaTemplateAjax( params, reload ) {		
	jQuery.ajax({
	    url: ctx + "/pharmaNoteTemplate.do",
	    type: 'POST',
	    data: params,
	  	dataType: "html",
	    success: function(html) {	
	    	jQuery("#statusMessage").html("Saved");
	    	
	    	if( reload ) {
	    		location.reload(true);
	    	} 
		}
	})
}

//--> Load functions on document ready
jQuery(document).ready(function(){
	jQuery(".additionalAnnotation").hide();
	jQuery(".capitalize").capitalize();
})

//--> DTP Display Actions
/* jQuery(".dtpassessment").click(function(){
	var id;
	if( jQuery(this).is(':checked') ) {
		id = this.id + "_assessment";
		jQuery("#" + id).show();
	} else {
		id = this.id + "_assessment";
		jQuery("#" + id).hide();
	}
}) */

//--> Save action.
jQuery(".btn_pharmaTemplate").click(function(){
	
	var params = $(this.form).serialize();
	
	
	var idArray = this.id.split("_");
	var action = idArray[2].trim();
	var subAction;
	if( idArray.length == 4 ) {
		subAction = idArray[3].trim();
	}
	var reload = false;
	
	if( action == "close" ) {
		closeTemplate();
		return;
	}
	
	params += "&method=" + action;

	if( subAction == "sign" ) {
		params += "&signed=true"
		reload = true;
	}
	
	pharmaTemplateAjax( params, reload );

})


//--> Expand comment boxes
jQuery(".additionalCommentBtn").click(function(){
	
	var id = "#additionalAnnotation_" + this.id;

	jQuery(id).toggle();

	if( this.innerHTML == "comment") {
		this.innerHTML = "close";
	}else{
		this.innerHTML = "comment";
	}

})

//--> Close the template;
function closeTemplate() {	
	jQuery("#templateContainer").empty().toggle().append('<div id="templatePlaceholder" ></div>');
	location.reload(true);
}

//-->
</script>
