<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ include file="/casemgmt/taglibs.jsp"%>

<script type="text/javascript" src="${ pageContext.request.contextPath }/library/DataTables-1.10.12/media/js/jquery.dataTables.min.js" ></script>

<script type="text/javascript">

var drilldownTable; 
	
jQuery(document).ready( function() {

	//--> table init
	drilldownTable = jQuery('#dtpDashboardTable').DataTable({
		columnDefs: [
			{
				target: 2,
				visible: false,
			},
		],
	});
	
});

</script>

<table class="display" id="dtpDashboardTable" >
	<thead>
		<tr>
			<td>Date Identified</td>
			<td>Issue</td>
			<td>Medication</td>
			<td>Event</td>
			<td>Pharmacist Action</td>
			<td>Status</td>
			<td>Comment</td>
			<td>Update Date</td>
			<td>&nbsp;</td>
			<td style="display:none;">&nbsp;</td>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td>Date Identified</td>
			<td>Issue</td>
			<td>Medication</td>
			<td>Event</td>
			<td>Pharmacist Action</td>
			<td>Status</td>
			<td>Comment</td>
			<td>Update Date</td>
			<td>&nbsp;</td>
			<td style="display:none;"></td>
		</tr>
	</tfoot>
	<tbody>
		<c:forEach items="${ DrugTherapyProblemForm.drugTherapyProblems }" var="drugTherapyProblem">
			<tr class="clickable-row" id="dtpPanelItem_${drugTherapyProblem.id}" data-poload="${ pageContext.request.contextPath }/pharmaDTPPanel.do?method=fetchSingle&dtpId=${ drugTherapyProblem.id }" >
				<td>
					<c:out value="${ drugTherapyProblem.dateIdentified }" />
				</td>
				<td>
					<c:out value="${ drugTherapyProblem.issue }" />
				</td>
				<td>
					<c:out value="${ drugTherapyProblem.drugName }" />
				</td>
				<td>
					<c:out value="${ not empty drugTherapyProblem.event ? drugTherapyProblem.event : 'No event' }" />
				</td>
				<td>
					<ul>
						<c:forEach items="${ drugTherapyProblem.drugTherapyProblemPharmacistAction }" var="pharmacistAction" >
							<li><c:out value="${ pharmacistAction.drugTherapyProblemPharmacistAction.name }" /></li>
						</c:forEach>
					</ul>
				</td>
				<td>
					<c:out value="${ drugTherapyProblem.status }" />
				</td>
				<td>
					<c:out value="${ drugTherapyProblem.comment }" />
				</td>
				<td>
					<c:out value="${ drugTherapyProblem.dateUpdated }" />
				</td>
				<td>
					<button value="edit">update</button>
				</td>
				<td style="display:none;">${ drugTherapyProblem.trackerId }</td>
			</tr>


			<c:forEach items="${ drugTherapyProblem.drugTherapyProblemHistory }" var="drugTherapyProblemHistory">
				<tr>
					<td>
						<c:out value="${ drugTherapyProblemHistory.dateIdentified }" />
					</td>
					<td>
						<c:out value="${ drugTherapyProblemHistory.issue }" />
					</td>
					<td>
						<c:out value="${ drugTherapyProblemHistory.drugName }" />
					</td>
					<td>
						<c:out value="${ not empty drugTherapyProblemHistory.event ? drugTherapyProblemHistory.event : 'No event' }" />
					</td>
					<td>
						<ul>
							<c:forEach items="${ drugTherapyProblemHistory.drugTherapyProblemPharmacistAction }" var="pharmacistAction" >
								<li><c:out value="${ pharmacistAction.drugTherapyProblemPharmacistAction.name }" /></li>
							</c:forEach>
						</ul>
					</td>
					<td>
						<c:out value="${ drugTherapyProblemHistory.status }" />
					</td>
					<td>
						<c:out value="${ drugTherapyProblemHistory.comment }" />
					</td>
					<td>
						<c:out value="${ drugTherapyProblemHistory.dateUpdated }" />
					</td>
					<td>&nbsp;</td>
					<td style="display:none;">${ drugTherapyProblemHistory.trackerId }</td>
				</tr>
			</c:forEach>


		</c:forEach>
	</tbody>
</table>
