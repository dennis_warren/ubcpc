/*<%--

    Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    The Pharmacists Clinic
    Faculty of Pharmaceutical Sciences
    University of British Columbia
    Vancouver, British Columbia, Canada
    
--%>*/

/* Add this script to any page where editing a Lookup List Item Description, Colour or Icon is desired.
 * 
 * See a working demonstration in appointmentstatussetting.jsp and appointmentReasonEdit.jsp
 * 
 * Dependencies: 
 * 
 * iconPicker.index.jsp
 * JQuery
 * JQuery UI
 * JQuery colorPicker
 * JQuery modal
 * Bootstrap 3 
 * Struts ActionClass
 *
 * Enable an edit trigger (button) with these class attributes:
 * 
 * To edit Description: class="editDescription"
 * To edit Colour: class="editColour"
 * To edit Icon: class="editIcon"
 * 
 * The id attributes for each of the triggers need to include the LookupListItem ID. 
 * 
 * Description: id="description-${ reasonLookupListItem.id }"
 * Colour: id="colour-${ reasonLookupListItem.id }"
 * Icon: id="icon-${ reasonLookupListItem.id }"
 * 
 * To include the currently set value for each element add this id to the value one of two ways. IE:
 * 
 * <span id="current-colour-${ reasonLookupListItem.id }">#FFFFFF</span>
 * or
 * <input type="hidden" id="current-colour-${ reasonLookupListItem.id }" value="#FFFFFF" />
 * 
 * Id's are: 
 * 
 * Description: id="current-description-${ reasonLookupListItem.id }"
 * Colour: id="current-colour-${ reasonLookupListItem.id }"
 * Icon: id="current-icon-${ reasonLookupListItem.id }"
 * 
 * This script will load the target item to be edited into a form in a modal window. The following HTML must be added
 * to the bottom of each page in order to enable the modal: 
 * 
 * <div id="dialog-container">
 *	<form action="${pageContext.request.contextPath}/appointment/appointmentMetadata.do" id="dialog-form" class="form-horizontal"></form>
 * </div>
 * 
 * The action attribute can be changed as needed for the application. 
 *   
 * The form in the modal window will submit the following request attribute variable names (make sure the action class is listening for them):
 * 
 * If using Struts Dispatch action: 
 * 
 * "method" or "dispatch"
 * 
 * The struts dispatch methods must be: 
 * 
 * "updateColour()"
 * "updateDescription()"
 * "updateIcon()"
 * 
 * To feed a specific Action Mapping alias (don't for get to set this value in struts-config):
 * 
 * "forward"
 * 
 * ie: <input type="hidden" value="reason" name="forward" />
 * 
 * OR
 * 
 * "forward" can alternatively be set into the Form action attribute: action="${pageContext.request.contextPath}/appointment/appointmentMetadata.do?forward=${response-page}"
 * 
 * 
 * For the Lookup List Item id: 
 * 
 * "ID"
 * 
 * For the attributes being edited: 
 * 
 * Colour: "apptColor"
 * Icon: "selectedIcon"
 * Description: "apptDesc"
 * 
 */

$(document).ready(function(){
	
	var dialogFields = $( [] ); 
	/*
	 * Creates a modal container 
	 */
    var dialog = $( "div#dialog-container" ).dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
          Cancel: function() {
            dialog.dialog( "close" );
          },
    	  Save: function() {
    		  $("#dialog-container #dialog-form").submit();
    		  dialog.dialog( "close" );
    	  }
        },
        close: function() {
        	$("#dialog-container #dialog-form")[0].reset();
        	dialogFields.removeClass( "ui-state-error" );
        	dialogFields.remove();
        }
    });
    
	/*
	 * Creates a modal for editing a lookup list item Description 
	 */
    $(".editDescription").click(function(){
    	var dialogForm = $("#dialog-container #dialog-form");
    	var identity = this.id;
    	var currentSetting = $("#current-" + identity).text();
    	if(! currentSetting)
    	{
    		currentSetting = $("#current-" + identity).val();
    	}
    	var forwardTo = $("#thispage").val();

    	var inputElement = $("<input />")
    		.attr("type","text")
    		.attr("name","apptDesc")
    		.val(currentSetting);
    	
    	var idElement = $("<input />")
    		.attr("type","hidden")
    		.attr("name","ID")
    		.val(this.id.split("-")[1]);
    	
    	var dispatchElement = $("<input />")
    		.attr("type","hidden")
    		.attr("name","dispatch")
    		.val("update");
    	
    	var methodElement = $("<input />")
			.attr("type","hidden")
			.attr("name","method")
			.val("updateDescription");
    	
    	if(forwardElement)
    	{
	    	var forwardElement = $("<input />")
	    		.attr("type","hidden")
	    		.attr("name","forward")
	    		.val(forwardTo);
	    	
	    	forwardElement.appendTo(dialogForm);
    	}
    	
       	dispatchElement.appendTo(dialogForm);
    	methodElement.appendTo(dialogForm);
    	idElement.appendTo(dialogForm);
    	inputElement.appendTo(dialogForm);
    	
    	dialogFields.add( dispatchElement ).add( methodElement ).add( idElement ).add(inputElement);
        	
    	dialog.dialog( "option", "height", 150 )
    		.dialog( "option", "width", 350 )
    		.dialog( "option", "title", "Enter New Description" )
    		.dialog("open");
    });
    
	/*
	 * Creates a modal for editing a lookup list item Colour with a graphical colour picker 
	 */
    $(".editColour").click(function(){
    	
		var dialogForm = $("#dialog-container #dialog-form");
		var identity = this.id;
		var currentSetting = $("#current-" + identity).text();
		if(! currentSetting)
		{
			currentSetting = $("#current-" + identity).val();
		}
		
		if(! currentSetting)
		{
			currentSetting = "#FFFFFF";
		}
		
		var forwardTo = $("#thispage").val();
		
		var inputElement = $("<input />")
			.attr("type","text")
			.attr("id","apptColor")
			.attr("name","apptColor")
			.val(currentSetting);
		
		var idElement = $("<input />")
			.attr("type","hidden")
			.attr("name","ID")
			.val(identity.split("-")[1]);
		
		var dispatchElement = $("<input />")
			.attr("type","hidden")
			.attr("name","dispatch")
			.val("update");
		
		var methodElement = $("<input />")
			.attr("type","hidden")
			.attr("name","method")
			.val("updateColour");
		
		if(forwardElement)
		{
		 	var forwardElement = $("<input />")
		 		.attr("type","hidden")
				.attr("name","forward")
		 		.val(forwardTo);
			forwardElement.appendTo(dialogForm);
		}
		
		dispatchElement.appendTo(dialogForm);
		methodElement.appendTo(dialogForm);
		idElement.appendTo(dialogForm);
		inputElement.appendTo(dialogForm);
		
		// collection to destroy when the modal closes.
		dialogFields.add( dispatchElement ).add( methodElement ).add( idElement ).add(inputElement);
		
		dialog.dialog( "option", "height", 410 )
			.dialog( "option", "width", 350 )
			.dialog( "option", "title", "Select a New Colour" )
			.dialog("open");
		
		var colorPicker = inputElement.colorPicker({
			format:'hex',
			colorChange: function(e, ui) {
				inputElement.val(ui.color);
		    }
		 });
		 
		colorPicker.colorPicker('setColor',currentSetting);
		
		dialogFields.add(colorPicker);

    });
    
	/*
	 * Creates a modal for editing a lookup list item Icon with a selection of available 
	 * Bootstrap Glyphicons. 
	 */
    $(".editIcon").click(function(){
    	var dialogForm = $("#dialog-container #dialog-form");
    	var identity = this.id;
    	var currentSetting = $("#current-" + identity).text();
    	if(! currentSetting)
    	{
    		currentSetting = $("#current-" + identity).val();
    	}
    	var listName = $("#listName");

    	dialogForm.load('../iconPicker/index.jsp', function() {
    		
    	   	var idElement = $("<input />")
    	   		.attr("type","hidden")
    	   		.attr("name","ID")
    	   		.val(identity.split("-")[1]);
        	
    	   	var dispatchElement = $("<input />")
        		.attr("type","hidden")
        		.attr("name","dispatch")
        		.val("update");
        	
        	var methodElement = $("<input />")
				.attr("type","hidden")
				.attr("name","method")
				.val("updateIcon");
        	
           	if(forwardElement)
        	{
	        	var forwardElement = $("<input />")
	        		.attr("type","hidden")
	        		.attr("name","forward")
	        		.val(forwardTo);
	        	
	        	forwardElement.appendTo(dialogForm);
        	}
           	
           	dispatchElement.appendTo(dialogForm);
        	methodElement.appendTo(dialogForm);
        	idElement.appendTo(dialogForm);
        	listName.appendTo(dialogForm);

	    	dialog.dialog( "option", "width", 526 )
	    		.dialog( "option", "height", 400 )
	    		.dialog( "option", "title", "Select a New Icon" )
	    		.dialog("open");
	    });
    });
    
});

