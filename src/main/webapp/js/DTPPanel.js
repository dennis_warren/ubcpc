/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */

// creates DTP buttons based on the panel (new or update)
// didn't have time to be concise.

const validate = {
	dateIdentified: function() {
		if( ! jQuery('#dateIdentified').val().length ) {
			jQuery('#dateIdentified').css("border", "red thin solid");
			alert("The Date Identified is missing.");
			return false;
		}

		let dateStringIdentified = jQuery('#dateIdentified').val().split("-");
		let dateIdentified = new Date( dateStringIdentified[0], dateStringIdentified[1] -1,  dateStringIdentified[2]);
		let today = new Date();

		if( dateIdentified > today ) {
			jQuery('#dateIdentified').css("border", "red thin solid");
			alert("The Date Identified cannot be in the future.");
			jQuery('#dateIdentified').val("");
			return false;
		}
		return true;
	},

	dateEntered: function() {
		const dateStringEntered = jQuery('#dateEntered').val().split("-");
		const dateStringIdentified = jQuery('#dateIdentified').val().split("-");
		const dateStringLastDateEntered = jQuery('#lastDateEntered').val().split("-");
		const dateEntered = new Date( dateStringEntered[0] ,dateStringEntered[1] -1, dateStringEntered[2] );
		const dateIdentified = new Date( dateStringIdentified[0], dateStringIdentified[1] -1, dateStringIdentified[2]);
		const lastDateEntered = new Date( dateStringLastDateEntered[0], dateStringLastDateEntered[1] -1, dateStringLastDateEntered[2]);
		const today = new Date();

		if( ! jQuery('#dateEntered').val().length ) {
			alert("The Entry Date is missing.");
			return false;
		}

		if( dateEntered < dateIdentified ) {
			alert("Entry Date: "
				+ jQuery('#dateEntered').val()
				+ "\nDate Identified: "
				+ jQuery('#dateIdentified').val()
				+ "\n\nThe Entry Date cannot occur before the Date Identified.");
			jQuery('#dateEntered').val("");
			return false;
		}

		if( dateEntered < lastDateEntered ) {
			alert("Last Entry Date: "
				+ jQuery('#lastDateEntered')
				+ "\nEntry Date: "
				+ jQuery('#dateEntered').val()
				+ "\n\nThe current Entry Date cannot occur before the Last Entry Date.");
			jQuery('#dateEntered').val("");
			return false;
		}

		if( dateEntered > today ) {
			alert("The Entry Date cannot occur in the future.");
			jQuery('#dateEntered').val("");
			return false;
		}

		return true;
	},

	dtpElements: function(form) {
		/* the requirement property for this drugIdElement element is dynamically changed prior to reaching this block of code
         * depending on the state of the Issue element.
         */
		let drugIdElement = form.find("select[name*='drugId']");
		let issueIdElement = form.find("select[name*='issueId']");
		let eventElement = form.find("input[name*='hasEvent']");
		let actionElement = form.find("select[name*='pharmacistActionId']");
		let statusElement = form.find("select[name*='statusId']")
		let eventComment = form.find("input[name*='event']");
		let commentElement = form.find("textarea[name*='comment']");
		let eventSelected = form.find("input:radio[name='hasEvent']:checked");

		console.log("ELEMENT STATE");
		console.log("Drug ID: " + drugIdElement.val() + " Required: " + drugIdElement.prop('required'));
		console.log("Issue ID: " + issueIdElement.val() + " Required: " + issueIdElement.prop('required'));
		console.log("Event: " + eventElement.val() + " Required: " + eventElement.prop('required'));
		console.log("Action: " + actionElement.val() + " Required: " + actionElement.prop('required'));
		console.log("Status: " + statusElement.val() + " Required: " + statusElement.prop('required'));
		console.log("Event Comment: " + eventComment.val() + " Required: " + eventComment.prop('required'));
		console.log("Comment: " + commentElement.val() + " Required: " + commentElement.prop('required'));
		console.log("Event Checked: " + eventElement.is(':checked') + " Required: " + eventSelected.prop('required'));

		if(issueIdElement.prop('required') && (issueIdElement.val() === '0' || ! issueIdElement.val())) {
			issueIdElement.css("border", "red thin solid");
		} else if( (drugIdElement.val() === "0" || ! drugIdElement.val()) && drugIdElement.prop('required')) {
			drugIdElement.css("border", "red thin solid");
		} else if(! eventElement.is(':checked')) {
			jQuery("#event-radio-select").css("border", "red thin solid");
		} else if (eventElement.is(':checked') && eventSelected.val() === "1" && ! eventComment.val()) {
			eventComment.css("border", "red thin solid");
		} else if (actionElement.prop('required') && (actionElement.val() === '0' || ! actionElement.val())) {
			actionElement.css("border", "red thin solid");
		} else if (statusElement.val() === "0" || ! statusElement.val()) {
			statusElement.css("border", "red thin solid");
		} else if (commentElement.prop('required') && commentElement.val().length === 0 ){
			commentElement.css("border", "red thin solid");
		} else {
			return true;
		}
	},

	reset: function() {
		jQuery("select").css("border", "revert");
		jQuery("#event-radio-select").css("border", "none");
		jQuery("input[name*='event']").css("border", "revert");
		jQuery("#dateIdentified").css("border", "revert");
		jQuery("textarea").css("border", "revert");
	}
}


function createButtons( action ) {

	const buttons = [];
	const save = {};
	const saveAnother = {};
	const update = {};
	const icon = {};

	icon.primary = "ui-icon-heart";

	if( action && action.split("_")[0] === "dtpPanelItem" ) {

		/*
		 * Bind these events to the UPDATE button of the DTP Panel
		 */
		update.text = "Update";
		update.icons = icon.primary;
		update.click = function() {
			validate.reset();
			const form = jQuery('#drugTherapyProblemForm');
			if(validate.dateEntered() && validate.dtpElements(form)) {
				let params = form.serialize();
				params += "&method=update";
				jQuery.post(form.attr("action"), params);
				jQuery(this).dialog("close");
			}
		};
		buttons.push(update);

	} else if( action && action.split("_")[0] === "dtpPanel" ) {

		/*
		 * Bind these events to the ADD ANOTHER button of the DTP Panel
		 */
		saveAnother.icons = icon.primary;
		saveAnother.text = "Add Another";
		saveAnother.click = function() {
			validate.reset();
			const form = jQuery('#drugTherapyProblemForm');
			if(validate.dateIdentified() && validate.dtpElements(form)) {
				let params = form.serialize();
				params += "&method=save";
				jQuery.post( form.attr("action"), params );
				// Clear the form.
				// hidden elements are retained for the demographic number and form status.
				form.find("input[type=text], textarea, select").val("");
				form.find("input[type=radio]").prop('checked', false);
				form.find("input[id=event]").val('').hide();
			}
		};
		buttons.push(saveAnother);

		/*
		 * Bind these events to the ADD button of the DTP Panel
		 */
		save.icons = icon.primary;
		save.text = "Add";
		save.click = function() {
			validate.reset();
			const form = jQuery('#drugTherapyProblemForm');
			if(validate.dateIdentified() && validate.dtpElements(form)){
				let params = form.serialize();
				params += "&method=save";
				jQuery.post( form.attr("action"), params );
				jQuery( this ).dialog( "close" );
			}
		};
		buttons.push(save);
	}
	
	return buttons;
}

function setTitle( action ) {
	
	if( action && action.split("_")[0] === "dtpDashboard" ) {
		return "DTP Dashboard";
	}
	
	if( action && action.split("_")[0] === "dtpPanelItem" ) {
		return "DTP History";
	} else {
		return "New DTP";
	}
}

jQuery(document).on( 'click', '*[data-poload]', function() {
	let element = jQuery(this);
	element.off('click');
	let elementId = this.id;
	jQuery("#encounterModal").load( element.data('poload') ).dialog({    		
		title: setTitle( elementId ),
		modal:true,
		closeText: "Close",
		hieght: 'auto',
		width: 'auto',
		resizable: true,
		close: function() {
			jQuery('#drugTherapyProblemForm').find("input[type=text], input[type=hidden], textarea, select").val("");
		},
		position: {my:"left", at:"left", of:jQuery("#navigation-layout") },
		buttons: createButtons( elementId ),
		open: function(){
			// disable/enable save or update button for when there are actual changes made. 
			jQuery(".ui-dialog-buttonset .ui-button").button('disable');
			jQuery(document).on('change', 'select.form-control', function() {
				jQuery(".ui-dialog-buttonset .ui-button").button('enable');
			});
			jQuery(document).on('blur', 'input.form-control', function() {
				jQuery(".ui-dialog-buttonset .ui-button").button('enable');
			});
		}
	});

})

