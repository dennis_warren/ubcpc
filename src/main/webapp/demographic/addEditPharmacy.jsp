<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="oscar.oscarRx.pageUtil.*,oscar.oscarRx.data.*,java.util.*"%>	
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%
	String roleName2$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_rx" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_rx");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<html:html locale="true">
<head>
<title>Add/Edit Pharmacy</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">  
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/css/healthCareTeam.css" />

<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/library/bootstrap/3.0.0/css/bootstrap.css" />

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js" ></script>                
<script type="text/javascript">

var id = '${ requestScope.id }'; // server returns the id that was saved.
var demographic_no = '${ requestScope.demographic_no }';

function setfocus() {
	this.window.focus();
	forwardToParent();		  			  		
}

function forwardToParent() {
	// basically tells the parent page to reload if a valid ID is returned.
	if( id ) {
		try {
			window.opener.reloadHealthCareTeam( id, demographic_no );
		} catch(e) {
			// do nothing if function not available.
		}
	} 
}


function onPharmacySave() {
	
	if( checkAllPharmacyFields() ) {
		document.pharmacyForm.submit();
	} else {
		jQuery("#requiredError").show(); 
	}

}

function checkAllPharmacyFields() {
	
	var verified = true;
	var fields = document.forms[0].elements;			
	var fieldname;
	var fieldvalue;
	var fieldobject;
	
	for( var i = 0; i < fields.length; i++ ) {
		
		fieldobject = fields[i];
		fieldname = fieldobject.id;
		fieldvalue = fieldobject.value.trim();

		if( fieldname == "name" && fieldvalue.length == 0 ) {
			verified = false;
			paintErrorField(fieldobject);	
		}
		
		if( fieldname == "city" && fieldvalue.length == 0 ) {
			verified = false;
			paintErrorField(fieldobject);
		} 

		if( fieldname == "phone1" && fieldvalue.length == 0 ) {
			verified = false;
			paintErrorField(fieldobject);
		}
		
	}
	
	return verified;
}

function paintErrorField( fieldobject ) {
	fieldobject.style.border = "medium solid red";
}

function isNumber(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (c == ".") continue;
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}


jQuery(document).ready( function() {
    jQuery(".phoneNumberField").focusout(function(){

    	var elementId = "#" + this.id;
    	var elementValue = this.value;
    	var newNumber;
    	
    	elementValue = elementValue.replace(/ /g,'');
    	elementValue = elementValue.replace(/\D/g,''); 
   	
        if ( elementValue.length == 10 ) {
	        newNumber = "(" + elementValue.substring(0,3) + ") " + elementValue.substring(3,6) + "-" + elementValue.substring(6);
        	jQuery(elementId).attr('value', newNumber );
        }
        
        if ( elementValue.length == 11 ) {
            newNumber = elementValue.substring(0,1) + " (" + elementValue.substring(1,4) + ") " + elementValue.substring(4,7) + "-" + elementValue.substring(7);
        	jQuery(elementId).attr('value', newNumber);
        }
   	
    });
})

</script>
<style type="text/css" >

	body {
		font-size:small;
	}
	
	label {
		margin:0px;
		padding:0px;
	}

	.form-group{
		margin-bottom:0px;
	}
	
	label.required:after {content: " *"; color: red;}
	
</style>
</head>
<body onLoad="setfocus()" >

<div class="container">

<html:form action="/demographic/Contact2.do" styleId="saveEditPharmacyForm"  >
		
		<div class="panel panel-default">
	
		<div class="panel-heading">
			<h4 class="panel-title">
			Add/Edit Pharmacy
			</h4>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<label class="required" for="name">
				
				<input type="hidden" name="method" value="${ method }" /> 
				<input type="hidden" name="demographic_no" value="${ demographic_no }" /> 
				<input type="hidden" name="pharmacyInfo.id" value="${ pharmacyInfo.id }" />
				
				<bean:message key="ManagePharmacy.txtfld.label.pharmacyName" /> :
				</label>
				<input class="form-control" type="text" id="name" name="pharmacyInfo.name" value="${ pharmacyInfo.name }" />
			</div>
			<div class="form-group">
				<label for="address"><bean:message key="ManagePharmacy.txtfld.label.address" />:
				</label>
				<input class="form-control" type="text" id="address" name="pharmacyInfo.address" value="${ pharmacyInfo.address }" />
			</div>
			<div class="form-group">
				<label class="required" for="city"><bean:message key="ManagePharmacy.txtfld.label.city" />
				:</label>
				<input class="form-control" type="text" id="city" name="pharmacyInfo.city" value="${ pharmacyInfo.city }" />
			</div>
			<div class="form-group">
				<label for="province"><bean:message key="ManagePharmacy.txtfld.label.province" />
				:</label>
				

				<jsp:include page="./includes/_provinceStateSelectList.jsp" flush="true">
					<jsp:param value="${ pharmacyInfo.province }" name="selectedProvince"/>
					<jsp:param value="pharmacyInfo.province" name="selectListName" />
					<jsp:param value="province" name="selectListId" />
				</jsp:include>
				
				
			</div>
			<div class="form-group">
				<label for="postalCode"><bean:message key="ManagePharmacy.txtfld.label.postalCode" /> :</label>
				<input class="form-control" type="text" id="postalCode" name="pharmacyInfo.postalCode" value="${ pharmacyInfo.postalCode }" />
			</div>
			<div class="form-group">
				<label class="required" for="phone1"><bean:message key="ManagePharmacy.txtfld.label.phone1" />
				:</label>
				<input  class="form-control phoneNumberField" type="text" id="phone1" name="pharmacyInfo.phone1"  value="${ pharmacyInfo.phone1 }" />
			</div>
			<div class="form-group">
				<label for="phone2"><bean:message key="ManagePharmacy.txtfld.label.phone2" />:</label>
				<input class="form-control phoneNumberField" type="text" id="phone2" name="pharmacyInfo.phone2" value="${ pharmacyInfo.phone2 }" />
			</div>
			<div class="form-group">
				<label for="fax"><bean:message key="ManagePharmacy.txtfld.label.fax" /> :</label>
				<input class="form-control phoneNumberField" id="fax" name="pharmacyInfo.fax" value="${ pharmacyInfo.fax }" />
			</div>
			<div class="form-group">
				<label for="email"><bean:message key="ManagePharmacy.txtfld.label.email" />
				:</label>
				<input class="form-control" type="email" id="email" name="pharmacyInfo.email" value="${ pharmacyInfo.email }" />
			</div>
            <div class="form-group">
                <label for="serviceLocationIdentifier"><bean:message key="ManagePharmacy.txtfld.label.serviceLocationIdentifier" /> :
                </label>
                <input class="form-control" type="text" id="serviceLocationIdentifier" name="pharmacyInfo.serviceLocationIdentifier" value="${ pharmacyInfo.serviceLocationIdentifier }" />
            </div>

			<div class="form-group">
				<label for="notes">
				<bean:message key="ManagePharmacy.txtfld.label.notes" /> :
				</label>
				
					<textarea class="form-control" name="pharmacyInfo.notes" id="notes"  > 
						${ pharmacyInfo.notes }
					</textarea>
				
			</div>
	
		</div> <!-- end panel body -->
		<div class="panel-footer">
        		<button class="btn btn-default" type="button" onclick="onPharmacySave();">
        		<bean:message key="ManagePharmacy.submitBtn.label.submit"/>
        		</button> 

				<button class="btn btn-default" type="button" onclick="window.close()">
        		Cancel </button>
        		
        			<span id="requiredError" style="display:none;color:red;">
				Missing Required Fields
			</span>
        	</div> <!-- end panel footer -->	
</html:form>

</div> <!-- end container -->
</body>
</html:html>

