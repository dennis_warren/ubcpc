<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List, org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.common.web.ContactAction" %>
<%@ page import="org.oscarehr.common.model.Provider, org.oscarehr.common.model.DemographicPharmacy" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.model.ContactSpecialty" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<security:oscarSec roleName="${ sessionScope.userrole }" objectName="_demographic" rights="w" reverse="${ false }">

<%-- DETACHED VIEW ENABLED  --%>
<c:if test="${ param.view eq 'detached' }">

	<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
	<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
	
	<!DOCTYPE html>
	<html>
	<head>
	
	<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/css/healthCareTeam.css" />
	<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/share/css/OscarStandardLayout.css" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js" ></script>                
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.8.18.custom.min.js" ></script>
	
</c:if>
<%-- END DETACHED VIEW ENABLED  --%> 

<script type="text/javascript" >
var popupWindow;
var ctx = "${ pageContext.request.contextPath }";
var demographic_no = "${ param.demographic_no }"

//--> Serialize Javascript Objects
function serializeObject2( prefix, object ) {
	var param;
	if( object instanceof Object ) {
		param = Object.keys( object ).map( function(key) { 
		  	return encodeURIComponent( prefix ) + encodeURIComponent( key ) + '=' + encodeURIComponent( object[key] ); 
		}).join('&')
	}
	return param;	
}

//--> Add contact from popup selection or internal method.
function popUpData( data ){	

	if( data ) {

		jQuery('#searchHealthCareTeamInput').attr('value', null);
		var path = ctx + "/demographic/Contact.do";

		var contactData = new Object();
		var demographicContactData = new Object();
		var contactPrefix = "";
		var param = "";

		contactData.demographic_no = demographic_no;
		contactData.postMethod = "ajax";
		
		contactData.method = data.method;
		contactData.view = data.view;

		if(data.contactType) {
			contactData.contactType = data.contactType;
		}
		
		if( data.contactList ) {
			contactData.contactList = data.contactList;
		}
		
		if( data.preferredOrder > -1 ) {
			contactData.preferredOrder = data.preferredOrder;
		}

		// force parameters required by the saveManage action method.
		if( data.method == "saveManage" ) {			
			contactData.procontact_num = 1; // total number of procontacts to add.				
			contactData.contact_num = 0; // total number of demographic contacts to add.
			param = serializeObject2( contactPrefix, contactData );
			var contactCategory = "procontact"; // prefix is always "procontact" but could change later.
			contactPrefix = contactCategory + '_' + contactData.procontact_num + '.';
		} else {
			param = serializeObject2( contactPrefix, contactData );
		}
						
		demographicContactData.active = 1;
		demographicContactData.consentToContact = 1;
		demographicContactData.type = data.type;
		demographicContactData.contactId = data.contactId;
		demographicContactData.id = 0; // Id of a demographicContact object - set as zero when adding new.

		if( data.id ) {
			demographicContactData.id = data.id;
		}
		
		if( data.contactName ) {
			demographicContactData.contactName = data.contactName;
		}

		if( data.role ) {
			demographicContactData.role = data.role;
		}

		if( data.role == -1 ) {
			demographicContactData.role = jQuery('#selectHealthCareTeamRoleType option:selected').val();
		}

		if( ! data.type ) {
			demographicContactData.type = jQuery('#searchInternalExternal').val().trim();
		}

		param += '&' + serializeObject2( contactPrefix, demographicContactData );

		return sendData(path, param, data.target);	
	}	
}

//--> called to refresh only the Health Care Team portion of any form.
function reloadHealthCareTeam(id, demographic_no) {
	var data = new Object();				
	data.demographicContactId = id;
	data.view = "detached"; // this forces the manageHealthCareTeam.jsp to return a full document.
	data.demographic_no = demographic_no;
	data.method = "manageContactList";
	data.target = "#listHealthCareTeam";
	data.contactList = "HCT";
	popUpData( data );
}

//--> AJAX the data to the server.
function sendData(path, param, target) {
	sendData(path, param, target, "html");
}
function sendData(path, param, target, datatype) {

	jQuery.ajax({
	    url: path,
	    type: 'POST',
	    data: param,
	  	dataType: datatype,
	    success: function(data) {
	    	if( target == 'popup' ) {
 		    	if( popupWindow ) {
	    			popupWindow.close();
				}
 		    	popupWindow = window.open("", "_blank", "scrollbars=yes, resizable=yes, width=600, height=600");
			with(popupWindow.document) {
		      open();
		      write(data);
		      close();
	    	}
			popupWindow.focus();
	    	} else {
				renderResponse(jQuery(data), target);
				if( popupWindow ) {
	    			popupWindow.close();
				}
	    	}
	    }
	});

}

//--> Refresh containing elements 
function renderResponse(html, id) {
	
	if( id instanceof Array ) {
		jQuery.each(id, function(i, val){
			jQuery(val).replaceWith( jQuery(val, html) );
		});			
	} else {			
		jQuery(id).replaceWith( jQuery(id, html) );
	}
	
	jQuery().bindFunctions();

}

//--> Wrapped in a function to re-bind after postback
jQuery.fn.bindFunctions = function() {

	// unbind first just in case.
	jQuery('.editAction').unbind("click");
	jQuery('.removeAction').unbind("click");
	jQuery("input[id*='mostResponsibleProviderCheckbox']").unbind("change");
	jQuery("input[id*='consentToContactCheckbox']").unbind("click");
	
	//--> Edit contact action. 
	jQuery('.editAction').bind("click", function(event){			
		var id = this.id.split("_");
		var param = new Object();
		param.contactId = id.pop();
		param.type = id.pop();
		param.method = id.pop();
		param.target = 'popup';
		popUpData(param);
	})
	
	//--> Remove contact action. 
	jQuery('.removeAction').bind("click", function(event){			
		var id = this.id.split("_");
		var param = new Object();
		param.contactId = id.pop();
		param.type = id.pop();
		param.method = id.pop();
		param.target = "#listHealthCareTeam";
		popUpData(param);
	})

	//--> Change MRP Status
	jQuery("input[id*='mostResponsibleProviderCheckbox']").bind("change", function(){
		var path = ctx + "/demographic/Contact.do"; 
		var param = new Object();
		param.method = "setMRP";
		param.contactId = jQuery("#" + this.id).val(); 

		sendData(path, param, "");
	})
	
	//--> Change Consent to contact status
	jQuery("input[id*='consentToContactCheckbox']").bind("click", function(){
		var elementId = this.id.split("_");
		var path = ctx + "/demographic/Contact.do";
		var param = new Object();
		param.method = "setDNC";
		param.contactId = elementId.pop();
		param.contactGroup = elementId.pop();
		param.dnc = ! this.checked;

		sendData(path, param, "");
	})
}

//--> when this parent window is closed, close all popups.
window.onunload = function() {
    if (popupWindow && ! popupWindow.closed) {
    	popupWindow.close();
    }
};

//--> Document Ready Methods 
jQuery(document).ready( function() {

	//--> Autocomplete searches
	jQuery( "#searchHealthCareTeamInput" ).autocomplete({				
		source: function( request, response ) {  
			var role = jQuery('#selectHealthCareTeamRoleType option:selected').text().trim();
			var url;
			if( "PHARMACY" == role ) {
				url = ctx + "/oscarRx/managePharmacy.do?method=search";
			} else {
				url = ctx + "/demographic/Contact.do?method=searchAllContacts&searchMode=search_name&orderBy=c.lastName,c.firstName";
			}

			jQuery.ajax({
				url: url,
				type: "GET",
				dataType: "json",
				data: {
				 	term: request.term   			  
				},
				contentType: "application/json",
				success: function( data ) {
					response(jQuery.map(data, function( item ) {
						if( role == "PHARMACY" ) {
							 return {
							  label: item.name + " :: " 
							  + item.address 
							  + " :: " + item.city,
							  value: item.id,
							  pharmacy: item
							 }
						} else {
							return {
							  label: item.lastName + ", " 
							  + item.firstName + " :: "
							  + item.residencePhone
							  + " :: " + item.address 
							  + " " + item.city,
							  value: item.id,
							  contact: item
							 }
						}
						 
					}));
				}
			});
	    },
	    minLength: 2,  
	    focus: function( event, ui ) {
	    	event.preventDefault();
	        return false;
	    },
	    select: function(event, ui) { 
	    	event.preventDefault(); 
			if( ui.item.pharmacy ) {
				linkPharmacy( ui.item.pharmacy );				
			} else {
				linkExternalProvider( ui.item.contact );		
			}
	    }      
	});

	//--> Link Pharmacy to Demographic
	function linkPharmacy( pharmacy ) {		
    	var data = new Object();
		data.target = "#listHealthCareTeam";
		data.contactId = pharmacy.id;
		data.preferredOrder = 0;
		data.method = "addPharmacy";
		popUpData( data );
	}
	
	//--> Link external professional contact to Demographic
	function linkExternalProvider( contact ) {
		var data = new Object();
		data.target = "#listHealthCareTeam";
		data.contactId = contact.id;
		data.contactName = contact.lastName + ", " + contact.firstName;
		data.method = "saveManage";
		data.type = contact.systemId; 
		data.role = -1; // -1 uses the setting from the interface.
		popUpData( data );
	}

	//--> Link internal provider to Demographic
	jQuery('#addHealthCareTeamButton').bind("click", function(){
		// get the selected value
		var selected = jQuery('#internalProviderList option:selected');
		var selectedtext = selected.text();
		var roledata = ( (selectedtext.split("(")[1]).trim() ).split(")")[0].trim()
		
		var param = new Object();
		param.contactId = selected.val();
		param.contactName = (selectedtext.split("(")[0]).trim();
		param.role = roledata;
		param.id = 0;
		param.method = "saveManage";
		param.type = "";
		param.target = '#listHealthCareTeam';
		popUpData( param );
	})	
	
	//--> Create new External Professional Specialist.
	//--> Or create new Pharmacy Contact.
	jQuery(".createAction").click(function(){
		var param = new Object();
		param.target = 'popup';
		param.method = this.id; 
		param.contactType = 3;
		popUpData( param );
	})


	//--> Toggle search options
	jQuery(".external").hide();	// internal = on, external = off	
	jQuery('#searchInternalExternal').bind("change", function(){
		var selected = jQuery('#' + this.id + ' option:selected').text();
		jQuery(this).find('option').each(function(){
			if( selected == jQuery(this).text() )	{
				jQuery( "." + selected ).toggle();
			} else {
				jQuery( "." + jQuery(this).text() ).toggle();
			}
		});			
	})
	
	jQuery("#referralSource").bind("change", function(){
		var selected = jQuery('#referralSource option:selected').val();

		var path = ctx + "/demographic/Contact.do"; 
		var param = new Object();
		param.method = "setReferralSource";
		param.referralSourceCode = selected;
		param.demographic_no = demographic_no;

		sendData(path, param, "");
	})
	
	//--> do on page ready
	jQuery().bindFunctions();
	
})
				
</script>

<%-- DETACHED VIEW ENABLED  --%>
<c:if test="${ param.view eq 'detached' }" >
	</head>
	<body id="${ param.view }View" >
	<table class="MainTable" >

	<tr class="MainTableTopRow">
		<td class="MainTableTopRowLeftColumn" width="20%">Manage Health Care Team</td>
		<td class="MainTableTopRowRightColumn">
		<table class="TopStatusBar">
			<tr>
				<td>&nbsp;</td>
				<td>
					<c:out value="${ demographic.lastName }" />,&nbsp;
					<c:out value="${ demographic.firstName }" />&nbsp;
					<c:out value="${ demographic.age }" />&nbsp;years
				</td>
				<td style="text-align: right">
					<oscar:help keywords="contact" key="app.top1"/> | 
					<a href="javascript:popupStart(300,400,'About.jsp')">
					<bean:message key="global.about" /></a> | <a
					href="javascript:popupStart(300,400,'License.jsp')">
					<bean:message key="global.license" /></a></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="2">
</c:if>

<%-- END DETACHED VIEW ENABLED  --%>


<table id="listHealthCareTeam" class="${ param.view }View" >
	<%-- MANAGE PATIENTS HEALTH CARE TEAM  --%>
		<%-- LIST CURRENT HEALTH CARE TEAM --%>
		
		<c:set value="${ healthCareTeam }" var="demographicContactList" scope="page" />

		<tr id="tableTitle" >
			<th colspan="9" class="alignLeft" >Health Care Team</th>
		</tr>
		<tr><td class="firstRow" colspan="9"></td></tr>
		
		<c:if test="${ not empty demographicContactList or not empty pharmacies }" >
			<tr id="healthCareTeamSubHeading" >
				<td class="firstColumn" >&nbsp;</td>
				<td id="deleteColumn"></td>
				<td id="editColumn" ></td>
				<td id="directiveColumn" ></td>
				<td>Role</td>
				<td>Name</td>
				<td>Phone</td>
				<td>Fax</td>
				<td class="lastColumn" >&nbsp;</td>
			</tr>
		</c:if>
		<tr>
		<td class="placeHolderDonotRemove"></td>
		<!-- <td colspan="7" class="displayHCTHeading">Internal providers</td> -->
		<td class="placeHolderDonotRemove"></td>
		</tr>
		<c:forEach items="${ demographicContactList }" var="demographicContact" >
			<c:if test="${ demographicContact.type eq internalProvider }">
			<tr class="highlight">					
				<td class="placeHolderDonotRemove"></td>
				<td colspan="2" class="deleteContactField contactTool" title="Remove Contact" >
					<a href="javascript:void(0)" id="removeContact_${ demographicContact.type }_${ demographicContact.id }"  
						class="removeAction"  >
						<img src="${ oscar_context_path }/images/delete.png" width="16" height="16" alt="remove" />
					</a>
				</td>
				<td>			  
			 	<input type="radio" value="${ demographicContact.id }" name="mostResponsibleProviderCheckbox"
			 		id="mostResponsibleProviderCheckbox_${ demographicContact.id }" 
			 		${ demographicContact.mrp ? 'checked' : '' } />
					<label for="mostResponsibleProviderCheckbox_${ demographicContact.id }" 
			 		title="Assign as the Most Responsible Provider" >MRP</label>
				</td>
				<td class="alignLeft" >	
					<c:out value="${ demographicContact.role }" />				 					
				</td>
                <td class="alignLeft">
                		<c:out value="${ demographicContact.contactName }" />
                </td>
                <td>
                		-
                </td>
                <td>
                		-
                </td>
                <td class="placeHolderDonotRemove"></td>			
			</tr>
			</c:if>		
		</c:forEach>
		<tr>
		<td class="placeHolderDonotRemove"></td>
		<!-- <td colspan="7" class="displayHCTHeading">External Providers</td> -->
		<td class="placeHolderDonotRemove"></td>
		</tr>
		
		<c:forEach items="${ demographicContactList }" var="demographicContact" >		
			<c:if test="${ demographicContact.type ne internalProvider }">
			<tr class="highlight">
				<td class="placeHolderDonotRemove"></td>
				<td class="deleteContactField contactTool" title="Remove Contact" >
					<a href="javascript:void(0)" id="removeContact_${ demographicContact.type }_${ demographicContact.id }"  
						class="removeAction"  >
						<img src="${ oscar_context_path }/images/delete.png" width="16" height="16" alt="remove" />
					</a>
				</td>
				<td class="editContactField contactTool" title="Edit Contact" >
					<a href="javascript:void(0)" id="editHealthCareTeam_${ demographicContact.type }_${ demographicContact.id }"
						class="editAction" >
						<img src="${ oscar_context_path }/images/edit.png" width="16" height="16" alt="edit" />
					</a>
				</td>
				
				<td>			  
			 	<input type="checkbox" id="consentToContactCheckbox_${ demographicContact.id }" 
			 		${ ! demographicContact.consentToContact ? 'checked' : '' } />
			 		
				 	<label for="consentToContactCheckbox_contact_${ demographicContact.id }" 
				 		title="Do not contact this provider" ${ ! demographicContact.consentToContact ? 'class="redAlert"' : '' } >
				 		DNC
				 	</label>
				</td>
								
				<td class="alignLeft" >	
					<c:out value="${ demographicContact.role }" />				 					
				</td>
				
                <td class="alignLeft" >
                		<c:out value="${ demographicContact.contactName }" />
                </td>
	                 		
                <td>
                	<c:out value="${ demographicContact.details.workPhone }" />
                </td>
                <td>
                	<c:out value="${ demographicContact.details.fax }" />
                </td>
 
               	<td class="placeHolderDonotRemove"></td>
            </tr>
            </c:if>	
		</c:forEach>
		<tr>
		<td class="placeHolderDonotRemove"></td>
<!-- 		<td colspan="7" class="displayHCTHeading">Pharmacies</td> -->
		<td class="placeHolderDonotRemove"></td>
		</tr>
		
		<c:forEach items="${ pharmacies }" var="pharmacy" >
			<tr class="highlight">
				<td class="placeHolderDonotRemove"></td>
				<td class="deleteContactField contactTool" title="Remove Contact" >
					<a href="javascript:void(0)" id="removePharmacy_pharmacy_${ pharmacy.id }"  
						class="removeAction"  >
						<img src="${ oscar_context_path }/images/delete.png" width="16" height="16" alt="remove" />
					</a>
				</td>
				<td class="editContactField contactTool" title="Edit Contact" >
					<a href="javascript:void(0)" id="editPharmacyInfo_pharmacy_${ pharmacy.id }"
						class="editAction" >
						<img src="${ oscar_context_path }/images/edit.png" width="16" height="16" alt="edit" />
					</a>
				</td>
				
				<td>			  
			 	<input type="checkbox" id="consentToContactCheckbox_pharmacy_${ pharmacy.id }" 
			 		${ ! pharmacy.consentToContact ? 'checked' : '' }  />
			 		
			 	<label for="consentToContactCheckbox_pharmacy_${ pharmacy.id }" 
			 		title="Do not contact this provider" ${ ! pharmacy.consentToContact ? 'class="redAlert"' : '' } >DNC</label>
				</td>
				
				<td class="alignLeft">
					PHARMACY					
				</td>
				
				<td class="alignLeft">
					<c:out value="${ pharmacy.details.name }" />
				</td>
				
				<td>
					<c:out value="${ pharmacy.details.phone1 }" />
				</td>
				
				<td>				
					<c:out value="${ pharmacy.details.fax }" />
				</td>
				
				<td class="placeHolderDonotRemove"></td>
			</tr>
		</c:forEach>
		<tr><td class="lastRow" colspan="9"></td></tr>
</table>

<table id="addEditHealthCareTeam" class="${ param.view }View" >	
<tr>
<td class="placeHolderDonotRemove"></td>
<td colspan="7" >
<table>	
		<%-- ADD NEW MEMBER TO HEALTH CARE TEAM --%>
<tr><td class="firstRow" colspan="9"></td></tr>
		<tr>		
			<td class="alignLeft">
				<select id="searchInternalExternal" >
					<option value="${ providerType }" >internal</option>
		            <option value="${ professionalContactType }" >external</option>
				</select>			
			</td>
			
			<!-- If Internal list, then display the Internal Demographic options
			 External, then display the external search options -->

			 <td class="internal" >
			 	<select id="internalProviderList">
			 		<c:forEach items="${ providerList }" var="providerDetail" >
			 			<option value="${ providerDetail.providerNo }" >
			 				<c:out value="${ providerDetail.formattedName }" />
			 				&#40;<c:out value="${ providerDetail.specialty }" />&#41;
			 			</option>
			 		</c:forEach>		 
				</select>
			 </td>
			 
			 <td class="internal">
				<input type="button" id="addHealthCareTeamButton" value="Add" />						
			</td>
			
			<td class="external" >
				<select id="selectHealthCareTeamRoleType" >					
					<c:forEach items="${ specialty }" var="specialtyType">						
						<option value="${ specialtyType.id }" ${ specialtyType.specialty eq 'UNKNOWN' ? 'selected' : '' } > 						 
							<c:out value="${ specialtyType.specialty }" />			
						</option>
					</c:forEach>
				</select>
			</td>
			
			<td class="external" >
				<input type="text" placeholder="search name" id="searchHealthCareTeamInput" value="" />
			</td>
				
		</tr>
	<%-- END MANAGE PATIENTS HEALTH CARE TEAM  --%>
</table>
</td>
<td id="referralSourceTable">
<table>
	<tr>
		<td colspan="2" class="firstRow"></td>
	</tr>
	<tr>
		<td>
			<label for="referralSource">
				<strong>Referral Source</strong>
			</label>
		</td>
		<td style="vertical-align:middle;">
			<select name="referralSource" id="referralSource" >
				<c:choose>
					<c:when test="${ not empty referralSourceLookupList }">
						<c:forEach items="${ referralSourceLookupList}" var="referralSourceItem">
							<option value="${ referralSourceItem.id }" id="${ referralSourceItem.value }" ${ referralSourceItem.id eq referralSource.id ? 'selected' : '' }>
								<c:out value="${ referralSourceItem.label }" />
							</option>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<option value="-1">Other</option>
					</c:otherwise>
				</c:choose>	
			</select>
		</td>
	</tr>
</table>
</td>
<td class="placeHolderDonotRemove"></td>
</tr>
	<tr class="external" >
	<td class="placeHolderDonotRemove"></td>
		<td colspan="8" >
			<a href="Javascript:void(0);" id="addProContact" class="createAction" >Create New External Provider</a>
			&nbsp;
			<a href="Javascript:void(0);" id="addPharmacyInfo" class="createAction" >Create New Pharmacy</a>
		</td>
	<td class="placeHolderDonotRemove"></td>
	</tr>
	<tr>
	<td class="lastRow" colspan="10"></td></tr>
</table>

<%-- DETACHED VIEW ENABLED  --%>
<c:if test="${ param.view eq 'detached' }">
	</td></tr>
	</table>
	</body>
	</html>
</c:if>
<%-- END DETACHED VIEW ENABLED  --%>


</security:oscarSec>
