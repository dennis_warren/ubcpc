<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%
  if (session.getAttribute("user") == null) {
    response.sendRedirect("../logout.jsp");
  }
%>
<%@page import="oscar.OscarProperties"%>
<%@ include file="/taglibs.jsp"%>
<%@ page import="java.util.Properties"%>
<%@ page import="java.util.List, org.oscarehr.util.SpringUtils" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%
  
  String msg = "Enter contact details.";

%>
<html:html locale="true">
<head>
<title>Add/Edit Contact</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">  
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" type="text/css">
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js" ></script>
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/library/bootstrap/3.0.0/css/bootstrap.css" />

<script type="text/JavaScript">

		function setfocus() {
			  this.focus();
			  captureParameters(this);
			  forwardToParent();
		}

		//--> get forwarded parameters
		function captureParameters(id) {
			var method = "${ param.method }";
			if( method == "addContact" ) {
				document.getElementById("lastName").value = "${ param.ecLastName }";
				document.getElementById("firstName").value = "${ param.ecFirstName }";
				document.getElementById("residencePhone").value = "${ param.ecResidencePhone }";
				document.getElementById("contact.role").value = "${ param.ecRelationship }";
			}
		}

		//--> Get parameters sent from servlet on postback and then send to parent window.
		function forwardToParent() {
			
			var id = '${ requestScope.contactId }'; // server returns the id that was saved.

			if( id ) {
				
				var data = new Object();				
				data.id = id;
				data.contactRole = '${ requestScope.contactRole }';
				data.demographicContactId = '${ requestScope.demographicContactId }';
				data.method = "saveManage";

				window.opener.contactPopUpData( data );					
			} 
		}

	    function onSearch() {
	        var ret = checkreferral_no();
	        return ret;
	    }

	    function verifyContact() {
	    	if( checkAllFields() ) {
	    		document.contactForm.submit();
	    	} else {
	    		jQuery("#requiredError").show(); 
	    	}
		}


		function checkAllFields() {
			
			var verified = true;
			var fields = document.forms[0].elements;			
			var fieldname;
			var fieldvalue;
			var fieldobject;
			
			for( var i = 0; i < fields.length; i++ ) {
				
				fieldobject = fields[i];
				fieldname = fieldobject.id;
				fieldvalue = fieldobject.value.trim();
	
				if( fieldname == "lastName" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);	
				}
				
				if( fieldname == "firstName" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);
				} 

				if( fieldname == "residencePhone" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);
				}
				
			}
			
			return verified;
	    }
		
		function paintErrorField( fieldobject ) {
			fieldobject.style.border = "medium solid red";
		}
		
	    function isNumber(s){
	        var i;
	        for (i = 0; i < s.length; i++){
	            // Check that current character is number.
	            var c = s.charAt(i);
	            if (c == ".") continue;
	            if (((c < "0") || (c > "9"))) return false;
	        }
	        // All characters are numbers.
	        return true;
	    }

	    jQuery(document).ready( function() {
	    	jQuery(".phoneNumberField").focusout(function(){

	        	var elementId = "#" + this.id;
	        	var elementValue = this.value;
	        	var newNumber;
	        	
	        	elementValue = elementValue.replace(/ /g,'');
	        	elementValue = elementValue.replace(/\D/g,''); 
	       	
	            if ( elementValue.length == 10 ) {
	    	        newNumber = "(" + elementValue.substring(0,3) + ") " + elementValue.substring(3,6) + "-" + elementValue.substring(6);
	            	jQuery(elementId).attr('value', newNumber );
	            }
	            
	            if ( elementValue.length == 11 ) {
	                newNumber = elementValue.substring(0,1) + " (" + elementValue.substring(1,4) + ") " + elementValue.substring(4,7) + "-" + elementValue.substring(7);
	            	jQuery(elementId).attr('value', newNumber);
	            }
	       	
	        });
	    })

</script>
<style type="text/css" >

	body {
		font-size:small;
	}
	
	label {
		margin:0px;
		padding:0px;
	}

	.form-group {
		margin-bottom:0px;
	}
	
	label.required:after {content: " *"; color: red;}
	
</style>
</head>
<body onLoad="setfocus()" >

<div class="container">
<html:form action="/demographic/Contact.do" >
	<div class="panel panel-default">
	
		<div class="panel-heading">
			<h4 class="panel-title">
			<%=msg%>
			</h4>
		</div>
	
		<input type="hidden" name="role" value="${ ecRelationship }"/>
		<input type="hidden" name="demographicContactId" id="demographicContactId" value="${ demographicContactId }"/>
		<input type="hidden" name="contact.id" value="${ contact.id }" />
		<input type="hidden" name="method" value="saveContact" />
		
		<div class="panel-body">
		
		<div class="form-group">
		
			<label class="required" for="contact.firstName">First Name</label>
			
			<input class="form-control" type="text" name="contact.firstName" id="firstName" value="<c:out value="${contact.firstName}"/>" >
			
		</div>
		
		<div class="form-group">
			<label class="required" for="contact.lastName">Last Name</label>
			
				<input class="form-control" type="text" name="contact.lastName" id="lastName" 
					value="${ contact.lastName }" />
			
		</div>
		
		<div class="form-group">
			<label for="contact.address">Address</label>
			
				<input class="form-control" type="text" name="contact.address" id="contact.address" value="<c:out value="${contact.address}"/>" />
			
		</div>
		<div class="form-group">
			<label for="contact.address2">Address2</label>
			
				<input class="form-control" type="text" name="contact.address2" id="contact.address2" value="<c:out value="${contact.address2}"/>" />
			
		</div>
		<div class="form-group">
			<label for="contact.city">City</label>
			
				<input class="form-control" type="text" name="contact.city" id="contact.city" value="<c:out value="${contact.city}"/>" >
			
		</div>
		<div class="form-group">
			<label for="contact.province">Province</label>
				
			
			<c:set var="select" value="${ region }" scope="page" />
			<c:if test="${ not empty contact.province }" >
				<c:set var="select" value="${ contact.province }" />
			</c:if>
	
	        <select class="form-control" name="contact.province" id="contact.province">
				<option value="AB" ${ pageScope.select eq 'AB' ? 'selected' : '' }>AB-Alberta</option>
				<option value="BC" ${ pageScope.select eq 'BC' ? 'selected' : '' }>BC-British Columbia</option>
				<option value="MB" ${ pageScope.select eq 'MB' ? 'selected' : '' }>MB-Manitoba</option>
				<option value="NB" ${ pageScope.select eq 'NB' ? 'selected' : '' }>NB-New Brunswick</option>
				<option value="NL" ${ pageScope.select eq 'NL' ? 'selected' : '' }>NL-Newfoundland & Labrador</option>
				<option value="NT" ${ pageScope.select eq 'NT' ? 'selected' : '' }>NT-Northwest Territory</option>
				<option value="NS" ${ pageScope.select eq 'NS' ? 'selected' : '' }>NS-Nova Scotia</option>
				<option value="NU" ${ pageScope.select eq 'NU' ? 'selected' : '' }>NU-Nunavut</option>
				<option value="ON" ${ pageScope.select eq 'ON' ? 'selected' : '' }>ON-Ontario</option>
				<option value="PE" ${ pageScope.select eq 'PE' ? 'selected' : '' }>PE-Prince Edward Island</option>
				<option value="QC" ${ pageScope.select eq 'QC' ? 'selected' : '' }>QC-Quebec</option>
				<option value="SK" ${ pageScope.select eq 'SK' ? 'selected' : '' }>SK-Saskatchewan</option>
				<option value="YT" ${ pageScope.select eq 'YT' ? 'selected' : '' }>YT-Yukon</option>
				<option value="US" ${ pageScope.select eq 'US' ? 'selected' : '' }>US resident</option>
			</select> 
			
			
			
		</div>
		<div class="form-group">
			<label for="contact.country" >Country </label>
			<input class="form-control" type="text" name="contact.country"  id="contact.country" value="${contact.country}" />
		</div>
		
		<div class="form-group">
			<label for="contact.postal">Postal</label>
			
				<input class="form-control" type="text" name="contact.postal" id="contact.postal" value="<c:out value="${contact.postal}"/>" >
			
		</div>
		<div class="form-group">
			<label class="required" for="residencePhone">Res. Phone</label>
			
				<input class="form-control phoneNumberField" type="text" name="contact.residencePhone" id="residencePhone" value="${contact.residencePhone}" >
			
		</div>
		<div class="form-group">
			<label for="cellPhone">Cell Phone</label>
			
				<input class="form-control phoneNumberField" type="text" name="contact.cellPhone" id="cellPhone" value="${contact.cellPhone}" >
			
		</div>	
		<div class="form-group">
			<label for="contact.workPhone">Work Phone</label>
			
				<input class="form-control phoneNumberField" type="text" name="contact.workPhone" id="workPhone" value="${contact.workPhone}" />
			Ext: <input class="form-control" type="text" name="contact.workPhoneExtension" value="${contact.workPhoneExtension}"/>
			
		</div>
		<div class="form-group">
			<label for="contact.fax">Fax</label>
			
				<input class="form-control phoneNumberField" type="text" name="contact.fax" id="fax" value="${contact.fax}" >
			
		</div>	
		<div class="form-group">
			<label for="contact.email">Email</label>
			
				<input class="form-control" type="email" name="contact.email" id="contact.email" value="${contact.email}" >
			
		</div>
		
		<div class="form-group">
			<label for="contact.note">Note</label>
			
				<input class="form-control" type="text" name="contact.note" value="${contact.note}" >
			
		</div>
		
			
		<div class="form-group">
			<label for="contact.role" >Relationship</label>
	
			<select class="form-control" id="contact.role" name="contact.role" >					
				<c:forEach items="${ relationships }" var="relationship">			
					<option value="${ relationship.value }" ${ relationship.value eq ecRelationship ? 'selected' : '' } >  
						<c:out value="${ relationship.label }" />
					</option>
				</c:forEach>
			</select>
		</div>
		
		</div>
		<div class="panel-footer">
			<button class="btn btn-default" type="button" name="submitBtn" onClick="verifyContact();" >
				<bean:message key="admin.resourcebaseurl.btnSave"/>
			</button> 			
			<button class="btn btn-default" type="button" name="Cancel" onClick="window.close()" >
				<bean:message key="admin.resourcebaseurl.btnExit"/>
			</button>
			
			<span id="requiredError" style="display:none;color:red;">
				Missing Required Fields
			</span>
		</div>	<!-- end panel footer -->
	</div><!-- end panel -->
</html:form>

</body>
</html:html>
