<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%--
Example Usage

<jsp:include page="./includes/_provinceStateSelectList.jsp">
	<jsp:param value="<%= hctype %>" name="selectedProvince"/>
	<jsp:param value="<%= pNames %>" name="provinceList"/>
	<jsp:param value="hc_type" name="selectListName" />
	<jsp:param value="hc_type" name="selectListId" />
</jsp:include>
--%>


<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="oscar.OscarProperties" %>

<c:set var="selectedProvince" value="${ param.selectedProvince }"/>
<c:set var="provinces" value='<%= OscarProperties.getInstance().getPropertyMap("province_names") %>' scope="page" />

<select class="form-control" name="${ param.selectListName }" id="${ param.selectListId }" <c:out value="${ param.disabledProvince }" /> >

	<c:if test="${ empty selectedProvince }" >
		<c:set var="selectedProvince" value="OT"/>
	</c:if>
	
	<c:if test="${ fn:length( selectedProvince ) gt 2 }" >
		<c:set var="selectedProvince" value="OT"/>
	</c:if>
	
	<option value="OT" ${ selectedProvince eq 'OT' ? 'selected' : '' } >
		Other
	</option>
		
	<c:choose>
	<c:when test="${ not empty pageScope.provinces }">	
		<c:forEach items="${ pageScope.provinces }" var="province" varStatus="iteration">
			<option value="${ province.key }" ${ selectedProvince eq province.key ? 'selected' : '' } >
				<c:out value="${ province.value }" />
			</option>
		</c:forEach>	
	</c:when>
	<c:otherwise>
		<option value="AB" <%="AB".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>AB-Alberta</option>
		<option value="BC" <%="BC".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>BC-British Columbia</option>
		<option value="MB" <%="MB".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>MB-Manitoba</option>
		<option value="NB" <%="NB".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>NB-New Brunswick</option>
		<option value="NL" <%="NL".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>NL-Newfoundland Labrador</option>
		<option value="NT" <%="NT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>NT-Northwest Territory</option>
		<option value="NS" <%="NS".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>NS-Nova Scotia</option>
		<option value="NU" <%="NU".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>NU-Nunavut</option>
		<option value="ON" <%="ON".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>ON-Ontario</option>
		<option value="PE" <%="PE".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>PE-Prince Edward Island</option>
		<option value="QC" <%="QC".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>QC-Quebec</option>
		<option value="SK" <%="SK".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>SK-Saskatchewan</option>
		<option value="YT" <%="YT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>YT-Yukon</option>
		<option value="US" <%="US".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US resident</option>
		<option value="US-AK" <%="US-AK".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-AK-Alaska</option>
		<option value="US-AL" <%="US-AL".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-AL-Alabama</option>
		<option value="US-AR" <%="US-AR".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-AR-Arkansas</option>
		<option value="US-AZ" <%="US-AZ".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-AZ-Arizona</option>
		<option value="US-CA" <%="US-CA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-CA-California</option>
		<option value="US-CO" <%="US-CO".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-CO-Colorado</option>
		<option value="US-CT" <%="US-CT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-CT-Connecticut</option>
		<option value="US-CZ" <%="US-CZ".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-CZ-Canal Zone</option>
		<option value="US-DC" <%="US-DC".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-DC-District Of Columbia</option>
		<option value="US-DE" <%="US-DE".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-DE-Delaware</option>
		<option value="US-FL" <%="US-FL".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-FL-Florida</option>
		<option value="US-GA" <%="US-GA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-GA-Georgia</option>
		<option value="US-GU" <%="US-GU".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-GU-Guam</option>
		<option value="US-HI" <%="US-HI".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-HI-Hawaii</option>
		<option value="US-IA" <%="US-IA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-IA-Iowa</option>
		<option value="US-ID" <%="US-ID".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-ID-Idaho</option>
		<option value="US-IL" <%="US-IL".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-IL-Illinois</option>
		<option value="US-IN" <%="US-IN".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-IN-Indiana</option>
		<option value="US-KS" <%="US-KS".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-KS-Kansas</option>
		<option value="US-KY" <%="US-KY".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-KY-Kentucky</option>
		<option value="US-LA" <%="US-LA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-LA-Louisiana</option>
		<option value="US-MA" <%="US-MA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MA-Massachusetts</option>
		<option value="US-MD" <%="US-MD".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MD-Maryland</option>
		<option value="US-ME" <%="US-ME".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-ME-Maine</option>
		<option value="US-MI" <%="US-MI".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MI-Michigan</option>
		<option value="US-MN" <%="US-MN".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MN-Minnesota</option>
		<option value="US-MO" <%="US-MO".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MO-Missouri</option>
		<option value="US-MS" <%="US-MS".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MS-Mississippi</option>
		<option value="US-MT" <%="US-MT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-MT-Montana</option>
		<option value="US-NC" <%="US-NC".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NC-North Carolina</option>
		<option value="US-ND" <%="US-ND".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-ND-North Dakota</option>
		<option value="US-NE" <%="US-NE".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NE-Nebraska</option>
		<option value="US-NH" <%="US-NH".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NH-New Hampshire</option>
		<option value="US-NJ" <%="US-NJ".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NJ-New Jersey</option>
		<option value="US-NM" <%="US-NM".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NM-New Mexico</option>
		<option value="US-NU" <%="US-NU".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NU-Nunavut</option>
		<option value="US-NV" <%="US-NV".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NV-Nevada</option>
		<option value="US-NY" <%="US-NY".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-NY-New York</option>
		<option value="US-OH" <%="US-OH".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-OH-Ohio</option>
		<option value="US-OK" <%="US-OK".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-OK-Oklahoma</option>
		<option value="US-OR" <%="US-OR".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-OR-Oregon</option>
		<option value="US-PA" <%="US-PA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-PA-Pennsylvania</option>
		<option value="US-PR" <%="US-PR".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-PR-Puerto Rico</option>
		<option value="US-RI" <%="US-RI".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-RI-Rhode Island</option>
		<option value="US-SC" <%="US-SC".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-SC-South Carolina</option>
		<option value="US-SD" <%="US-SD".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-SD-South Dakota</option>
		<option value="US-TN" <%="US-TN".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-TN-Tennessee</option>
		<option value="US-TX" <%="US-TX".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-TX-Texas</option>
		<option value="US-UT" <%="US-UT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-UT-Utah</option>
		<option value="US-VA" <%="US-VA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-VA-Virginia</option>
		<option value="US-VI" <%="US-VI".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-VI-Virgin Islands</option>
		<option value="US-VT" <%="US-VT".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-VT-Vermont</option>
		<option value="US-WA" <%="US-WA".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-WA-Washington</option>
		<option value="US-WI" <%="US-WI".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-WI-Wisconsin</option>
		<option value="US-WV" <%="US-WV".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-WV-West Virginia</option>
		<option value="US-WY" <%="US-WY".equals( (String) pageContext.getAttribute("selectedProvince") )?" selected":""%>>US-WY-Wyoming</option>
	</c:otherwise>
	</c:choose>
</select>

