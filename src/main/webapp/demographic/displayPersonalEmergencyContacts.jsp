<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List, org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.DemographicContact" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.managers.DemographicManager" %>
<%@ page import="org.oscarehr.common.dao.CtlRelationshipsDao" %>
<%@ page import="org.oscarehr.common.model.CtlRelationships" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>

<security:oscarSec roleName="${ sessionScope.userrole }" objectName="_demographic" rights="r" reverse="${ false }">

<script type="text/javascript" >
	jQuery(document).ready( function($) {		
		//--> Popup effects
		jQuery(".personalContactList").bind( "mouseover", function(){
			nhpup.popup( jQuery('#personalEmergencyContactDetail_' + this.id).html(), { 'width':250 } );						
		})
	})
</script>

<div class="demographicSection" id="emergencyContacts" >
		<h3>&nbsp;Personal &amp; Emergency Contacts</h3>
		<ul>
		<c:forEach items="${ personalEmergencyContacts }" var="personalContact" varStatus="row">
	
			<c:set value="even" var="rowclass" scope="page" />
			<c:if test="${ row.index mod 2 ne 0 }" >
				<c:set value="odd" var="rowclass" scope="page" />
			</c:if>
		
			<li id="${ personalContact.id }" class="personalContactList ${ rowclass } highlight" >
				
				<span class="label"> 
					<c:out value="${ personalContact.role }" />					
				</span>
				<span>
					<c:out value="${ personalContact.details.lastName }" />, 
					<c:out value="${ personalContact.details.firstName }" />
				</span>
				<span>
					<strong> Ph: </strong>
					<c:out value="${ personalContact.details.residencePhone }" />
				</span>
				
				<c:if test="${ personalContact.ec }">
					<span class='emergencyLabel' >Emergency</span>
				</c:if>
			</li>
			
			<table class="personalEmergencyContactDetailTable" 
				id="personalEmergencyContactDetail_${ personalContact.id }" style="display:none;" >
			
				<tr><th class="alignLeft contactName" colspan="2"><c:out value="${ personalContact.contactName }" /></th></tr>
				<tr>
					<td class="alignRight alignTop smallText role" >Role:</td>
					<td class="alignLeft alignTop smallText role" ><c:out value="${ personalContact.role }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">Address:</td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.address }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">City:</td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.city }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">Province:</td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.province }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">Residence Phone: </td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.residencePhone }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">Cell Phone: </td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.cellPhone }" /></td>
				</tr>
			
				<tr>
					<td class="alignRight alignTop smallText">Work Phone: </td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.workPhone }" /></td>
				</tr>
				<tr>
					<td class="alignRight alignTop smallText">eMail: </td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.email }" /></td>
				</tr>
				
				<tr>
					<td class="alignRight alignTop smallText">Note: </td>
					<td class="alignLeft alignTop smallText"><c:out value="${ personalContact.details.note }" /></td>
				</tr>
			
			</table>
		</c:forEach>
		</ul>
</div>

</security:oscarSec>