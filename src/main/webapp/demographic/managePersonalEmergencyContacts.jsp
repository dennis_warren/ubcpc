<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List, org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.DemographicContact" %>
<%@ page import="org.oscarehr.managers.DemographicManager" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.dao.CtlRelationshipsDao" %>
<%@ page import="org.oscarehr.common.model.CtlRelationships" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<security:oscarSec roleName="${ sessionScope.userrole }" objectName="_demographic" rights="w" reverse="${ false }">
<script type="text/javascript" >

var demographic_no = "${ demographicNoString }";
var path = "${ oscar_context_path }/demographic/Contact.do"; 


//--> Serialize Javascript Objects
function serializeObject( object ) {
	var param;
	if( object instanceof Object ) {
		param = Object.keys( object ).map( function(key) { 
		  	return encodeURIComponent( key ) + '=' + encodeURIComponent( object[key] ); 
		}).join('&')
	}
	return param;	
}


//--> Link method to popup window.
function contactPopUpData( data ){

	if( data ) {
 
		data.demographic_no = demographic_no;
		data.postMethod = "ajax";
		data.contact_num = 1; // total number of demographic contacts to add.
		data.procontact_num = 0; // total number of professional contacts to add
		
		var param = Object.keys( data ).map( function(key) { 
			if( key !== "id" || key !== "contactRole" || key != "demographicContactId") {
		  		return encodeURIComponent( key ) + '=' + encodeURIComponent( data[key] ); 
			}
		}).join('&'); 

		var contactPrefix = 'contact_' + data.contact_num + '.';
		var contactObject = new Object();

		contactObject.active = 1;
		contactObject.contactId = data.id;
		contactObject.id = (data.demographicContactId) ? data.demographicContactId : 0;	
		contactObject.consentToContact = 1;
		contactObject.note = "";
		contactObject.type = 2; // plain Contact type.
		contactObject.role = data.contactRole; // the type, role or relationship of this contact.

		if(! contactObject.role ) {
			contactObject.role = jQuery("#ecRelationship :selected").val();
		}
		
		param += '&' + Object.keys( contactObject ).map( function(key) { 
		  	return encodeURIComponent( contactPrefix ) + encodeURIComponent( key ) + '=' + encodeURIComponent( contactObject[key] ); 
		}).join('&');
		
		sendContactData(path, param, ["#listPersonalEmergencyContacts", "#addEditPersonalEmergencyContact"]);		
	}
}

//--> Postback function
function getPostBackAttributes( html ) {
	var data = new Object();
	data.id = jQuery("#previousContactId", html).val();
	data.contactRole = jQuery("#ecRelationship :selected").val();
	data.method = "saveManage"
	contactPopUpData( data );
}

//--> Refresh containing elements 
function renderPersonalEmergencyContactResponse(html, id) {
	
	if( id instanceof Array ) {
		jQuery.each(id, function(i, val){
			jQuery(val).replaceWith( jQuery(val, html) );
		});			
	} else {			
		jQuery(id).replaceWith( jQuery(id, html) );
	}

	jQuery().bindPersonalEmergencyContactFunctions();
	
}

//--> Check fields
function checkAddContactFields( paramString ) {

	var missingFields = true;
	jQuery.each( paramString.split("&"), function( key, value ){ 
		
		var param = value.split("=")[1];
		var fieldName = value.split("=")[0];

		unpaintContactErrorField( fieldName )
		
		if( param == null || param == "" ) {
			paintContactErrorField( fieldName );
			missingFields = false;
		}
	}); 
	
	return missingFields;
}

//--> Remove error css fields
function unpaintContactErrorField( fieldobjectid ) {
	jQuery( '[name="' + fieldobjectid + '"]' ).removeClass( "missingFieldValue" );
}

//--> Mark missing fields and report error.
function paintContactErrorField( fieldobjectid ) {
	jQuery( '[name="' + fieldobjectid + '"]' ).addClass( "missingFieldValue" );
	jQuery("#errorContainer").html("Missing required fields");
}

//--> Wrapped in a function to re-bind after postback
jQuery.fn.bindPersonalEmergencyContactFunctions = function() {

	//--> Unbind just incase.
	jQuery('#detailEmergencyContactButton').unbind("click");
	jQuery('#addEmergencyContactButton').unbind("click");
	jQuery('.addEditPersonalContact').unbind("click")
	jQuery('.personalEmergencyContactField').unbind("click");
	//--> set or unset contact as emergency contact.
	jQuery('.personalEmergencyContactInput').bind("change", function(){

		var id = this.id.split("_")[1].trim();
		var data = new Object()
		data.method = "setEmergencyContact";
		data.contactId = id;
		data.setting = this.checked;
		data.demographic_no = demographic_no;
		sendContactData(path, serializeObject( data ), "");
	})
	
	//--> Get new contact form
	jQuery('#detailEmergencyContactButton').bind("click", function(){
				
		var data = new Object();
		data.method = "addContact";
		data.ecFirstName = document.getElementById("ecFirstName").value;
		data.ecLastName = document.getElementById("ecLastName").value;
		data.ecResidencePhone = document.getElementById("ecResidencePhone").value;		
		data.ecRelationship = jQuery("#ecRelationship :selected").val(); 

		sendContactData(path, serializeObject( data ), "popup");
	})
	
	//--> Quick add with minimal info
	jQuery('#addEmergencyContactButton').bind("click", function(){

		var param = jQuery('#addEditPersonalEmergencyContact :input').serialize();

		if( checkAddContactFields( param ) ) {
			
			var data = new Object()
			data.method = "saveContact";
			data.postMethod = "ajax";
			data.demographic_no = demographic_no;

			param += '&' + serializeObject( data );

			sendContactData(path, param, "postback");
		}

	})
	
	 //--> Remove/Edit contact action. 
	jQuery('.addEditPersonalContact').bind("click", function(event){			

		var directive = this.id.split("_")[0].trim();
		var id = this.id.split("_")[2].trim();
		var target = "#listPersonalEmergencyContacts";
		var proceed = true;
		var data = new Object();
		data.postMethod = 'ajax';
		data.demographic_no = demographic_no;
		
		if( directive == "remove") {
			proceed = window.confirm("Delete this contact?");
			data.contactId = id;	 
			data.method = "removeContact";		 		 
		} else if( directive == "edit") {
			target = 'popup';
			data.demographicContactId = id; 
			data.method = "editContact";			 				 
		}

		if( proceed ) {
			sendContactData(path, serializeObject( data ), target)
		}
	})
}

//--> AJAX the data to the server.
function sendContactData(path, param, target) { 

	jQuery.ajax({
	    url: path,
	    type: 'POST',
	    data: param,
	  	dataType: 'html',
	    success: function(data) {
	    	if( target == 'popup' ) {
	    		if( popupWindow ) {
	    			popupWindow.close();
				}
	    		popupWindow = window.open("", target, "scrollbars=yes, resizable=yes, width=600, height=600");
				with(popupWindow.document) {
				    open();
				    write(data);
				    close();
		    	}
				popupWindow.focus();				
	    	} else if (target == 'postback') {
	    		getPostBackAttributes( data );	    		
	    	} else {	    		
	    		renderPersonalEmergencyContactResponse(data, target);
				if( popupWindow ) {
	    			popupWindow.close();
				}				
	    	}	    	

	    }
	});
}



//--> Document Ready Methods 
jQuery(document).ready( function() {
	//--> Intial jQuery event bindings
	jQuery().bindPersonalEmergencyContactFunctions();

})

</script>

<input type="hidden" id="previousContactId" value="${ requestScope.contactId }" />

<table id="listPersonalEmergencyContacts"  >
		<tr id="tableTitle" >
			<th colspan="8" class="alignLeft" >Personal Contact(s)</th>
		</tr>
		<tr><td class="firstRow" colspan="8"></td></tr>

		<c:if test="${ not empty personalEmergencyContacts }" >
			<tr id="personalEmergencyContactsSubHeading" >
				<td class="firstColumn" >&nbsp;</td><td></td><td></td><td></td>
				<td>Relationship</td><td>Name</td><td>Phone</td><td class="lastColumn">&nbsp;</td>
			</tr>
		
		<c:forEach items="${ personalEmergencyContacts }" var="personalEmergencyContact" >		

			<tr class="highlight" >
				<td class="placeHolderDonotRemove"></td>
				<td class="deleteContactField contactTool" title="Remove Contact" >
					<a href="javascript:void(0)" id="remove_PersonalEmergencyContact_${ personalEmergencyContact.id }" 
						class="addEditPersonalContact"  >
						<img src="${ oscar_context_path }/images/delete.png" width="16" height="16" alt="remove" />
					</a>
				</td>
				<td class="editContactField contactTool" title="Edit Contact" >
					<a href="javascript:void(0)" id="edit_PersonalEmergencyContact_${ personalEmergencyContact.id }" 
						class="addEditPersonalContact" >
						<img src="${ oscar_context_path }/images/edit.png" width="16" height="16" alt="edit" />
					</a>
				</td>
				<td class="emergencyContactField contactTool" title="Assign as primary emergency contact" >
					<input class="personalEmergencyContactInput" type="checkbox" 
					id="emergencyContactSwitch_${ personalEmergencyContact.id }"  
					${ personalEmergencyContact.ec == 'true' ? 'checked' : '' } />
	
					<label for="emergencyContactSwitch_${ personalEmergencyContact.id }" ${ personalEmergencyContact.ec ? 'style="color:red;"' : '' } >
						EMG
					</label>
				</td>

				<td>	
					<c:out value="${ personalEmergencyContact.role }" />				 					
				</td>
                <td>
                	<c:out value="${ personalEmergencyContact.details.lastName }" />, 
                	<c:out value="${ personalEmergencyContact.details.firstName }" />
                </td>
                <td class="contactPhoneNumberOutput" >
                	<c:out value="${ personalEmergencyContact.details.residencePhone }" />
                </td>
                
        		<td class="placeHolderDonotRemove"></td>
			</tr>
			<tr><td id="contactDetailContainer" colspan="8" style="display:none;"></td></tr>			
		</c:forEach>
		</c:if>
<tr><td class="lastRow" colspan="8"></td></tr>
</table>

<table id="addEditPersonalEmergencyContact" >
<tr><td class="firstRow" colspan="8"></td></tr>
	<tr>
		<td class="placeHolderDonotRemove"></td>
		<td class="label" >First Name:</td>
		<td> <input type="text" name="contact.firstName" id="ecFirstName" value="${ contact.firstName }" /></td>
		<td class="label" >Last Name:</td>
		<td colspan="2">
			<input type="text" name="contact.lastName" id="ecLastName"	value="${ contact.lastName }" />
		</td>
		<td class="placeHolderDonotRemove"></td>
	</tr>
	<tr>
	<td class="placeHolderDonotRemove"></td>
		<td class="label" >Primary Phone:</td>
		<td><input type="text" id="ecResidencePhone" name="contact.residencePhone" value="${ contact.residencePhone }" /></td>
		<td class="label" >Relationship:</td>
		<td>
		<select id="ecRelationship" name="contact.role" >					
			<c:forEach items="${ relationships }" var="relationship">			
				<option value="${ relationship.value }" ${ relationship.value eq param.ecRole ? 'selected' : '' } >  
					<c:out value="${ relationship.label }" />
				</option>
			</c:forEach>
		</select>
		
			<a href="Javascript:void(0);" id="detailEmergencyContactButton" >
				more &hellip;
			</a>
		</td>
		<td id="emergencyContactsButtonSet" >
			<input type="button" value="add" name="addEmergencyContactButton" id="addEmergencyContactButton" />					
		</td>
		<td class="placeHolderDonotRemove"></td>
	</tr>
	<tr>
	<td class="placeHolderDonotRemove"></td>		
		<td id="errorContainer" colspan="5"></td>
	<td class="placeHolderDonotRemove"></td>
	</tr>
	<tr><td class="lastRow" colspan="8"></td></tr>

</table>

</security:oscarSec>