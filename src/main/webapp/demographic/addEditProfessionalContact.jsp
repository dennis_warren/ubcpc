<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%
  if (session.getAttribute("user") == null) {
    response.sendRedirect("../logout.jsp");
  }
%>
<%@page import="oscar.OscarProperties"%>
<%@ include file="/taglibs.jsp"%>
<%@ page import="java.util.Properties"%>
<%@ page import="java.util.List, org.oscarehr.util.SpringUtils" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.oscarehr.common.dao.ContactSpecialtyDao" %>
<%@ page import="org.oscarehr.common.model.ContactSpecialty" %>

<html:html locale="true">

<head>
<title>Add/Edit Professional Contact</title>

<meta name="viewport" content="width=device-width,initial-scale=1.0" />
  
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" type="text/css" />
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/library/bootstrap/3.0.0/css/bootstrap.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js" ></script>    
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap/3.0.0/js/bootstrap.min.js" ></script>
        
<script type="text/javascript">

//<!--

var contact_exists = '${ existing_contact_found }';
var id = '${ requestScope.id }'; // server returns the id that was saved.
var demographic_no = '${ requestScope.demographic_no }';
	
		function setfocus() {
			
			this.window.focus();
			captureParameters(this);
			
			if(contact_exists) {
				jQuery('#existingContactModal').modal('show');
			} else {
				forwardToParent();
			}		  			  		
		}
		
		function captureParameters(id) {
			
			var keyword = '${ param.keyword }';
			var keywordLastName = null;
			var keywordFirstName = null;
			var firstName = '${ pcontact.firstName }';
			var lastName = '${ pcontact.lastName }';
			
			if( keyword && keyword.contains(",") ) {
				keywordLastName = keyword.split(",")[0].trim();
				keywordFirstName = keyword.split(",")[1].trim();
			} else if( keyword ) {
				keywordLastName = keyword;
			}

			if( ! lastName ) {
				document.getElementById("pcontact.lastName").value = keywordLastName;
			}
			if( ! firstName ) {
				document.getElementById("pcontact.firstName").value = keywordFirstName;
			}
		}

		function forwardToParent() {
			if( id ) {
				try {
					window.opener.reloadHealthCareTeam(id, demographic_no);
				} catch(e) {
					// do nothing if function not available.
				}
			}
		}
		
	    function onSearch() {
	        var ret = checkreferral_no();
	        return ret;
	    }
	    
	    function onProContactSave() {
	    	
	    	if( checkAllProContactFields() ) {
	    		jQuery("#addEditProfessionalForm").submit();
	    	} else {
	    		jQuery("#requiredError").show(); 
	    	}

	    }
		
		function checkAllProContactFields() {
			
			var verified = true;
			var fields = document.forms[0].elements;			
			var fieldname;
			var fieldvalue;
			var fieldobject;
			
			for( var i = 0; i < fields.length; i++ ) {
				
				fieldobject = fields[i];
				fieldname = fieldobject.id;
				fieldvalue = fieldobject.value.trim();
	
				if( fieldname == "pcontact.lastName" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);	
				}
				
				if( fieldname == "pcontact.firstName" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);
				} 

				if( fieldname == "workPhone" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);
				}
				
				if( fieldname == "pcontact.cpso" && fieldvalue.length == 0 ) {
					verified = false;
					paintErrorField(fieldobject);
				}
				
			}
			
			return verified;
	    }
		
		function paintErrorField( fieldobject ) {
			fieldobject.style.border = "medium solid red";
		}
		
	    function isNumber(s){
	        var i;
	        for (i = 0; i < s.length; i++){
	            // Check that current character is number.
	            var c = s.charAt(i);
	            if (c == ".") continue;
	            if (((c < "0") || (c > "9"))) return false;
	        }
	        // All characters are numbers.
	        return true;
	    }

	    jQuery(document).ready( function() {
	    	
	    	// format phone number entries.
	        jQuery(".phoneNumberField").focusout(function(){

	        	var elementId = "#" + this.id;
	        	var elementValue = this.value;
	        	var newNumber;
	        	
	        	elementValue = elementValue.replace(/ /g,'');
	        	elementValue = elementValue.replace(/\D/g,''); 
	       	
	            if ( elementValue.length == 10 ) {
	    	        newNumber = "(" + elementValue.substring(0,3) + ") " + elementValue.substring(3,6) + "-" + elementValue.substring(6);
	            	jQuery(elementId).attr('value', newNumber );
	            }
	            
	            if ( elementValue.length == 11 ) {
	                newNumber = elementValue.substring(0,1) + " (" + elementValue.substring(1,4) + ") " + elementValue.substring(4,7) + "-" + elementValue.substring(7);
	            	jQuery(elementId).attr('value', newNumber);
	            }
	        })
	        
	        // put all the contact values back into the form if the user
			// cancels the update existing contact option
			jQuery("#cancel-modal").click(function(){
				jQuery("#editExistingContact .edit-contact").each(function(){
					var editValue = jQuery(this).val();
					var editName = jQuery(this).attr("name");
					jQuery( "#addEditProfessionalForm input[name*='" + editName + "']" ).val(editValue);
				})
			})
			
	    })
//-->

</script>
<style type="text/css" >

	body {
		font-size:small;
	}
	
	label, form {
		margin:0px;
		padding:0px;
	}

	#existingContactModal label {
		width:70px;
	}

	.form-group{
		margin-bottom:0px;
	}
	
	label.required:after {content: " *"; color: red;}
	
	
</style>
</head>

<body onLoad="setfocus();" >

<div class="container">
<html:form action="/demographic/Contact.do" styleId="addEditProfessionalForm">
<div class="panel panel-default">

	<div class="panel-heading">
		<h4 class="panel-title">
		<c:out value="${ pcontact.id gt 0 ? 'Edit' : 'Add' }" />
		Professional Contact
		</h4>
	</div>

	<c:if test="${ pcontact.id gt 0 }" >
		<input type="hidden" name="pcontact.id" value="${ pcontact.id }" />
	</c:if>
	<input type="hidden" name="method" value="saveProContact"/>
	<input type="hidden" name="demographic_no" id="demographic_no" value="${ param.demographic_no }"/>
	<input type="hidden" name="demographicContactId" id="demographicContactId" value="${ requestScope.demographicContactId }"/>
	<input type="hidden" name="keywordFirstName" id="keywordFirstName" value=""/>
	<input type="hidden" name="keywordLastName" id="keywordLastName" value="" />

	<%-- this can be 2 for professional contact or 3 for professional specialist --%>
	<input type="hidden" name="contactType" id="contactType" value="${ param.contactType }"/>
	
	<div class="panel-body">
	
		<div class="form-group">
			<label class="required"  for="pcontact.firstName">First Name</label>
			
				<input class="form-control" type="text" name="pcontact.firstName" id="pcontact.firstName" value="<c:out value="${pcontact.firstName}"/>" >
			
		</div>
		
		<div class="form-group">
			<label class="required" for="pcontact.lastName">Last Name</label>
			
				<input class="form-control" type="text" name="pcontact.lastName" id="pcontact.lastName" 
					value="${ pcontact.lastName }" />
			
		</div>
		
		<div class="form-group">
			<label class="required"  for="pcontact.cpso">
				CPSO ID / MSP No. 
				<oscar:oscarPropertiesCheck property="billregion" value="BC">
				&nbsp;&nbsp;
				<a target="_blank" id="checkDirectory" href="https://www.cpsbc.ca/physician_search?filter_first_name=&filter_last_name=" >
					Check College Directory
				</a>
				</oscar:oscarPropertiesCheck>
			</label>
			
				<input class="form-control" type="text" name="pcontact.cpso" id="pcontact.cpso" value="${pcontact.cpso}" >
			
		</div>
		
		<div class="form-group">
			<label for="pcontact.specialty">Specialty</label>
						
			<oscar:oscarPropertiesCheck property="DEMOGRAPHIC_PATIENT_HEALTH_CARE_TEAM" value="true">	
			
						<select class="form-control" id="pcontact.specialty" name="pcontact.specialty" >
							<option value="25" >*</option>					
							<c:forEach items="${ specialties }" var="specialtyType">			
								<option value="${ specialtyType.id }" ${ specialtyType.id eq requestScope.contactRole ? 'selected' : '' } >  
									<c:out value="${ specialtyType.specialty }" />
								</option>
							</c:forEach>
						</select>
			</oscar:oscarPropertiesCheck>
			<oscar:oscarPropertiesCheck property="DEMOGRAPHIC_PATIENT_HEALTH_CARE_TEAM" value="false">			
						<input class="form-control" type="text" name="pcontact.specialty" value="${ pcontact.specialty }" >
			</oscar:oscarPropertiesCheck>
						
		</div>		
		
		<div class="form-group">
			<label for="pcontact.address">Address</label>
			
				<input class="form-control" type="text" name="pcontact.address" id="pcontact.address" value="<c:out value="${pcontact.address}"/>" />
			
		</div>
		<div class="form-group">
			<label for="pcontact.address2">Address2</label>
			
				<input class="form-control" type="text" name="pcontact.address2" id="pcontact.address2" value="<c:out value="${pcontact.address2}"/>" />
			
		</div>
		<div class="form-group">
			<label for="pcontact.city">City</label>
			
				<input class="form-control" type="text" name="pcontact.city" id="pcontact.city" value="<c:out value="${pcontact.city}"/>" >
			
		</div>
		
		<div class="form-group">
			<label for="pcontact.postal">Postal</label>
			
				<input class="form-control" type="text" name="pcontact.postal" id="pcontact.postal" value="${pcontact.postal}" />
			
		</div>
		
		<div class="form-group">
			<label for="pcontact.province">Province</label>
				
			
			<c:set var="select" value="${ region }" scope="page" />
			<c:if test="${ not empty pcontact.province }" >
				<c:set var="select" value="${ pcontact.province }" />
			</c:if>
	
	        <select class="form-control" name="pcontact.province" id="pcontact.province">
				<option value="AB" ${ pageScope.select eq 'AB' ? 'selected' : '' }>AB-Alberta</option>
				<option value="BC" ${ pageScope.select eq 'BC' ? 'selected' : '' }>BC-British Columbia</option>
				<option value="MB" ${ pageScope.select eq 'MB' ? 'selected' : '' }>MB-Manitoba</option>
				<option value="NB" ${ pageScope.select eq 'NB' ? 'selected' : '' }>NB-New Brunswick</option>
				<option value="NL" ${ pageScope.select eq 'NL' ? 'selected' : '' }>NL-Newfoundland & Labrador</option>
				<option value="NT" ${ pageScope.select eq 'NT' ? 'selected' : '' }>NT-Northwest Territory</option>
				<option value="NS" ${ pageScope.select eq 'NS' ? 'selected' : '' }>NS-Nova Scotia</option>
				<option value="NU" ${ pageScope.select eq 'NU' ? 'selected' : '' }>NU-Nunavut</option>
				<option value="ON" ${ pageScope.select eq 'ON' ? 'selected' : '' }>ON-Ontario</option>
				<option value="PE" ${ pageScope.select eq 'PE' ? 'selected' : '' }>PE-Prince Edward Island</option>
				<option value="QC" ${ pageScope.select eq 'QC' ? 'selected' : '' }>QC-Quebec</option>
				<option value="SK" ${ pageScope.select eq 'SK' ? 'selected' : '' }>SK-Saskatchewan</option>
				<option value="YT" ${ pageScope.select eq 'YT' ? 'selected' : '' }>YT-Yukon</option>
				<option value="US" ${ pageScope.select eq 'US' ? 'selected' : '' }>US resident</option>
			</select> 
	
		</div>
		<div class="form-group">
			<label for="pcontact.country" >Country </label>
			<input class="form-control" type="text" name="pcontact.country"  id="pcontact.country" value="${pcontact.country}" />
		</div>
		
	
		
		<div class="form-group">
			<label class="required" for="pcontact.workPhone">Work Phone</label>
			
				<input class="form-control phoneNumberField" type="text" name="pcontact.workPhone" id="workPhone" value="${pcontact.workPhone}" />
			Ext: <input class="form-control" type="text" name="pcontact.workPhoneExtension" value="${pcontact.workPhoneExtension}"/>
			
		</div>
		<div class="form-group">
			<label for="pcontact.fax">Fax</label>
			
				<input class="form-control phoneNumberField" type="text" name="pcontact.fax" id="fax" value="${pcontact.fax}" >
			
		</div>
			
		<div class="form-group">
			<label for="pcontact.email">Email</label>
			
				<input class="form-control" type="email" name="pcontact.email" id="pcontact.email" value="${pcontact.email}" >
			
		</div>
	
		<!-- disable these fields if the contact type is a Professional Specialist -->
		<div class="form-group">
			<label for="residencePhone">Private Phone</label>
			
				<input class="form-control phoneNumberField" type="text" name="pcontact.residencePhone" id="residencePhone" value="${pcontact.residencePhone}" >
			
		</div>
		<div class="form-group">
			<label for="cellPhone">Cell Phone / Pager</label>
			
				<input class="form-control phoneNumberField" type="text" name="pcontact.cellPhone" id="cellPhone" value="${pcontact.cellPhone}" >
			
		</div>	
	
		
		<c:if test="${ not param.contactType eq '3' }">
		
			<div class="form-group">
				<label for="pcontact.systemId">System Id</label>
				
					<input class="form-control" type="text" name="pcontact.systemId" 
					value="${pcontact.systemId}" />
				
			</div>	
			
			<div class="form-group">
				<label for="pcontact.note">Note</label>
				
					<input class="form-control" type="text" name="pcontact.note" value="${pcontact.note}" >
				
			</div>	
		</c:if>
	
	</div><!-- end panel body -->
	
	<div class="panel-footer">
		
			<button class="btn btn-default" type="button" name="submitbtn" onclick="javascript: onProContactSave();">
				<bean:message key="admin.resourcebaseurl.btnSave"/>
			</button> 			
			<button class="btn btn-default" type="button" name="cancelbtn" onClick="window.close()">
				<bean:message key="admin.resourcebaseurl.btnExit"/>
			</button>
			<span id="requiredError" style="display:none;color:red;">
				Missing Required Fields
			</span>

	</div>	<!-- end panel footer -->
</div> <!--  end panel -->

</html:form>


<!-- Modal window for existing contact found -->

<div id="existingContactModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Existing contact found</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-6">
			
					<div class="row">
						<h4>Update existing contact</h4>
						<label>Name</label>
						<c:out value="${ existing_contact_found.firstName }" />
						<c:out value="${ existing_contact_found.lastName }" />
					</div>
					<div class="row">	
						<label>CPSO</label>
						<c:out value="${ existing_contact_found.cpso }" />
					</div>
					<div class="row">
					<label>Specialty</label>
						<c:out value="${ existing_contact_found.specialty }" />
					</div>
					<div class="row">
					<label>Address</label>
						<c:out value="${ existing_contact_found.address }" />
						<c:out value="${ existing_contact_found.address2 }" />,
						<c:out value="${ existing_contact_found.city }" />,
						<c:out value="${ existing_contact_found.postal }" />,
						<c:out value="${ existing_contact_found.province }" />,
						<c:out value="${ existing_contact_found.country }" />
					</div>
					<div class="row">
					<label>Phone</label>
						<c:out value="${ existing_contact_found.workPhone }" /> ext <c:out value="${  existing_contact_found.workPhoneExtension }" />
					</div>
					<div class="row">
					<label>Fax</label>
						<c:out value="${ existing_contact_found.fax }" />
					</div>
					<div class="row">
					<label>Email</label>
						<c:out value="${ existing_contact_found.email }" />
					</div>
					<div class="row">
					<label>Private</label>
						<c:out value="${ existing_contact_found.residencePhone }" />
					</div>
					<div class="row">
					<label>Cell</label>
						<c:out value="${ existing_contact_found.cellPhone }" />
					</div>
						
				</div>
				<div class="col-sm-6">
					<div class="row">
						<h4>with this information?</h4>
						<label>Name</label>
						<c:out value="${ contact_submitted.firstName }" />
						<c:out value="${ contact_submitted.lastName }" />
					</div>
					<div class="row">	
						<label>CPSO</label>
						<c:out value="${ contact_submitted.cpso }" />
					</div>
					<div class="row">	
						<label>Specialty</label>
						<c:out value="${ contact_submitted.specialty }" />
					</div>
					<div class="row">	
						<label>Address</label>
						<c:out value="${ contact_submitted.address }" />
						<c:out value="${ contact_submitted.address2 }" />,
						<c:out value="${ contact_submitted.city }" />,
						<c:out value="${ contact_submitted.postal }" />,
						<c:out value="${ contact_submitted.province }" />,
						<c:out value="${ contact_submitted.country }" />
					</div>
					<div class="row">	
						<label>Phone</label>
						<c:out value="${ contact_submitted.workPhone }" /> ext <c:out value="${ contact_submitted.workPhoneExtension }" />
					</div>
					<div class="row">	
						<label>Fax</label>
						<c:out value="${ contact_submitted.fax }" />
					</div>
					<div class="row">	
						<label>Email</label>
						<c:out value="${ contact_submitted.email }" />
					</div>
					<div class="row">	
						<label>Private</label>
						<c:out value="${ contact_submitted.residencePhone }" />
					</div>
					<div class="row">	
						<label>Cell</label>
						<c:out value="${ contact_submitted.cellPhone }" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
			
				<html:form action="/demographic/Contact.do" styleId="editExistingContact">
				
					<input type="hidden" class="edit-contact" name="pcontact.id" value="${ existing_contact_found.id }" />
					<input type="hidden" class="edit-contact" name="demographic_no" id="demographic_no" value="${ param.demographic_no }"/>
					<input type="hidden" class="edit-contact" name="demographicContactId" id="demographicContactId" value="${ requestScope.demographicContactId }"/>
					<input type="hidden" class="edit-contact" name="contactType" id="contactType" value="${ param.contactType }"/>
					<input type="hidden" name="method" value="saveProContact" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.firstName" value="${ not empty contact_submitted.firstName ? contact_submitted.firstName : existing_contact_found.firstName }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.lastName" value="${ not empty contact_submitted.lastName ? contact_submitted.lastName : existing_contact_found.lastName }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.cpso" value="${ existing_contact_found.cpso }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.specialty" value="${ not empty contact_submitted.specialty ? contact_submitted.specialty : existing_contact_found.specialty }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.address" value="${ not empty contact_submitted.address ? contact_submitted.address : existing_contact_found.address }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.address2" value="${ not empty contact_submitted.address2 ? contact_submitted.address2 : existing_contact_found.address2 }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.city" value="${ not empty contact_submitted.city ? contact_submitted.city : existing_contact_found.city  }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.postal" value="${ not empty contact_submitted.postal ? contact_submitted.postal : existing_contact_found.postal }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.province" value="${ not empty contact_submitted.province ? contact_submitted.province : existing_contact_found.province }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.country" value="${ not empty contact_submitted.country ? contact_submitted.country : existing_contact_found.country }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.workPhone" value="${ not empty contact_submitted.workPhone ? contact_submitted.workPhone : existing_contact_found.workPhone }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.workPhoneExtension" value="${ not empty contact_submitted.workPhoneExtension ? contact_submitted.workPhoneExtension :  existing_contact_found.workPhoneExtension }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.fax" value="${ not empty contact_submitted.fax ? contact_submitted.fax :  existing_contact_found.fax }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.email" value="${ not empty contact_submitted.email ? contact_submitted.email : existing_contact_found.email }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.residencePhone" value="${ not empty contact_submitted.residencePhone ? contact_submitted.residencePhone : existing_contact_found.residencePhone }" />
					<input type="hidden" class="edit-contact" 
						name="pcontact.cellPhone" value="${ not empty contact_submitted.cellPhone ? contact_submitted.cellPhone : existing_contact_found.cellPhone }" />
					
					<button type="submit" class="btn btn-default" >
						Update
					</button>
					<button type="button" class="btn btn-default" id="cancel-modal" data-dismiss="modal">
						Cancel
					</button>
					
				</html:form>
			</div>
		</div>

	</div>
</div> <!-- end modal container -->
</div> <!--  end container -->

</body>
</html:html>
