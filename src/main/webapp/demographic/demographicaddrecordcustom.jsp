<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.springframework.web.context.*,org.springframework.web.context.support.*,org.oscarehr.common.dao.*,org.oscarehr.common.model.*"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.*, java.sql.*, oscar.*, oscar.oscarDemographic.data.ProvinceNames, oscar.oscarWaitingList.WaitingList" errorPage="errorpage.jsp"%>
<%@ page import="org.springframework.web.context.*,org.springframework.web.context.support.*,org.oscarehr.common.dao.*,org.oscarehr.common.model.*"%>
<%@ page import="org.oscarehr.common.dao.CountryCodeDao" %>
<%@ page import="org.oscarehr.util.SessionConstants"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.PMmodule.web.GenericIntakeEditAction" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.managers.LookupListManager"%>
<%@ page import="org.oscarehr.common.model.LookupList"%>
<%@ page import="org.oscarehr.common.model.LookupListItem"%>

<%
	if(session.getAttribute("user") == null) response.sendRedirect("../logout.jsp");
	String curUser_no = (String) session.getAttribute("user");
	String str = null;
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
%>

<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="w" reverse="<%=true%>" >
	<% response.sendRedirect("../noRights.html"); %>
</security:oscarSec>

<%
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);

	LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
	LookupList referralSourceLookupList = lookupListManager.findLookupListByName(loggedInInfo, "demographicReferralSource");	
	pageContext.setAttribute("referralSourceLookupList", referralSourceLookupList);

  OscarProperties props = OscarProperties.getInstance();

  GregorianCalendar now=new GregorianCalendar();
  String curYear = Integer.toString(now.get(Calendar.YEAR));
  String curMonth = Integer.toString(now.get(Calendar.MONTH)+1);
  if (curMonth.length() < 2) curMonth = "0"+curMonth;
  String curDay = Integer.toString(now.get(Calendar.DAY_OF_MONTH));
  if (curDay.length() < 2) curDay = "0"+curDay;

  int nStrShowLen = 20;
  OscarProperties oscarProps = OscarProperties.getInstance();

  ProvinceNames pNames = ProvinceNames.getInstance();
  String prov= ((String ) props.getProperty("billregion","")).trim().toUpperCase();

  String billingCentre = ((String ) props.getProperty("billcenter","")).trim().toUpperCase();
  String defaultCity = prov.equals("ON")&&billingCentre.equals("N") ? "Toronto":"";

  WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
  CountryCodeDao ccDAO =  SpringUtils.getBean( CountryCodeDao.class );

  List<CountryCode> countryList = ccDAO.getAllCountryCodes();

  // Used to retrieve properties from user (i.e. HC_Type & default_sex)
  UserPropertyDAO userPropertyDAO = (UserPropertyDAO) ctx.getBean("UserPropertyDAO");

  String HCType = "";
  // Determine if curUser has selected a default HC Type
  UserProperty HCTypeProp = userPropertyDAO.getProp(curUser_no,  UserProperty.HC_TYPE);
  if (HCTypeProp != null) {
     HCType = HCTypeProp.getValue();
  } else {
     // If there is no user defined property, then determine if the hctype system property is activated
     HCType = props.getProperty("hctype","");
     if (HCType == null || HCType.equals("")) {
           // The system property is not activated, so use the billregion
           String billregion = props.getProperty("billregion", "");
           HCType = billregion;
     }
  }
  // Use this value as the default value for province, as well
  String defaultProvince = HCType;
		  
  String demographic_string = props.getProperty("cust_demographic_fields");
  if(demographic_string == null){
	  demographic_string = "";
  }
  

  ProgramManager pm = SpringUtils.getBean(ProgramManager.class);
  if( pm != null && loggedInInfo != null) {
	  
	  GenericIntakeEditAction gieat = new GenericIntakeEditAction();
	  gieat.setProgramManager(pm); 
	  String _pvid = loggedInInfo.getLoggedInProviderNo();
	  Set<Program> pset = gieat.getActiveProviderProgramsInFacility(loggedInInfo,_pvid,loggedInInfo.getCurrentFacility().getId());
	  List<Program> bedP = gieat.getBedPrograms(pset,_pvid);
	  pageContext.setAttribute( "residentialStatus", bedP.get(0).getId() );
	  
  }
  String searchMode = request.getParameter("search_mode");
  String keyWord = request.getParameter("keyword");
	  // carry over the search criteria.
	  String lastNameVal = "";
	  String firstNameVal = "";

	if ( (searchMode != null) && searchMode.equals("search_name")) {
	     int commaIdx = keyWord.indexOf(",");
	     if (commaIdx == -1)  {
	  		lastNameVal = keyWord.trim();
	     } else if (commaIdx == (keyWord.length()-1)) {
	        lastNameVal = keyWord.substring(0,keyWord.length()-1).trim();
	     } else {
	        lastNameVal = keyWord.substring(0,commaIdx).trim();
	   		firstNameVal = keyWord.substring(commaIdx+1).trim();
	     }
     } 
%>

<html>
<head>

<title><bean:message key="demographic.demographicaddrecordhtm.title" /></title>

<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="../share/calendar/calendar.css" title="win2k-cold-1" />
<!-- main calendar program -->
<script type="text/javascript" src="../share/calendar/calendar.js"></script>
<!-- language for the calendar -->
<script type="text/javascript" src="../share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->       
<script type="text/javascript" src="../share/calendar/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../css/Demographic.css"  />
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript" >

function setfocus() {
  this.focus();
  document.adddemographic.last_name.focus();
  document.adddemographic.last_name.select();
  window.resizeTo(1000,700);
}
function upCaseCtrl(ctrl) {
	ctrl.value = ctrl.value.toUpperCase();
}

function checkTypeIn() {
  var dob = document.titlesearch.keyword; typeInOK = false;

  if (dob.value.indexOf('%b610054') == 0 && dob.value.length > 18){
     document.titlesearch.keyword.value = dob.value.substring(8,18);
     document.titlesearch.search_mode[4].checked = true;
  }

  if(document.titlesearch.search_mode[2].checked) {
    if(dob.value.length==8) {
      dob.value = dob.value.substring(0, 4)+"-"+dob.value.substring(4, 6)+"-"+dob.value.substring(6, 8);
      //alert(dob.value.length);
      typeInOK = true;
    }
    if(dob.value.length != 10) {
      alert("<bean:message key="demographic.search.msgWrongDOB"/>");
      typeInOK = false;
    }

    return typeInOK ;
  } else {
    return true;
  }
}

function checkTypeInAdd() {
	var typeInOK = false;
	if(document.adddemographic.last_name.value!="" && document.adddemographic.first_name.value!="" && document.adddemographic.sex.value!="") {
      if(checkTypeNum(document.adddemographic.year_of_birth.value) && checkTypeNum(document.adddemographic.month_of_birth.value) && checkTypeNum(document.adddemographic.date_of_birth.value) ){
	    typeInOK = true;
	  }
	}
	if(!typeInOK) alert ("<bean:message key="demographic.demographicaddrecordhtm.msgMissingFields"/>");
	return typeInOK;
}

function newStatus() {
    newOpt = prompt("Please enter the new status:", "");
    if (newOpt != "") {
        document.adddemographic.patient_status.options[document.adddemographic.patient_status.length] = new Option(newOpt, newOpt);
        document.adddemographic.patient_status.options[document.adddemographic.patient_status.length-1].selected = true;
    } else {
        alert("Invalid entry");
    }
}
function newStatus1() {
    newOpt = prompt("Please enter the new status:", "");
    if (newOpt != "") {
        document.adddemographic.roster_status.options[document.adddemographic.roster_status.length] = new Option(newOpt, newOpt);
        document.adddemographic.roster_status.options[document.adddemographic.roster_status.length-1].selected = true;
    } else {
        alert("Invalid entry");
    }
}

function formatPhoneNum() {
    if (document.adddemographic.phone.value.length == 10) {
        document.adddemographic.phone.value = document.adddemographic.phone.value.substring(0,3) + "-" + document.adddemographic.phone.value.substring(3,6) + "-" + document.adddemographic.phone.value.substring(6);
        }
    if (document.adddemographic.phone.value.length == 11 && document.adddemographic.phone.value.charAt(3) == '-') {
        document.adddemographic.phone.value = document.adddemographic.phone.value.substring(0,3) + "-" + document.adddemographic.phone.value.substring(4,7) + "-" + document.adddemographic.phone.value.substring(7);
    }

    if (document.adddemographic.phone2.value.length == 10) {
        document.adddemographic.phone2.value = document.adddemographic.phone2.value.substring(0,3) + "-" + document.adddemographic.phone2.value.substring(3,6) + "-" + document.adddemographic.phone2.value.substring(6);
        }
    if (document.adddemographic.phone2.value.length == 11 && document.adddemographic.phone2.value.charAt(3) == '-') {
        document.adddemographic.phone2.value = document.adddemographic.phone2.value.substring(0,3) + "-" + document.adddemographic.phone2.value.substring(4,7) + "-" + document.adddemographic.phone2.value.substring(7);
    }
}

function rs(n,u,w,h,x) {
  args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0,top=60,left=30";
  remote=window.open(u,n,args);
}
function referralScriptAttach2(elementName, name2) {
     var d = elementName;
     t0 = escape("document.forms[1].elements[\'"+d+"\'].value");
     t1 = escape("document.forms[1].elements[\'"+name2+"\'].value");
     rs('att',('../billing/CA/ON/searchRefDoc.jsp?param='+t0+'&param2='+t1),600,600,1);
}

function checkName() {
	var typeInOK = false;
	if(document.adddemographic.last_name.value!="" && document.adddemographic.first_name.value!="" && document.adddemographic.last_name.value!=" " && document.adddemographic.first_name.value!=" ") {
	    typeInOK = true;
	} else {
		alert ("You must type in the following fields: Last Name, First Name.");
    }
	return typeInOK;
}

function checkDob() {
	var typeInOK = false;
	var yyyy = document.adddemographic.year_of_birth.value;
	var selectBox = document.adddemographic.month_of_birth;
	var mm = selectBox.options[selectBox.selectedIndex].value
	selectBox = document.adddemographic.date_of_birth;
	var dd = selectBox.options[selectBox.selectedIndex].value

	if(checkTypeNum(yyyy) && checkTypeNum(mm) && checkTypeNum(dd) ){
        //alert(yyyy); alert(mm); alert(dd);
        var check_date = new Date(yyyy,(mm-1),dd);
        //alert(check_date);
		var now = new Date();
		var year=now.getFullYear();
		var month=now.getMonth()+1;
		var date=now.getDate();
		//alert(yyyy + " | " + mm + " | " + dd + " " + year + " " + month + " " +date);

		var young = new Date(year,month,date);
		var old = new Date(1800,01,01);
		//alert(check_date.getTime() + " | " + young.getTime() + " | " + old.getTime());
		if (check_date.getTime() <= young.getTime() && check_date.getTime() >= old.getTime() && yyyy.length==4) {
		    typeInOK = true;
		    //alert("failed in here 1");
		}
		if ( yyyy == "0000"){
        typeInOK = false;
      }
	}

	if (!typeInOK){
      alert ("You must type in the right DOB.");
   }

   if (!isValidDate(dd,mm,yyyy)){
      alert ("DOB Date is an incorrect date");
      typeInOK = false;
   }

	return typeInOK;
}


function isValidDate(day,month,year){
   month = ( month - 1 );
   dteDate=new Date(year,month,day);
//alert(dteDate);
   return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}

function checkHin() {
	var hin = document.adddemographic.hin.value;
	var province = document.adddemographic.hc_type.value;

	if ( !isValidHin(hin, province) )
	{
		return confirm("HIN may be invalid. Press Ok to continue.");
		//return(false);
	}

	return(true);
}

function checkAllDate() {
	var typeInOK = false;
	typeInOK = checkDateYMD( document.adddemographic.date_joined_year.value , document.adddemographic.date_joined_month.value , document.adddemographic.date_joined_date.value , "Date Joined" );
	if (!typeInOK) { return false; }

	typeInOK = checkDateYMD( document.adddemographic.end_date_year.value , document.adddemographic.end_date_month.value , document.adddemographic.end_date_date.value , "End Date" );
	if (!typeInOK) { return false; }

	typeInOK = checkDateYMD( document.adddemographic.hc_renew_date_year.value , document.adddemographic.hc_renew_date_month.value , document.adddemographic.hc_renew_date_date.value , "PCN Date" );
	if (!typeInOK) { return false; }

	typeInOK = checkDateYMD( document.adddemographic.eff_date_year.value , document.adddemographic.eff_date_month.value , document.adddemographic.eff_date_date.value , "EFF Date" );
	if (!typeInOK) { return false; }

	return typeInOK;
}
	function checkDateYMD(yy, mm, dd, fieldName) {
		var typeInOK = false;
		if((yy.length==0) && (mm.length==0) && (dd.length==0) ){
			typeInOK = true;
		} else if(checkTypeNum(yy) && checkTypeNum(mm) && checkTypeNum(dd) ){
			if (checkDateYear(yy) && checkDateMonth(mm) && checkDateDate(dd)) {
				typeInOK = true;
			}
		}
		if (!typeInOK) { alert ("You must type in the right '" + fieldName + "'."); return false; }
		return typeInOK;
	}

	function checkDateYear(y) {
		if (y>1900 && y<2045) return true;
		return false;
	}
	function checkDateMonth(y) {
		if (y>=1 && y<=12) return true;
		return false;
	}
	function checkDateDate(y) {
		if (y>=1 && y<=31) return true;
		return false;
	}

function checkFormTypeIn() {
	if ( !checkName() ) return false;
	if ( !checkDob() ) return false;
	if ( !checkHin() ) return false;
	if ( !checkAllDate() ) return false;
	return true;
}

function checkTitleSex(ttl) {
    if (ttl=="MS" || ttl=="MISS" || ttl=="MRS" || ttl=="SR") document.adddemographic.sex.selectedIndex=1;
	else if (ttl=="MR" || ttl=="MSSR") document.adddemographic.sex.selectedIndex=0;
}

function removeAccents(s){
        var r=s.toLowerCase();
        r = r.replace(new RegExp("\\s", 'g'),"");
        r = r.replace(new RegExp("[������]", 'g'),"a");
        r = r.replace(new RegExp("�", 'g'),"c");
        r = r.replace(new RegExp("[����]", 'g'),"e");
        r = r.replace(new RegExp("[����]", 'g'),"i");
        r = r.replace(new RegExp("�", 'g'),"n");
        r = r.replace(new RegExp("[�����]", 'g'),"o");
        r = r.replace(new RegExp("[����]", 'g'),"u");
        r = r.replace(new RegExp("[��]", 'g'),"y");
        r = r.replace(new RegExp("\\W", 'g'),"");
        return r;
}

function autoFillHin(){
   var hcType = document.getElementById('hc_type').value;
   var hin = document.getElementById('hin').value;
   if(	hcType == 'QC' && hin == ''){
   	  var last = document.getElementById('last_name').value;
   	  var first = document.getElementById('first_name').value;
      var yob = document.getElementById('year_of_birth').value;
      var mob = document.getElementById('month_of_birth').value;
      var dob = document.getElementById('date_of_birth').value;

   	  last = removeAccents(last.substring(0,3)).toUpperCase();
   	  first = removeAccents(first.substring(0,1)).toUpperCase();
   	  yob = yob.substring(2,4);
   	  
   	  var sex = document.getElementById('sex').value;
   	  if(sex == 'F'){
   		  mob = parseInt(mob) + 50; 
   	  }

      document.getElementById('hin').value = last + first + yob + mob + dob;
      hin.focus();
      hin.value = hin.value;
   }
}

</script>
</head>

<body onLoad="setfocus();" >
<table>	
	<tr>
	<td id="topLinkLeftColumn" style="background-color: black;" > 
		<h1 style="color:white;">Add Demographic Quick Form</h1> 	
	</td>
	</tr>
</table>
<form method="post" name="adddemographic" action="demographicaddarecord.jsp" onsubmit="return checkFormTypeIn()" >

<input type="hidden" name="fromAppt" value="<%=request.getParameter("fromAppt")%>">
<input type="hidden" name="originalPage" value="<%=request.getParameter("originalPage")%>">
<input type="hidden" name="bFirstDisp" value="<%=request.getParameter("bFirstDisp")%>">
<input type="hidden" name="provider_no" value="<%=request.getParameter("provider_no")%>">
<input type="hidden" name="start_time" value="<%=request.getParameter("start_time")%>">
<input type="hidden" name="end_time" value="<%=request.getParameter("end_time")%>">
<input type="hidden" name="duration" value="<%=request.getParameter("duration")%>">
<input type="hidden" name="year" value="<%=request.getParameter("year")%>">
<input type="hidden" name="month" value="<%=request.getParameter("month")%>">
<input type="hidden" name="day" value="<%=request.getParameter("day")%>">
<input type="hidden" name="appointment_date" value="<%=request.getParameter("appointment_date")%>">
<input type="hidden" name="notes" value="<%=request.getParameter("notes")%>">
<input type="hidden" name="reason" value="<%=request.getParameter("reason")%>">
<input type="hidden" name="location" value="<%=request.getParameter("location")%>">
<input type="hidden" name="resources" value="<%=request.getParameter("resources")%>">
<input type="hidden" name="type" value="<%=request.getParameter("type")%>">
<input type="hidden" name="style" value="<%=request.getParameter("style")%>">
<input type="hidden" name="billing" value="<%=request.getParameter("billing")%>">
<input type="hidden" name="status" value="<%=request.getParameter("status")%>">
<input type="hidden" name="createdatetime" value="<%=request.getParameter("createdatetime")%>">
<input type="hidden" name="creator" value="<%=request.getParameter("creator")%>">
<input type="hidden" name="remarks" value="<%=request.getParameter("remarks")%>">

<input type="hidden" name="formType" value="demographicQuickForm" />

<div id="addDemographicQuickForm">
	<div>
		<label><b><bean:message key="demographic.demographicaddrecordhtm.msgDemoTitle"/><font color="red">:</font></b></label>
		<select name="title" onchange="checkTitleSex(value);">
			<option value="" selected><bean:message key="demographic.demographicaddrecordhtm.msgNotSet"/></option>
			<option value="MS"><bean:message key="demographic.demographicaddrecordhtm.msgMs"/></option>
			<option value="MISS"><bean:message key="demographic.demographicaddrecordhtm.msgMiss"/></option>
			<option value="MRS"><bean:message key="demographic.demographicaddrecordhtm.msgMrs"/></option>
			<option value="MR"><bean:message key="demographic.demographicaddrecordhtm.msgMr"/></option>
			<option value="MSSR"><bean:message key="demographic.demographicaddrecordhtm.msgMssr"/></option>
			<option value="PROF"><bean:message key="demographic.demographicaddrecordhtm.msgProf"/></option>
			<option value="REEVE"><bean:message key="demographic.demographicaddrecordhtm.msgReeve"/></option>
			<option value="REV"><bean:message key="demographic.demographicaddrecordhtm.msgRev"/></option>
			<option value="RT_HON"><bean:message key="demographic.demographicaddrecordhtm.msgRtHon"/></option>
			<option value="SEN"><bean:message key="demographic.demographicaddrecordhtm.msgSen"/></option>
			<option value="SGT"><bean:message key="demographic.demographicaddrecordhtm.msgSgt"/></option>
			<option value="SR"><bean:message key="demographic.demographicaddrecordhtm.msgSr"/></option>
		</select>
	</div>
		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formLastName"/><font color="red">:</font> </b></label>
			<input type="text" name="last_name" value="<%=lastNameVal%>" onBlur="upCaseCtrl(this)" size=30 />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formFirstName"/><font color="red">:</font> </b></label>
			<input type="text" name="first_name" value="<%=firstNameVal%>" onBlur="upCaseCtrl(this)"  size=30>
		</div>

		<div>
			<label> <b><bean:message key="demographic.demographicaddrecordhtm.formMiddleNames"/>: </b></label>
			<input type="text" name="middleNames" id="middleNames" onBlur="upCaseCtrl(this)" size=50 value="">
		</div>

		<div>
			<label><strong><bean:message key="demographic.demographicaddrecordhtm.formNameUsed" />:</strong></label>
			<input type="text" name="nameUsed" size="50" value=""onBlur="upCaseCtrl(this)" />
		</div>


		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formAddress" />: </b></label>
			<input type="text" name="address" size=40 />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formCity" />: </b></label>
			<input type="text" name="city" value="<%=defaultCity %>" />
		</div>

		<div>
			<label>
			
			<c:set value='<%= oscarProps.getProperty("demographicLabelProvince") %>' var="provinceLabel" />
			<c:choose>
			<c:when test="${ empty provinceLabel }" >
				<b><bean:message key="demographic.demographiceditdemographic.formProcvince" /></b>
			</c:when>
			<c:otherwise>
				<b><c:out value="${ provinceLabel }" /></b>
			</c:otherwise>
			</c:choose>
			</b>
		
	 		</label>

	 		<%-- Include file to reduce file size. --%>
			<jsp:include page="./includes/_provinceStateSelectList.jsp">
				<jsp:param value="<%= defaultProvince %>" name="selectedProvince"/>
				<jsp:param value="province" name="selectListName" />
				<jsp:param value="province" name="selectListId" />
			</jsp:include>

		</div>

		<div>
			<label>
				<b> 
				<% if(oscarProps.getProperty("demographicLabelPostal") == null) { %>
					<bean:message key="demographic.demographicaddrecordhtm.formPostal" />
				<% } else {
	  				out.print(oscarProps.getProperty("demographicLabelPostal"));
		 		} %> : 
		 		</b>
		 	</label>
			<input type="text" name="postal" onBlur="upCaseCtrl(this)">
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formPhoneHome" />: </b></label>
			<input type="text" name="phone" onBlur="formatPhoneNum()" value="<%=props.getProperty("phoneprefix")%>">
			<bean:message key="demographic.demographicaddrecordhtm.Ext" />:
			<input type="text" name="hPhoneExt" value="" size="4" />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formPhoneWork" />:</b></label>
			<input type="text" name="phone2" onBlur="formatPhoneNum()" value="">
			<bean:message key="demographic.demographicaddrecordhtm.Ext" />:
			<input type="text" name="wPhoneExt" value="" style="display: inline" size="4" />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formPhoneCell" />: </b></label>
			<input type="text" name="demo_cell" onBlur="formatPhoneNum()">
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formEMail" />: </b></label>
			<input type="text" name="email" value="">
		</div>
	
		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formDOB" /></b>
				<font size="-2">(yyyymmdd)</font><b><font color="red">:</font></b>
			</label>
			<input type="text" name="year_of_birth" size="4" 
				maxlength="4" value="yyyy" 
				onFocus="if(this.value=='yyyy')this.value='';" 
				onBlur="if(this.value=='')this.value='yyyy';" />
			-
			<select name="month_of_birth">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option selected value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
			</select>
			-
			<select name="date_of_birth">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option selected value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
			</select>
		</div>

		<div>
			<label for="patientPronouns"><bean:message key="demographic.demographicaddrecordhtm.formPronouns" /></label>
			<input type="text" id="patientPronouns" name="pronouns" />
		</div>
		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formSex" />
			<font color="red">:</font></b></label>
			<% // Determine if curUser has selected a default sex in preferences
                UserProperty sexProp = userPropertyDAO.getProp(curUser_no,  UserProperty.DEFAULT_SEX);
                String sex = "";
                if (sexProp != null) {
                    sex = sexProp.getValue();
                } else {
                    // Access defaultsex system property
                    sex = props.getProperty("defaultsex","");
                }
             %>
	         <select name="sex">
	             <option value="M"  <%= sex.equals("M") ? " selected": "" %> >
	             	<bean:message key="demographic.demographicaddrecordhtm.formM" />
	             </option>
	             <option value="F"  <%= sex.equals("F") ? " selected": "" %> >
	             	<bean:message key="demographic.demographicaddrecordhtm.formF" />
	             </option>
	         </select>
		</div>
		<div>
			<label for="patientGender"><bean:message key="demographic.demographicaddrecordhtm.formGender" /></label>
			<input type="text" id="patientGender" name="gender" />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formHIN" />: </b></label>
			<input type="text" name="hin" size="15">
			<b><bean:message key="demographic.demographicaddrecordhtm.formVer" />:</b>
			<input type="text" name="ver" value="" size="3" onBlur="upCaseCtrl(this)">
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formEFFDate" />: </b></label>
			<input type="text" name="eff_date_year" size="4" maxlength="4" />
			<input type="text" name="eff_date_month" size="2" maxlength="2" />
			<input type="text" name="eff_date_date" size="2" maxlength="2" />
		</div>

		<div>
			<label><b><bean:message key="demographic.demographicaddrecordhtm.formHCType" />: </b></label>

				<%-- Include file to reduce file size. --%>
				<jsp:include page="includes/_provinceStateSelectList.jsp">
					<jsp:param value="<%= HCType %>" name="selectedProvince"/>
					<jsp:param value="hc_type" name="selectListName" />
					<jsp:param value="hc_type" name="selectListId" />
				</jsp:include>
		</div>
		<oscar:oscarPropertiesCheck property="DEMOGRAPHIC_PATIENT_HEALTH_CARE_TEAM" value="true">
			<div>
		    	<label>
		    		<strong>Referral Source:</strong>
		    	</label>
	 
	    		<select name="referralSource" id="referralSource">
		    		<c:choose>
			            <c:when test="${ not empty referralSourceLookupList.items  }">
			    			<c:forEach items="${ referralSourceLookupList.items }" var="referralSourceItem">
			    				<c:if test="${ referralSourceItem.active }">	
				    				<option value="${ referralSourceItem.id }" id="${ referralSourceItem.value }">
				    					<c:out value="${ referralSourceItem.label }" />
				    				</option>
			    				</c:if>
			    			</c:forEach>
		    			</c:when>
		               	<c:otherwise>
		               		<option value="-1">Other</option>
		               	</c:otherwise>
		            </c:choose>
	    		</select>
			</div>
		</oscar:oscarPropertiesCheck>
		<div>
			<oscar:oscarPropertiesCheck value="true" property="STUDENT_PARTICIPATION_CONSENT">
			<label style="font-weight:bold;" for="informedConsent" >
						<bean:message key="demographic.demographiceditdemographic.studentParticipationConsent"/>
					</label>
				<input type="checkbox" id="informedConsent" name="informedConsent" value="yes" />
					
			</oscar:oscarPropertiesCheck>		
		</div>

			<input type="hidden" name="date_joined_year" value="<%=curYear%>">
			<input type="hidden" name="date_joined_month" value="<%=curMonth%>">
			<input type="hidden" name="date_joined_date" value="<%=curDay%>">
			<input type="hidden" name="list_id" value="0" />
			<input type="hidden" name="rps" value="${ residentialStatus}" />
			<input type="hidden" name="patient_status" value="AC">
			<input type="hidden" name="notes" value="">

</div>

 <div id="bottomToolBar" >
	<input type="hidden" name="dboperation"	value="add_record"> 
	<input type="hidden" name="displaymode" value="Add Record">
	<input type="submit" name="submit" value="<bean:message key="demographic.demographicaddrecordhtm.btnAddRecord"/>" >
	<input type="button" name="Button" value="<bean:message key="demographic.demographicaddrecordhtm.btnCancel"/>" onclick="self.close();" />
</div>

</form>

</body>
</html>