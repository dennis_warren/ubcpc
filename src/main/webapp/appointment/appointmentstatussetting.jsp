<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ page import="java.util.*,org.oscarehr.common.model.*"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
%>
<security:oscarSec roleName="<%=roleName$%>"
	objectName="_admin,_admin.userAdmin,_admin.schedule" rights="r" reverse="<%=true%>">
	<%response.sendRedirect("../logout.jsp");%>
</security:oscarSec>

<!DOCTYPE html>
<html>
<head>
<title><bean:message key="admin.appt.status.mgr.title" /></title>

<link href="${pageContext.request.contextPath}/library/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/jquery.ui.colorPicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/library/jquery/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/library/jquery/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/jquery-ui-1.11.4.min.js"></script> 

<script src="${pageContext.servletContext.contextPath}/js/jquery.ui.colorPicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/bootstrap.min.2.js"></script>
<script type="text/javascript">	var ctx = '${pageContext.servletContext.contextPath}'; </script>
</head>
<body>
<%
        String reseturl = request.getContextPath();
        reseturl = reseturl + "/appointment/apptStatusSetting.do?dispatch=reset";
%>

<%@ include file="aptStatusTopNav.jspf"%>

<h4>Appointment Status</h4>

<div class="row-fluid">
<form action="/appointment/apptStatusSetting">
<table class="borderAll table table-condensed">
	<tr>
		<th><bean:message key="admin.appt.status.mgr.label.status" /></th>
		<th></th>
		<th><bean:message key="admin.appt.status.mgr.label.desc" /></th>
		<th></th>
		<th><bean:message key="admin.appt.status.mgr.label.color" /></th>
		<th></th>
		<th>Icon</th>
		<th><bean:message key="admin.appt.status.mgr.label.active" /></th>
	</tr>
	<%
            List apptsList = (List) request.getAttribute("allStatus");
            AppointmentStatus apptStatus = null;
            int iStatusID = 0;
            String strStatus = "";
            String strDesc = "";
            String strColor = "";
            int iActive = 0;
            String icon = "";
            boolean iEditable = Boolean.FALSE;
            for (int i = 0; i < apptsList.size(); i++) {
                apptStatus = (AppointmentStatus) apptsList.get(i);
                iStatusID = apptStatus.getId();
                strStatus = apptStatus.getStatus();
                strDesc = apptStatus.getDescription();
                strColor = apptStatus.getColor();
                iActive = apptStatus.getActive();
                iEditable = (apptStatus.getEditable() == 1);
                icon = apptStatus.getIcon();
%>
	<tr>
		<td><%=strStatus%></td>
		<td style="text-align:right;">
			<c:if test="<%= iEditable %>" >
				<a href="javascript:void(0)" id="description-<%= iStatusID %>" class="editDescription" >
					<span title="edit description" class="glyphicon glyphicon-edit" aria-hidden="true"></span>
				</a>
			</c:if> 
		</td>
		<td>
			<span id="current-description-<%= iStatusID %>"><%=strDesc%></span>	
		</td>
		<td style="text-align:right;">
			<c:if test="<%= iEditable %>" >
				<a href="javascript:void(0)" id="colour-<%= iStatusID %>" class="editColour">
					<span title="edit colour" class="glyphicon glyphicon-edit" aria-hidden="true"></span>
				</a>
			</c:if>
		</td>
		<td style="background-color:<%=strColor%>;">
			<span id="current-colour-<%= iStatusID %>"><%=strColor%></span>
		</td>
		<td style="text-align:right;">
			<c:if test="<%= iEditable %>" >
				<a href="javascript:void(0)" id="icon-<%= iStatusID %>" class="editIcon">
					<span title="edit icon" class="glyphicon glyphicon-edit" aria-hidden="true"></span>
				</a>
			</c:if>
		</td>
		<td>
			<% if (icon.endsWith(".gif") || icon.endsWith(".png") || icon.endsWith(".jpg")) {%>
				<img width="30" alt="<%= icon %>" src="${pageContext.servletContext.contextPath}/images/<%= icon %>" 
				onerror="this.onerror=null;this.src='${pageContext.servletContext.contextPath}/images/icon_alert.gif';"/>
			<%} else { %>
				<span class="glyphicon <%= icon %>" aria-hidden="true"></span>
			<%} %>			
		</td>
		
		<td>

			    <% 
			    int iToStatus = (iActive > 0) ? 0 : 1;     
			    String url = request.getContextPath();
			    url = url + "/appointment/apptStatusSetting.do?dispatch=changestatus&iActive=";
			    url = url + iToStatus;
			    url = url + "&statusID=";
			    url = url + iStatusID;
			    if (iEditable) {
			        %> <a href=<%=url%>><%=(iActive > 0) ? "Disable" : "Enable"%></a>
					<%
			    }
			        %>
		</td>
	</tr>
	<%
            }
%>
</table>
</form>
</div>

<%
    String strUseStatus = (String)request.getAttribute("useStatus");
    if (null!=strUseStatus && strUseStatus.length()>0)
    {
%>
The code [<%=strUseStatus%>] has been used before, please enable that
status.
<%
    }
%>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/lookupListItemEditor.js"></script>
<div id="dialog-container">
	<form action="${pageContext.request.contextPath}/appointment/apptStatusSetting.do" id="dialog-form" class="form-horizontal"></form>
</div>

</body>
</html>
