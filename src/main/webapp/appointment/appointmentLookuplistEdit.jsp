<%--

    Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    The Pharmacists Clinic
    Faculty of Pharmaceutical Sciences
    University of British Columbia
    Vancouver, British Columbia, Canada

--%>

<%@ page import="java.util.*,org.oscarehr.common.model.*"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
%>
<security:oscarSec roleName="<%=roleName$%>"
	objectName="_admin,_admin.userAdmin,_admin.schedule" rights="r" reverse="<%=true%>">
	<%response.sendRedirect("../logout.jsp");%>
</security:oscarSec>

<!DOCTYPE html>
<html>
<head>
<title>Edit Appointment Reason List</title>
<link href="${pageContext.request.contextPath}/library/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/DT_bootstrap.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/jquery.ui.colorPicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/library/jquery/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/jquery-ui-1.11.4.min.js"></script> 

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/bootstrap.min.2.js"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery.ui.colorPicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#lookupListSelection").change(function(){
			$('#appointmentLookuplistSelectForm').submit();
		});
	});
	
	var ctx = '${pageContext.servletContext.contextPath}';
</script>
</head>
<body>
<%@ include file="aptStatusTopNav.jspf"%>

	<div class="row-fluid">
		<form action="${pageContext.servletContext.contextPath}/appointment/appointmentMetadata.do" id="appointmentLookuplistSelectForm" class="form-horizontal">
			<input type="hidden" name="listName" value="${ listName }" />
			<input type="hidden" name="method" value="setList" />
			<label>
				Select a custom Lookup List to use for appointment <c:out value="${ listName }" />.
			</label>
			<c:if test="${ not empty lookupLists }">
				<select id="lookupListSelection" name="lookupListSelection">
					<option value="" selected></option>
					<c:forEach items="${ lookupLists }" var="lookupList">
						<option value="${ lookupList.name }" ${ selectedLookupList.id eq lookupList.id ? 'selected' : '' }>
							<c:out value="${ lookupList.listTitle }" />
						</option>
					</c:forEach>
				</select>
			</c:if>	
		</form>
		<c:if test="${ not empty selectedLookupList }">
			<div class="lookupListName header">
				<h4>
					<c:out value="${ selectedLookupList.listTitle }" /><br />
					<small>
						<c:out value="${ selectedLookupList.description }" />
					</small>
				</h4>
			</div>
			
			<table class="table table-condensed">
				<th>Label</th>
				<th>Identifier</th>
				<th></th>
				<th>Colour</th>
				<th></th>
				<th>Icon</th>
				<c:forEach items="${ selectedLookupList.items }" var="selectedLookupListItem">
					<c:if test="${ selectedLookupListItem.active }">
						<tr>
							<td>
								<c:out value="${ selectedLookupListItem.label }" />
							</td>
							<td>
								<c:out value="${ selectedLookupListItem.value }" />
							</td>
							<td style="text-align:right;">
								<a href="javascript:void(0)" id="colour-${ selectedLookupListItem.id }" class="editColour">
									<span title="edit colour" class="glyphicon glyphicon-edit" aria-hidden="true"></span>
								</a>
							</td>
							<td class="reasonColor" style="background-color: <c:out value="${ selectedLookupListItem.colour }" /> ;">
								<span id="current-colour-${ selectedLookupListItem.id }"><c:out value="${ selectedLookupListItem.colour }" /></span>
							</td>	
							<td style="text-align:right;">
								<a href="javascript:void(0)" id="icon-${ selectedLookupListItem.id }" class="editIcon">
									<span title="edit icon" class="glyphicon glyphicon-edit" aria-hidden="true"></span>
								</a>
							</td>
							<td>
								<span class="glyphicon ${ selectedLookupListItem.icon }" aria-hidden="true"></span>
								<input type="hidden" id="current-icon-${ selectedLookupListItem.id }" 
									name="current-icon-${ selectedLookupListItem.id }" value="${ selectedLookupListItem.icon }" />
							</td>	
						</tr>
					</c:if>
				</c:forEach>
			</table>
		</c:if>
	</div>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/lookupListItemEditor.js"></script>
<div id="dialog-container">
	<form action="${pageContext.request.contextPath}/appointment/appointmentMetadata.do" id="dialog-form" class="form-horizontal">
		<input type="hidden" name="listName" id="listName" value="${ listName }" />
	</form>
</div>
</body>
</html>