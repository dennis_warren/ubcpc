<%--

    Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    The Pharmacists Clinic
    Faculty of Pharmaceutical Sciences
    University of British Columbia
    Vancouver, British Columbia, Canada

--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  %>

<%--
	Author: Dennis Warren
	Company: Colcamex Resources
	Date: November 2014
	Comment: written for the Pharmacy Clinic of UBC Pharmaceutical Science's
--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html:html>

	<head>
		<title><bean:message key="colcamex.formBPMH.title" /></title>
		<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/form/pharmaForms/index.css" />
<%--		<link rel="stylesheet" type="text/css" media="print" href="${ pageContext.request.contextPath }/form/pharmaForms/print.css" />--%>
		<link rel="stylesheet" type="text/css" media="screen" href="${ pageContext.request.contextPath }/library/bootstrap/3.0.0/css/bootstrap.css" />
		<script type="text/javascript" src="${ pageContext.request.contextPath }/library/jquery/jquery-1.12.0.min.js" ></script>
		<script type="text/javascript" >

			function popUpData(data) {
				if(data) {
					location.reload();
				}
			}

			function sortColumn(element){
				var column = element.id;
				var direction = $(element).attr("class");

				if(direction === "ASCENDING")
				{
					direction = "DESCENDING";
				}
				else if(direction === "DESCENDING")
				{
					direction = "ASCENDING";
				}

				var path = "${ pageContext.request.contextPath }/BPMHSort.do";
				path = path + "?method=sort&sortcolumn=" + column + "&" + "sortorder=" + direction;
				var form_data = $("form").serialize();

				$.ajax({
					method: "POST",
					url: path,
					data: form_data,
					success: function(data) {
						renderResponse($(data), "#drugtable");
						$("#"+column).attr("class", direction);
					}
				});
			}

			//--> Refresh containing elements
			function renderResponse(html, id) {
				$(id).replaceWith( $(id, html) );
			}

			jQuery(document).ready( function($) {
				$("#editFamilyDr").click(function(){
					var familyDrContactId = $("#familyDrContactId").val();
					var demographicNo = '${ bpmh.demographic.demographicNo }';
					var source = "${ pageContext.request.contextPath }/demographic/Contact.do?" +
							"demographic_no=" + demographicNo +
							"&view=detached" +
							"&element=providerId" +
							"&method=manageContactList" +
							"&contactList=HCT";
					var windowspecs = "width=700,height=400,left=-1000,top=-1000, scrollbars=yes, resizable=yes";
					window.open(source, "manageHealthCareTeam", windowspecs);
				})
			})


			function printForm(){
				window.print();
			}

			jQuery(document).ready( function ($) {
				// increase font size
				$("#incFontSize").click(function(){
					var currentSize = $("#drugtable").css("font-size");
					var currentSizeNum = parseFloat(currentSize);
					var newSize = currentSizeNum * 1.1;
					$("#patientId").css("font-size", newSize);
					$("#allergies").css("font-size", newSize);
					$("#providerId").css("font-size", newSize);
					$("#drugtable").css("font-size", newSize);
					$("textarea").css("font-size", newSize);
					$("#notes").css("font-size", newSize);
					$("#noteTable").css("font-size", newSize);
				});
				// decrease font size
				$("#decFontSize").click(function(){
					var currentSize = $("#drugtable").css("font-size");
					var currentSizeNum = parseFloat(currentSize);
					var newSize = currentSizeNum * 0.9;
					$("#patientId").css("font-size", newSize);
					$("#allergies").css("font-size", newSize);
					$("#providerId").css("font-size", newSize);
					$("#drugtable").css("font-size", newSize);
					$("textarea").css("font-size", newSize);
					$("#notes").css("font-size", newSize);
					$("#noteTable").css("font-size", newSize);
				});
				// reset font size
				var currentSize = $("#drugtable").css("font-size");
				$("#resetFontSize").click(function(){
					$("#patientId").css("font-size", currentSize);
					$("#allergies").css("font-size", currentSize);
					$("#providerId").css("font-size", currentSize);
					$("#drugtable").css("font-size", currentSize);
					$("textarea").css("font-size", currentSize);
					$("#notes").css("font-size", currentSize);
					$("#noteTable").css("font-size", currentSize);
				});
			})
		</script>

	</head>

	<body id="formBPMH">

	<!--  HEADING  -->
	<!-- FORM BEGIN -->
	<html:form action="/formBPMH.do" >

		<html:hidden property="demographicNo" />
		<html:hidden property="formId" />
		<div class="header">

			<table id="topTable" class="table-condensed">
				<tr>
					<td style="width:55%;">

						<logic:notEmpty name="bpmh" property="letterheadImage">
							<img width="350px" src="${ pageContext.request.contextPath }/imagelibrary/<bean:write name="bpmh" property="letterheadImage" />" />
						</logic:notEmpty>
						<logic:empty name="bpmh" property="letterheadImage">
							<h2><bean:write name="bpmh" property="clinic.clinicName" /></h2>
						</logic:empty>
					</td>
					<td rowspan="2">
						<c:set var="controls" value="on" scope="page" />

						<div id="bpmhId">

							<logic:empty name="bpmh" property="provider">

							<span class="red" >
								<bean:message key="colcamex.formBPMH.preparedby" />
								<bean:message key="colcamex.formBPMH.error.unknown" />
							</logic:empty>

							<logic:notEmpty name="bpmh" property="provider">

							<span>
						<bean:message key="colcamex.formBPMH.preparedby" />
						<bean:write name="bpmh" property="provider.formattedName" />
						<logic:notEmpty name="bpmh" property="provider.ohipNo">
							&#40;<bean:write name="bpmh" property="provider.ohipNo" />&#41;
						</logic:notEmpty>
						<logic:empty name="bpmh" property="provider.ohipNo">
							&#40;<bean:message key="colcamex.formBPMH.error.unknown" />&#41;
						</logic:empty>

					</logic:notEmpty>
				</span>


				<logic:empty name="bpmh" property="formDateFormatted">
					<span class="red" >
				</logic:empty>
				<logic:notEmpty name="bpmh" property="formDateFormatted">
					<span>
				</logic:notEmpty>
					<bean:message key="colcamex.formBPMH.preparedon" />
					<bean:write name="bpmh" property="formDateFormatted" />
				</span>

						</div>
					</td>

				</tr>
				<tr>
					<td>
						<logic:empty name="bpmh" property="letterheadImage">
							<h3>
									<bean:write name="bpmh" property="clinic.clinicAddress" /><br /><bean:write name="bpmh" property="clinic.clinicCity" />,
									<bean:write name="bpmh" property="clinic.clinicProvince" /> <bean:write name="bpmh" property="clinic.clinicPostal" />
							</h3>
						</logic:empty>
						<strong>Phone:</strong> <bean:write name="bpmh" property="clinic.clinicPhone" />  <strong>Fax:</strong> <bean:write name="bpmh" property="clinic.clinicFax" />
					</td>

				</tr>

			</table>

			<h3><bean:message key="colcamex.formBPMH.title" /></h3>

		</div>

		<!--  SUB HEADING  -->
		<div class="section" id="subHeader">

			<!--  PATIENT  -->
			<table id="patientId" class="table-condensed">
				<tr><th colspan="6"><bean:message key="colcamex.formBPMH.patient" /></th></tr>
				<tr>
					<td rowspan="2" class="columnTitle" ><bean:message key="colcamex.formBPMH.patient.name" /></td>
					<td rowspan="2">
						<bean:write name="bpmh" property="demographic.fullName" />
					</td>
					<td class="columnTitle"><bean:message key="colcamex.formBPMH.patient.insurance" /></td>
					<td>
						<bean:write name="bpmh" property="demographic.hin" />
					</td>
					<td class="columnTitle"><bean:message key="colcamex.formBPMH.patient.gender" /></td>
					<td>
						<bean:write name="bpmh" property="demographic.sex" />
					</td>
				</tr>

				<tr>
					<td class="columnTitle"><bean:message key="colcamex.formBPMH.patient.dob" /></td>
					<td>
						<bean:write name="bpmh" property="demographic.formattedDob" />
					</td>
					<td class="columnTitle"><bean:message key="colcamex.formBPMH.patient.phone" /></td>
					<td>
						<bean:write name="bpmh" property="demographic.phone" />
					</td>
				</tr>

				<tr>
					<td colspan="6" style="border:none;padding:0;">
						<table class="table-condensed">
							<tr>
								<td class="columnTitle">
									<bean:message key="colcamex.formBPMH.patient.allergies" />
								</td>
								<td>
									<logic:notEmpty name="bpmh" property="allergiesString">
										<bean:write name="bpmh" property="allergiesString" />
									</logic:notEmpty>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<!--  FAMILY PHYSICIAN  -->
		<div class="section" id="familyPhysician" >
			<html:hidden name="bpmh" property="familyDrContactId" styleId="familyDrContactId" />
			<table class="table-condensed" id="providerId">
				<tr><th colspan="7"><bean:message key="colcamex.formBPMH.familyDr" /></th></tr>
				<tr>
					<td class="columnTitle"><bean:message key="colcamex.formBPMH.familyDr.name" /></td>
					<td>
						<logic:empty name="bpmh" property="familyDrName" >
							<span class="red" >	Unknown	</span>
						</logic:empty>
						<logic:notEmpty name="bpmh" property="familyDrName" >
							<bean:write name="bpmh" property="familyDrName" />
						</logic:notEmpty>
					</td>

					<td class="columnTitle"><bean:message key="colcamex.formBPMH.familyDr.phone" /></td>
					<td>
						<logic:empty name="bpmh" property="familyDrPhone" >
							<span class="red" >	Unknown	</span>
						</logic:empty>
						<logic:notEmpty name="bpmh" property="familyDrPhone" >
							<bean:write name="bpmh" property="familyDrPhone" />
						</logic:notEmpty>
					</td>

					<td class="columnTitle"><bean:message key="colcamex.formBPMH.familyDr.fax" /></td>
					<td>
						<logic:empty name="bpmh" property="familyDrFax" >
							<span class="red" >	Unknown	</span>
						</logic:empty>
						<logic:notEmpty name="bpmh" property="familyDrFax" >
							<bean:write name="bpmh" property="familyDrFax" />
						</logic:notEmpty>
					</td>
					<logic:equal name="bpmh" property="formId" value="0">
						<logic:notEmpty name="bpmh" property="familyDrContactId" >
							<td style="text-align: right;">
								<input type="button" class="btn btn-primary" id="editFamilyDr" value="edit" />
							</td>
						</logic:notEmpty>
					</logic:equal>

				</tr>

			</table>
		</div>

		<!-- DRUG DATA -->
		<div class="section" id="drugData">

			<table class="table-condensed" id="drugtable">

				<tr>
					<th colspan="4">
						<bean:message key="colcamex.formBPMH.drugs" />
						<html:hidden name="bpmh" property="sortorder" />
						<html:hidden name="bpmh" property="sortcolumn" />
					</th>
				</tr>
				<tr id="drugtableSubheading">
					<td class="columnTitle" >
						<a href="#" id="drug_name" class="ASCENDING" onclick="Javascript:sortColumn(this);" ><bean:message key="colcamex.formBPMH.drugs.what" /></a>
						<span class="smallText">
							<bean:message key="colcamex.formBPMH.drugs.what.sub" />
						</span>
					</td>
					<td class="columnTitle">
						<a href="#" id="prescription" class="ASCENDING" onclick="Javascript:sortColumn(this);" ><bean:message key="colcamex.formBPMH.drugs.how" /></a>
						<span class="smallText">
							<bean:message key="colcamex.formBPMH.drugs.how.sub" />
						</span>
					</td>
					<td class="columnTitle">
						<a href="#" id="indication" class="ASCENDING" onclick="Javascript:sortColumn(this);" ><bean:message key="colcamex.formBPMH.drugs.why" /><sup>*</sup></a>
						<span class="smallText">
							<bean:message key="colcamex.formBPMH.drugs.why.sub" />
						</span>
					</td>
					<td class="columnTitle">
						<bean:message key="colcamex.formBPMH.drugs.instructions" /><br />
						<span class="smallText">
							<bean:message key="colcamex.formBPMH.drugs.instructions.sub" />
						</span>
					</td>
				</tr>
				<c:set value="missingDrugData" var="false" />
				<logic:notEmpty name="bpmh" property="drugs">
					<logic:iterate name="bpmh" property="drugs" id="drugs" indexId="status" >
						<tr>
							<td>
								<html:hidden name="drugs" property="id" indexed="true" />
								<!-- WHAT -->
								<bean:write name="drugs" property="what" />
							</td>
							<logic:empty name="drugs" property="how">
								<c:set value="${ true }" var="missingDrugData" />
								<td class="red" >
									<!-- HOW ERROR -->
								</td>
							</logic:empty>
							<logic:notEmpty  name="drugs" property="how" >
								<td>
									<!-- HOW -->
									<bean:write name="drugs" property="how" />
								</td>
							</logic:notEmpty>
							<logic:empty name="drugs" property="why">
								<c:set value="${ true }" var="missingDrugData" />
								<td class="red" >
									<!-- WHY ERROR -->
								</td>
							</logic:empty>
							<logic:notEmpty  name="drugs" property="why">
								<td>
									<!-- WHY -->
									<bean:write name="drugs" property="why" />
								</td>
							</logic:notEmpty>
							<td>
								<!-- INSTRUCTION -->
								<logic:notEmpty name="drugs" property="specialInstruction" >
									<bean:write name="drugs" property="specialInstruction" />
								</logic:notEmpty>
							</td>
						</tr>
					</logic:iterate>
				</logic:notEmpty>
			</table>
		</div>
		<p style="text-align: right;"><sup>*</sup>As described by patient and/or typical reason for use</p>
		<div class="section" id="declaration">
			<table class="table-condensed">
				<tr><td>
					<label class="checkbox-inline" for="confirm">
					<html:checkbox name="bpmh" styleId="confirm" property="confirm" value="checked"/>
					<bean:message key="colcamex.formBPMH.confirm" /></label>
				</td></tr>
			</table>
		</div>
		<div class="section">
			<table class="table-condensed" id="noteTable">
				<tr>
					<th>
						<bean:message key="colcamex.formBPMH.notes" />&nbsp;
					</th>
				</tr>
				<tr>
					<td id="notes">
						<logic:notEmpty name="bpmh" property="note" >
							<bean:write name="bpmh" property="note" />&nbsp;
						</logic:notEmpty>
						<logic:empty name="bpmh" property="note">
							<html:textarea style="width:100%;" property="note" > &nbsp;</html:textarea>
						</logic:empty>
					</td>
				</tr>
			</table>
		</div>

		<div class="section" id="patient-declaration" >
			<table class="table-condensed">
				<tr><th colspan="2">PATIENT ACKNOWLEDGEMENT</th></tr>
				<tr><td style="border:thin black solid;" colspan="2">My pharmacist has explained to me the purpose of a medication review service.
					I agreed that I could benefit from this publicly funded service. The review was conducted in a place that respected my privacy.
					During the appointment my pharmacist fully explained any medication changes or concerns to me.
					At the end of the medication review appointment, my pharmacist gave me a list of my current medications.
					The list includes any changes resulting from the medication review service provided.</td></tr>
				<tr><td>Signature of patient (or patient’s legal representative)</td><td>Date</td></tr>
				<tr><td style="height: 35px;"></td><td></td></tr>
			</table>
		</div>

		<div class="section donotprint" id="controls" >
			<table class="table-condensed">
				<tr>
					<td>
						<c:if test="${ missingDrugData }">
							<div class="red">Missing Medication Data</div>
						</c:if>
						<logic:empty name="bpmh" property="allergiesString">
							<div class="red">Allergy Notation is Required (ie: NKDA)</div>
						</logic:empty>
						<logic:empty name="bpmh" property="provider">
							<c:set var="controls" value="off" scope="page" />
							<div class="red" >
								<bean:message key="colcamex.formBPMH.error.missing.provider" />
							</div>
						</logic:empty>

						<c:if test="${ controls eq 'on' }" >
							<div class="donotprint">
								<input value="print" name="PrintButton" id="PrintButton" class="btn btn-info" type="button" onclick="printForm();">

								<logic:equal name="bpmh" property="formId" value="0">
									<html:submit property="method" styleClass="btn btn-primary">
										<bean:message key="colcamex.formBPMH.save" />
									</html:submit>
								</logic:equal>

								<!-- Buttons to increase or decrease font size on page -->
								<div class="fontsize pull-right donotprint">
									Font Size:
									<input value="+" name="incFontSize" id="incFontSize" type="button" class="btn btn-default">
									<input value="-" name="decFontSize" id="decFontSize" type="button" class="btn btn-default">
									<input value="reset" name="resetFontSize" id="resetFontSize" type="button" class="btn btn-caution">
								</div>
							</div>
						</c:if>

						<logic:messagesPresent message="true">
							<html:messages id="saved" message="true">
								<logic:present name="saved">
									<div class="messages">
										<bean:write name="saved" filter="false" />
									</div>
								</logic:present>
							</html:messages>
						</logic:messagesPresent>
					</td>
				</tr>
			</table>
		</div>
	</html:form>

	<!-- FOOTER -->
<%--	<div class="footer" style="font-size:60%">--%>
<%--		<span><bean:message key="colcamex.formBPMH.formowner" /></span>--%>
<%--	</div>--%>

	</body>
</html:html>