/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */
package oscar.form.pharmaForms.formBPMH.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.LoggedInInfo;

import oscar.form.pharmaForms.formBPMH.bean.BpmhFormBean;
import oscar.form.pharmaForms.formBPMH.business.BpmhFormHandler;

import oscar.form.pharmaForms.formBPMH.util.SortDrugList.Direction;
import oscar.form.pharmaForms.formBPMH.util.SortDrugList.SortableColumn;
/*
 * Author: Dennis Warren 
 * Company: Colcamex Resources
 * Date: November 2014
 * For: UBC Pharmacy Clinic and McMaster Department of Family Medicine
 */
public class BpmhSortAction extends DispatchAction {

	public ActionForward unspecified(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		return null;
	}
	
	public void sort(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BpmhFormBean bpmh = (BpmhFormBean) form;
		String direction = request.getParameter("sortorder");
		String column = request.getParameter("sortcolumn");
		Integer demographicNo = Integer.parseInt( bpmh.getDemographicNo() ); 
		BpmhFormHandler bpmhFormHandler = new BpmhFormHandler(bpmh);
		bpmhFormHandler.setDemographicNo( demographicNo ); 
		bpmhFormHandler.populateFormBean( LoggedInInfo.getLoggedInInfoFromSession(request) );		
		bpmhFormHandler.sortDrugList(SortableColumn.valueOf(column.toUpperCase()), Direction.valueOf(direction.toUpperCase()));

		response.setContentType("text/html");
		String path = mapping.findForward( "success" ).getPath();
	    request.getRequestDispatcher( path ).include(request, response);
	}
		
}
