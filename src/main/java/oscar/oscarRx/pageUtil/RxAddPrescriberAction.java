/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarRx.pageUtil;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.DrugDao;
import org.oscarehr.common.model.DemographicContact;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.ProfessionalContact;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

import oscar.log.LogAction;
import oscar.log.LogConst;

public class RxAddPrescriberAction extends DispatchAction  {
	
	private static final String REMOVE_PRESCRIBER = "-1";
	
	@Override
    public ActionForward unspecified(ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response)
    throws IOException, ServletException {

		String drugId = request.getParameter("drugId");
		String demographicContactId = request.getParameter("demographicContactId");

		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);		
		DrugDao drugDao = SpringUtils.getBean(DrugDao.class); 
		DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
		Drug drug = null;
		DemographicContact demographicContact = null;
		ProfessionalContact professionalContact = null;
		
		if(StringUtils.isNumeric(drugId)) {
			drug = drugDao.find(Integer.parseInt(drugId));
		}
		if(drug != null) {
			
			if(demographicContactId.equals(REMOVE_PRESCRIBER)) {
				drug.setDemographicContactId(null);
				drug.setOutsideProviderName(null);
				drug.setOutsideProviderOhip(null);
			} else {
				demographicContact = demographicManager.getHealthCareMemberbyId(loggedInInfo, Integer.parseInt(demographicContactId) );
				professionalContact = (ProfessionalContact) demographicContact.getDetails();
				drug.setDemographicContactId(demographicContact.getId());
				drug.setOutsideProviderName(demographicContact.getContactName());
				drug.setOutsideProviderOhip(professionalContact.getCpso());
			}
			
			drugDao.merge(drug);
			LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), LogConst.DELETE, LogConst.CON_PRESCRIPTION, 
					drugId, "local",
					drug.getDemographicId()+"", drug.getAuditString());
			  
		}

		ActionRedirect redirect = new ActionRedirect(mapping.findForward("success"));
		redirect.addParameter("demographicNo", drug.getDemographicId());
		redirect.addParameter("providerNo", loggedInInfo.getLoggedInProviderNo());
		return redirect;
	}
}
