/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarEncounter.pageUtil;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.DrugTherapyProblem;
import org.oscarehr.managers.DrugTherapyProblemManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

import oscar.util.StringUtils;

public class EctDisplayPharmacistsDTPPanel extends EctDisplayAction {
	
	private static Logger logger = Logger.getLogger(EctDisplayPharmacistsDTPPanel.class);
	private static final String cmd = "dtpPanel";
	private static final String DEFAULT_COLOUR = "#000"; // BLACK
	private String ctx;
	
	@Override
	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
	// TODO it is difficult to deprecate a parameter that a abstract method depends on. At least create an overload. 	
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		
		if( ! super.securityInfoManager.hasPrivilege(loggedInInfo, "_dtppanel", "r", null) ) {
			return Boolean.TRUE; // disable DTPPanel by removing security privilege 
		} 
		
		ctx = request.getContextPath();
		
		logger.info("Building Pharmacists DTP Panel");	
		// TODO add message properties.		
		// messages.getMessage(request.getLocale(), "oscarEncounter.LeftNavBar.Consult")

		String demographicNo = request.getParameter("demographicNo");
		String headingColour = request.getParameter("hC");

//		NavBarDisplayDAO.Item item = null;
//		String itemName = "";
		
		if( demographicNo == null ) {
			logger.error("Missing demographic number for the DTP Panel.");
			// TODO  there needs to be an alternate method for getting the demographicNo on postback in order to properly deprecate this bean
			demographicNo = bean.getDemographicNo();
		}
		
		if( headingColour == null ) {
			headingColour = DEFAULT_COLOUR;
		}
		
		Dao.setBootstrapPopover(Boolean.TRUE);

		// Attributes for the nav bar left link and title.
		Dao.setHeadingColour( headingColour );
		Dao.setLeftHeading("Drug Therapy Problems");		
		Dao.setDivId("drugTherapyProblems");
		Dao.setLeftURL( ctx + "/pharmaDTPPanel.do?method=fetchAll&demographicNo=" + demographicNo );
		Dao.setLeftHeadingPopoverTitle( " DTP Dashboard Display ");

		// Attributes for the nav bar right link ( for the plus(+) ).
		Dao.setRightHeadingID( "newDTPItem" );
		Dao.setRightURL( ctx + "/pharmaDTPPanel.do?method=create&demographicNo=" + demographicNo );	
		Dao.setRightHeadingPopoverTitle(" Add New DTP ");

		// Content list displayed under the nav bar heading.
		DrugTherapyProblemManager drugTherapyProblemManager = SpringUtils.getBean( DrugTherapyProblemManager.class );
		List<DrugTherapyProblem> drugTherapyProblems = drugTherapyProblemManager.findAllRecentlyUpdatedUnresolvedDTPByDemographicNo( loggedInInfo, Integer.parseInt( demographicNo ) );
		
		// return what has been established if there are no DTP entries.
		if( drugTherapyProblems == null ) {
			return Boolean.TRUE;
		}

		setDTPItemList( drugTherapyProblems, Dao );
		
		// launch
		// Dao.sortItems(NavBarDisplayDAO.DATESORT);
		return Boolean.TRUE;
	}

	/**
	 * This is the listing of active DTP's that appear under the 
	 * Drug Therapy Problem header of a patient encounter chart.
	 */
	private void setDTPItemList( List<DrugTherapyProblem> drugTherapyProblems, NavBarDisplayDAO Dao ) {
		
		NavBarDisplayDAO.Item item = null;
		
		for( DrugTherapyProblem drugTherapyProblem : drugTherapyProblems ) {
			
			item = NavBarDisplayDAO.Item();		
			
			Drug drug = drugTherapyProblem.getDrug();
						
			/* The issue name should NEVER be null. An NPE will result from a 
			null Issue.name. This is an indication of a bigger error. */
			String popOverTitle = drugTherapyProblem.getDrugTherapyProblemIssue().getName();
			
			// The drug name could be null if the drug is custom
			String title = null; 
			
			if( drug != null ) {
				title = drug.getGenericName();
				
				if( title == null ) {
					title = drug.getBrandName(); 
				}
				
				if( title == null ) {
					title = drug.getCustomName(); 
				}
			}
			
			if( title == null ) {
				title = popOverTitle;
				popOverTitle = "";
			}

			item.setBootstrapPopoverTitle( popOverTitle );
			item.setBootstrapPopoverURL( ctx + "/pharmaDTPPanel.do?method=fetchSingle&dtpId=" + drugTherapyProblem.getId() );
			item.setDate( drugTherapyProblem.getDateIdentified() );
			item.setLinkTitle( title );
			String itemName = StringUtils.maxLenString( title, 18, 15, ELLIPSES );
			itemName = String.format("%1$s<span class='dtpStatus' id='dtp_%2$s_%3$s' >%2$s</span>", itemName, drugTherapyProblem.getDrugTherapyProblemStatus().getName(), drugTherapyProblem.getId() );
			item.setTitle( itemName );
			item.setStyleClass( "dtpPanelItem" );
			item.setStyleId( "dtpPanelItem_" + drugTherapyProblem.getId() );
			item.setURL( "" );
			item.setURLJavaScript(Boolean.FALSE); // disable the Javascript feature so that the Boostrap link can be used.
			item.setBootstrapPopover( Boolean.TRUE );
			
			Dao.addItem( item );
		}
	}
	
	@Override
	public String getCmd() {
		return EctDisplayPharmacistsDTPPanel.cmd;
	}
	
}
