/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */
package org.oscarehr.common.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="DrugTherapyProblem")
public class DrugTherapyProblem extends AbstractModel<Integer> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=Drug.class)
	@JoinColumn( name="drug_id", referencedColumnName="drugid", insertable = false, updatable = false  )
	private Drug drug;

	@ManyToOne(targetEntity=DrugTherapyProblemEvent.class)
	@JoinColumn(name = "event_id", referencedColumnName="id", insertable = false, updatable = false)
	private DrugTherapyProblemEvent drugTherapyProblemEvent;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemStatus.class)
	@JoinColumn( name="status_id", referencedColumnName="id", insertable = false, updatable = false  )
	private DrugTherapyProblemStatus drugTherapyProblemStatus;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemPriority.class)
	@JoinColumn( name="priority_id", referencedColumnName="id", insertable = false, updatable = false  )
	private DrugTherapyProblemPriority drugTherapyProblemPriority;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemSeverity.class)
	@JoinColumn( name="severity_id", referencedColumnName="id", insertable = false, updatable = false  )
	private DrugTherapyProblemSeverity drugTherapyProblemSeverity;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemProbability.class)
	@JoinColumn( name="probability_id", referencedColumnName="id", insertable = false, updatable = false  )
	private DrugTherapyProblemProbability drugTherapyProblemProbability;
	
	@OneToMany(fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "dtp_id",referencedColumnName = "id")
	private List<CtlDrugTherapyProblemPharmacistAction> ctlDrugTherapyProblemPharmacistAction = new java.util.ArrayList<>();
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemIssue.class)
	@JoinColumn( name="issue_id", referencedColumnName="id", insertable = false, updatable = false  )
	private DrugTherapyProblemIssue drugTherapyProblemIssue;
	
	@OneToOne(fetch=FetchType.EAGER, targetEntity=DrugTherapyProblemComment.class)
	@JoinColumn( name="comment_id", referencedColumnName="id", insertable = false, updatable = false )
	private DrugTherapyProblemComment drugTherapyProblemComment;

	@Column(name="demographic_no")
	private Integer demographicNo;
	
	@Column(name="tracker_id")
	private String trackerId; 
	
	@Column(name="issue_id")
	private int issueId;

	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="drug_id")
	private Integer drugId;
	
	private String DIN;
	
	@Column(name="status_id")
	private int statusId;

	@Column(name="priority_id")
	private int priorityId;
	
	@Column(name="severity_id")
	private int severityId;
	
	@Column(name="probability_id")
	private int probabilityId;

	@Column(name="comment_id")
	private int commentId;
	
	@Column(name="date_entered")
	@Temporal(TemporalType.DATE)
	private Date dateEntered;
	
	@Column(name="date_identified")
	@Temporal(TemporalType.DATE)
	private Date dateIdentified;
	
	@Column(name="date_updated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateUpdated;
	
	@Column(name="date_response")
	@Temporal(TemporalType.DATE)
	private Date dateResponse;

	@Transient
	private List<DrugTherapyProblem> drugTherapyProblemHistory;

	public List<CtlDrugTherapyProblemPharmacistAction> getCtlDrugTherapyProblemPharmacistAction() {
		return ctlDrugTherapyProblemPharmacistAction;
	}

	public DrugTherapyProblemEvent getDrugTherapyProblemEvent() {
		if(this.drugTherapyProblemEvent == null) {
			this.drugTherapyProblemEvent = new DrugTherapyProblemEvent();
		}
		return drugTherapyProblemEvent;
	}

	public void setDrugTherapyProblemEvent(DrugTherapyProblemEvent drugTherapyProblemEvent) {
		this.drugTherapyProblemEvent = drugTherapyProblemEvent;
	}

	public String getEvent() {
		return getDrugTherapyProblemEvent().getEvent();
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDemographicNo() {
		return demographicNo;
	}

	public void setDemographicNo(Integer demographicNo) {
		this.demographicNo = demographicNo;
	}

	public String getTrackerId() {
		return trackerId;
	}

	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}
	
	public String getDrugName() {
		Drug drug = getDrug();
		String name = "";
		if( drug != null ) {
			name = drug.getGenericName();
			
			if( name == null || name.isEmpty() ) {
				name = drug.getBrandName();
			}
			
			if( name == null || name.isEmpty() ) {
				name = drug.getCustomName();
			}

			if(drug.isArchived()) {
				name = name + "(discontinued)";
			}
		}
		return name;
	}

	public Drug getDrug() {
		return drug;
	}

	public void setDrug(Drug drug) {
		this.drug = drug;
	}
	
	public String getStatus() {
		return getDrugTherapyProblemStatus().getName();
	}

	public DrugTherapyProblemStatus getDrugTherapyProblemStatus() {
		return drugTherapyProblemStatus;
	}

	public void setDrugTherapyProblemStatus(DrugTherapyProblemStatus drugTherapyProblemStatus) {
		this.drugTherapyProblemStatus = drugTherapyProblemStatus;
	}
	
	public String getPriority() {
		return getDrugTherapyProblemPriority().getName();
	}

	public DrugTherapyProblemPriority getDrugTherapyProblemPriority() {
		return drugTherapyProblemPriority;
	}

	public void setDrugTherapyProblemPriority(DrugTherapyProblemPriority drugTherapyProblemPriority) {
		this.drugTherapyProblemPriority = drugTherapyProblemPriority;
	}
	
	public String getSeverity() {
		return getDrugTherapyProblemSeverity().getName();
	}

	public DrugTherapyProblemSeverity getDrugTherapyProblemSeverity() {
		return drugTherapyProblemSeverity;
	}

	public void setDrugTherapyProblemSeverity(DrugTherapyProblemSeverity drugTherapyProblemSeverity) {
		this.drugTherapyProblemSeverity = drugTherapyProblemSeverity;
	}
	
	public String getProbability() {
		return getDrugTherapyProblemProbability().getName();
	}

	public DrugTherapyProblemProbability getDrugTherapyProblemProbability() {
		return drugTherapyProblemProbability;
	}

	public void setDrugTherapyProblemProbability(DrugTherapyProblemProbability drugTherapyProblemProbability) {
		this.drugTherapyProblemProbability = drugTherapyProblemProbability;
	}

	public List<CtlDrugTherapyProblemPharmacistAction> getDrugTherapyProblemPharmacistAction() {
		return ctlDrugTherapyProblemPharmacistAction;
	}

	public void setDrugTherapyProblemPharmacistAction(List<CtlDrugTherapyProblemPharmacistAction> drugTherapyProblemPharmacistAction) {
		this.ctlDrugTherapyProblemPharmacistAction = drugTherapyProblemPharmacistAction;
	}
	
	public String getIssue() {
		return getDrugTherapyProblemIssue().getName();
	}

	public DrugTherapyProblemIssue getDrugTherapyProblemIssue() {
		return drugTherapyProblemIssue;
	}

	public void setDrugTherapyProblemIssue(DrugTherapyProblemIssue drugTherapyProblemIssue) {
		this.drugTherapyProblemIssue = drugTherapyProblemIssue;
	}
	
	public String getComment() {
		return getDrugTherapyProblemComment().getComment();
	}

	public DrugTherapyProblemComment getDrugTherapyProblemComment() {
		return drugTherapyProblemComment;
	}

	public void setDrugTherapyProblemComment(DrugTherapyProblemComment drugTherapyProblemComment) {
		this.drugTherapyProblemComment = drugTherapyProblemComment;
	}

	public Integer getDrugId() {
		return drugId;
	}

	public void setDrugId(Integer drugId) {
		this.drugId = drugId;
	}

	public String getDIN() {
		return DIN;
	}

	public void setDIN(String dIN) {
		DIN = dIN;
	}

	public int getIssueId() {
		return issueId;
	}

	public void setIssueId(int issueId) {
		this.issueId = issueId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public int getSeverityId() {
		return severityId;
	}

	public void setSeverityId(int severityId) {
		this.severityId = severityId;
	}

	public int getProbabilityId() {
		return probabilityId;
	}

	public void setProbabilityId(int probabilityId) {
		this.probabilityId = probabilityId;
	}

	public Date getDateEntered() {
		if( dateEntered == null ) {
			return getDateUpdated();
		}
		return dateEntered;
	}

	public void setDateEntered(Date dateEntered) {
		this.dateEntered = dateEntered;
	}

	public Date getDateIdentified() {
		return dateIdentified;
	}

	public void setDateIdentified(Date dateIdentified) {
		this.dateIdentified = dateIdentified;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Date getDateResponse() {
		return dateResponse;
	}

	public void setDateResponse(Date dateResponse) {
		this.dateResponse = dateResponse;
	}

	public List<DrugTherapyProblem> getDrugTherapyProblemHistory() {
		return drugTherapyProblemHistory;
	}

	public void setDrugTherapyProblemHistory(List<DrugTherapyProblem> drugTherapyProblemHistory) {
		this.drugTherapyProblemHistory = drugTherapyProblemHistory;
	}
}
