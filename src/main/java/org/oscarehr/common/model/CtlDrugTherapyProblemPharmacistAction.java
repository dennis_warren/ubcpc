/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */

package org.oscarehr.common.model;

import javax.persistence.*;
@Entity
@Table(name = "ctl_drugTherapyProblemPharmacistAction")
public class CtlDrugTherapyProblemPharmacistAction extends AbstractModel<Integer>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name="dtp_action_id")
    private int dtpActionId;
    @Column(name="dtp_id")
    private int dtpId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dtp_id", referencedColumnName="id", insertable = false, updatable = false)
    private DrugTherapyProblem drugTherapyProblem;

    @ManyToOne(fetch=FetchType.EAGER, targetEntity = DrugTherapyProblemPharmacistAction.class)
    @JoinColumn(name = "dtp_action_id", referencedColumnName="id", insertable = false, updatable = false)
    private DrugTherapyProblemPharmacistAction drugTherapyProblemPharmacistAction;

    public DrugTherapyProblemPharmacistAction getDrugTherapyProblemPharmacistAction() {
        return drugTherapyProblemPharmacistAction;
    }

    public DrugTherapyProblem getDrugTherapyProblem() {
        return drugTherapyProblem;
    }

    public void setDrugTherapyProblem(DrugTherapyProblem drugTherapyProblem) {
        this.drugTherapyProblem = drugTherapyProblem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getDtpActionId() {
        return dtpActionId;
    }

    public void setDtpActionId(int dtpActionId) {
        this.dtpActionId = dtpActionId;
    }

    public int getDtpId() {
        return dtpId;
    }

    public void setDtpId(int dtpId) {
        this.dtpId = dtpId;
    }
}