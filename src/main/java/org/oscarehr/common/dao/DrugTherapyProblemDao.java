/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.DrugTherapyProblem;
import org.springframework.stereotype.Repository;

@Repository
public class DrugTherapyProblemDao extends AbstractDao<DrugTherapyProblem> {

	protected DrugTherapyProblemDao() {
		super( DrugTherapyProblem.class );
	}
	
	/**
	 * Return a list of distinct drug therapy problems for this demographic.
	 * A new entry is created for every DTP update. This method condenses the problems to a distinct list. This 
	 * list does not represent any specific DTP entry during the treatment period.
	 * Remember: 
	 * One drug can have many drug therapy problems.
	 * One drug therapy problem can have many drugs.
	 * One drug therapy Problem and drug can represent multiple entries with unique update dates and id's.
	 */
	@SuppressWarnings("unchecked")
	public List<DrugTherapyProblem> findDistinctDrugTherapyProblemsByDemographicNo( int demographicNo ) {
		String sql = "SELECT x FROM " + this.modelClass.getName() + " x WHERE x.demographicNo=? "
				+ "GROUP BY x.trackerId HAVING COUNT( x.trackerId ) > -1";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, demographicNo);

		List<DrugTherapyProblem> dtps = query.getResultList();
		return dtps;
	}
	
	/**
	 * Returns the entire history of DTP's for this patient.
	 */
	@SuppressWarnings("unchecked")
	public List<DrugTherapyProblem> findAllByDemographicNo( int demographicNo ) {
		String sql = "SELECT x FROM " + this.modelClass.getName() + " x WHERE x.demographicNo=?";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, demographicNo);

		List<DrugTherapyProblem> dtps = query.getResultList();
		return dtps;
	}
	
	/**
	 * A tracker id will be the same in all DTP entries that together make up a treatment history. 
	 */
	@SuppressWarnings("unchecked")
	public List<DrugTherapyProblem> findAllDrugTherapyProblemByTrackerId( int demographicNo, String trackerId ) {
		String sql = "SELECT x FROM " + this.modelClass.getName() 
			+ " x WHERE x.demographicNo=? AND x.trackerId=? ORDER BY x.dateUpdated DESC";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, demographicNo);
		query.setParameter(2, trackerId );

		List<DrugTherapyProblem> dtps = query.getResultList();
		return dtps;
	}
	
	/**
	 * A tracker id will be the same in all DTP entries that together make up a treatment history.
	 * This method returns only the most recently updated DTP 
	 */
	public DrugTherapyProblem findMostRecentDrugTherapyProblemByTrackerId( int demographicNo, String trackerId ) {
		String sql = "SELECT x FROM " + this.modelClass.getName() 
			+ " x WHERE x.demographicNo=? AND x.trackerId=? ORDER BY x.dateUpdated DESC";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, demographicNo);
		query.setParameter(2, trackerId );
		query.setMaxResults(1);
		
		DrugTherapyProblem dtps = super.getSingleResultOrNull(query);
		return dtps;
	}
	
	
	/**
	 * Get an entire list of drugs that are assigned a specific drug therapy problem.
	 * Do not depend on this query for update history, this only returns one random drug to therapy problem relationship each.  
	 * One drug therapy problem can have many drugs.
	 */
	@SuppressWarnings("unchecked")
	public List<DrugTherapyProblem> findDrugTherapyProblemByDemographicNoAndProblem( int drugTherapyProblemId ) {
		String sql = "SELECT x FROM " + this.modelClass.getName() 
			+ " x WHERE x.drugTherapyProblemId=?";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, drugTherapyProblemId);

		List<DrugTherapyProblem> dtps = query.getResultList();
		return dtps;
	}


}
