/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.managers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.oscarehr.common.dao.DxresearchDAO;
import org.oscarehr.common.model.Dxresearch;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Service;
import oscar.log.LogAction;
import oscar.oscarResearch.oscarDxResearch.bean.dxCodeSearchBean;

@Service
public class DxResearchManager {
	
	public static final char ACTIVE = 'A';
	public static final char RESOLVED = 'C';
	public static final char DELETED = 'D';
	
	public List<Dxresearch> findAllActiveByDemographicNoWithCodeDescription( LoggedInInfo loggedinInfo, Integer demographicNo ) {
		
		List<Dxresearch> dxresearchList = findAllActiveByDemographicNo( loggedinInfo, demographicNo );
		
		LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.DemographicNoWithCodeDescription",
				"Returning Dx Research list " +  dxresearchList + " for D: " + demographicNo);
		
		return getListWithCodeDescription( dxresearchList );
	}

	public List<Dxresearch> findAllActiveByDemographicNo( LoggedInInfo loggedinInfo, Integer demographicNo ) {
		DxresearchDAO dxResearchDao = SpringUtils.getBean(DxresearchDAO.class);
		List<Dxresearch> dxResearchList = dxResearchDao.findNonDeletedByDemographicNo(demographicNo);		
		LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.findAllActiveByDemographicNo",
				"Returning Dx Research list " +  dxResearchList + " for D: " + demographicNo);
		return dxResearchList;
	}

	public void deleteDemographicDxResearchCode(LoggedInInfo loggedinInfo, Integer demographicNo, String dxCode, String dxSystem) {	
		DxresearchDAO dxResearchDao = SpringUtils.getBean(DxresearchDAO.class);
		List<Dxresearch> dxResearchList =  dxResearchDao.find(demographicNo, dxSystem, dxCode);	
		if( dxResearchList != null && dxResearchList.size() > 0 ) {
			LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.deleteDemographicDxResearchCode",
					"Deleting Dx Code " + dxCode + " from demographic " + demographicNo);			
			for( Dxresearch dxResearch : dxResearchList ){
				dxResearch.setStatus(DxResearchManager.DELETED);
				dxResearchDao.merge(dxResearch);
			}
		}	
	}
	
	public boolean addToDemographicDxResearch(LoggedInInfo loggedinInfo, Integer demographicNo, String dxCode, String dxSystem) {	
		boolean success = Boolean.FALSE;
		DxresearchDAO dxResearchDao = SpringUtils.getBean(DxresearchDAO.class);
		CodingSystemManager codingSystemManager = new CodingSystemManager();

		if( ! codingSystemManager.isCodeAvailable(dxSystem, dxCode) ) {
			LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.addToDemographicDxResearch",
					"Error: given Dx Code is not valid: " + dxCode + " or system does not exist: " + dxSystem);
			return success;
		}
		
		if( ! dxResearchDao.activeEntryExists(demographicNo, dxSystem, dxCode) ) {
			Date today = Calendar.getInstance().getTime();
			Dxresearch dxResearch = new Dxresearch();
			dxResearch.setCodingSystem(dxSystem);
			dxResearch.setDxresearchCode(dxCode);
			dxResearch.setDemographicNo(demographicNo);
			dxResearch.setStatus(DxResearchManager.ACTIVE);
			dxResearch.setUpdateDate(today);
			dxResearch.setStartDate(today);

			LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.addToDemographicDxResearch",
					"Added new Dx to registry for D: " + demographicNo);
		
			dxResearchDao.save(dxResearch);
			
			if(dxResearch.getDxresearchNo() > 0) {
				success = Boolean.TRUE;
			}
		}
		
		return success;
	}
	
	
	public List<Dxresearch> findInDemographicDxResearch(LoggedInInfo loggedinInfo, Integer demographicNo, String dxCode, String dxSystem) {
		DxresearchDAO dxResearchDao = SpringUtils.getBean(DxresearchDAO.class);
		List<Dxresearch> dxResearchList = dxResearchDao.find(demographicNo, dxSystem, dxCode);		
		LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.findInDemographicDxResearch",
				"Returning Dx Research list " +  dxResearchList + " for D: " + demographicNo);
		return dxResearchList;
	}
	
	public List<dxCodeSearchBean> getDxQuickList(LoggedInInfo loggedinInfo, String quickListName) {		
		DxresearchDAO dxResearchDao = SpringUtils.getBean(DxresearchDAO.class);
		LogAction.addLogSynchronous(loggedinInfo,"DxResearchManager.getDxQuickList", 
				"Returning Dx quicklist " + quickListName);
		return dxResearchDao.getQuickListItems(quickListName);
	}
	
	public static String getCodeDescription( Dxresearch dxresearch ) {
		String codeDescription = "";
		CodingSystemManager codingSystemManager = new CodingSystemManager();
		if( dxresearch != null && codingSystemManager.isCodeAvailable( dxresearch.getCodingSystem(), dxresearch.getDxresearchCode() ) ) {
			codeDescription = codingSystemManager.getCodeDescription( dxresearch.getCodingSystem(), dxresearch.getDxresearchCode() );
		}
		return codeDescription;
	}
	
	public static List<Dxresearch> getListWithCodeDescription( List<Dxresearch> dxresearchList ) {
		
		if( dxresearchList != null && dxresearchList.size() > 0 ) {
			for(Dxresearch dxresearch : dxresearchList) {
				dxresearch.setCodeDescription( getCodeDescription( dxresearch ) );
			}
		}
		
		return dxresearchList;
	}
}
