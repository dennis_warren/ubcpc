/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.managers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.oscarehr.casemgmt.web.formbeans.DrugTherapyProblemFormBean;
import org.oscarehr.common.dao.*;
import org.oscarehr.common.model.*;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oscar.log.LogAction;

@Service
public class DrugTherapyProblemManager {

	@Autowired
	private DrugTherapyProblemDao drugTherapyProblemDao;
	@Autowired
	private DrugTherapyProblemStatusDao drugTherapyProblemStatusDao;
	@Autowired
	private DrugTherapyProblemPriorityDao drugTherapyProblemPriorityDao;
	@Autowired
	private DrugTherapyProblemIssueDao drugTherapyProblemIssueDao;
	@Autowired
	private DrugTherapyProblemPharmacistActionDao drugTherapyProblemPharmacistActionDao;
	@Autowired
	private DrugTherapyProblemSeverityDao drugTherapyProblemSeverityDao;
	@Autowired
	private DrugTherapyProblemProbabilityDao drugTherapyProblemProbabilityDao;
	@Autowired
	private DrugTherapyProblemCommentDao drugTherapyProblemCommentDao;

	@Autowired
	private DrugTherapyProblemEventDao drugTherapyProblemEventDao;

	@Autowired
	private PrescriptionManager prescriptionManager;

	@Autowired
	private CtlDrugTherapyProblemPharmacistActionDao ctlDrugTherapyProblemPharmacistActionDao;
	
	private final String DTP_RESOLVED_STATUS = "resolved";
	
	public List<DrugTherapyProblem> findAllDTPByDemographicNo(LoggedInInfo loggedInInfo, int demographicNo) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager.findAllDTPByDemographicNo", "demgraphicNo=" + demographicNo);

		List<DrugTherapyProblem> drugTherapyProblemList = findAllRecentlyUpdatedDTPByDemographicNo(loggedInInfo, demographicNo);

		for(DrugTherapyProblem drugTherapyProblem : drugTherapyProblemList) {
			List<DrugTherapyProblem> drugTherapyProblemHistoryListSorted = new ArrayList<>();
			List<DrugTherapyProblem> drugTherapyProblemHistory = findDTPandHistoryByTrackerId(loggedInInfo, demographicNo, drugTherapyProblem.getTrackerId() );
			for(DrugTherapyProblem drugTherapyProblemHistorySorted : drugTherapyProblemHistory) {
				if(!Objects.equals(drugTherapyProblemHistorySorted.getId(), drugTherapyProblem.getId())) {
					drugTherapyProblemHistoryListSorted.add(drugTherapyProblemHistorySorted);
				}
			}
			Collections.sort( drugTherapyProblemHistoryListSorted, new UpdateDateComparator() );
			drugTherapyProblem.setDrugTherapyProblemHistory(drugTherapyProblemHistoryListSorted);
		}

		return drugTherapyProblemList;
	}
	
	public List<DrugTherapyProblem> findAllRecentlyUpdatedUnresolvedDTPByDemographicNo(LoggedInInfo loggedInInfo, int demographicNo) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager", "demgraphicNo=" + demographicNo);
		List<DrugTherapyProblem> recentlyUpdatedUnresolvedDTPList = null;
		
		// get a list of distinct problems
		List<DrugTherapyProblem> problemList = drugTherapyProblemDao.findDistinctDrugTherapyProblemsByDemographicNo( demographicNo );
		DrugTherapyProblemStatus drugTherapyProblemStatus = drugTherapyProblemStatusDao.findByStatusName( DTP_RESOLVED_STATUS );
		
		// get the most recently updated entry from the history of each distinct problem
		for( DrugTherapyProblem problem : problemList ) {
			if( recentlyUpdatedUnresolvedDTPList == null ) {
				recentlyUpdatedUnresolvedDTPList = new ArrayList<DrugTherapyProblem>();
			}
			String trackerId = problem.getTrackerId();
			DrugTherapyProblem recentDTP = drugTherapyProblemDao.findMostRecentDrugTherapyProblemByTrackerId( demographicNo, trackerId );
			
			if( recentDTP != null && recentDTP.getStatusId() != drugTherapyProblemStatus.getId() ) {
				recentlyUpdatedUnresolvedDTPList.add( recentDTP );
			}
		}
		
		if( recentlyUpdatedUnresolvedDTPList != null ) {
			Collections.sort( recentlyUpdatedUnresolvedDTPList, new UpdateDateComparator() );
		}
		return recentlyUpdatedUnresolvedDTPList;
	}
	
	public List<DrugTherapyProblem> findAllRecentlyUpdatedDTPByDemographicNo(LoggedInInfo loggedInInfo, int demographicNo) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager", "demgraphicNo=" + demographicNo);
		List<DrugTherapyProblem> recentlyUpdatedDTPList = null;
		
		// get a list of distinct problems
		List<DrugTherapyProblem> problemList = drugTherapyProblemDao.findDistinctDrugTherapyProblemsByDemographicNo( demographicNo );
		
		// get the most recently updated entry from the history of each distinct problem
		for( DrugTherapyProblem problem : problemList ) {
			if( recentlyUpdatedDTPList == null ) {
				recentlyUpdatedDTPList = new ArrayList<DrugTherapyProblem>();
			}
			String trackerId = problem.getTrackerId();
			DrugTherapyProblem recentDTP = drugTherapyProblemDao.findMostRecentDrugTherapyProblemByTrackerId( demographicNo, trackerId );
			
			if( recentDTP != null ) {
				recentlyUpdatedDTPList.add( recentDTP );
			}
		}
		if( recentlyUpdatedDTPList != null ) {
			Collections.sort(recentlyUpdatedDTPList, new UpdateDateComparator() );
		}
		return recentlyUpdatedDTPList;
	}
	
	public List<DrugTherapyProblem> findDTPandHistoryById(LoggedInInfo loggedInInfo, int dtpId) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager.findDTPandHistoryById", "dtpId=" + dtpId);
		DrugTherapyProblem drugTherapyProblem = findDTPById(loggedInInfo, dtpId);
		List<DrugTherapyProblem> drugTherapyProblemList = null;
		if(drugTherapyProblem != null) {
			drugTherapyProblemList = findDTPandHistoryByTrackerId(loggedInInfo, drugTherapyProblem.getDemographicNo(), drugTherapyProblem.getTrackerId() );
		}
		return drugTherapyProblemList;
	}
	
	public List<DrugTherapyProblem> findDTPandHistoryByTrackerId(LoggedInInfo loggedInInfo, int demographicNo, String trackerId ) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager.findDTPandHistoryByTrackerId", "Tracker Id=" + trackerId);
		List<DrugTherapyProblem> drugTherapyProblemList = drugTherapyProblemDao.findAllDrugTherapyProblemByTrackerId(demographicNo, trackerId);
		
		if( drugTherapyProblemList != null ) {
			Collections.sort( drugTherapyProblemList, new UpdateDateComparator() );
		}
		
		return drugTherapyProblemList;
	}
	
	public DrugTherapyProblem findDTPById(LoggedInInfo loggedInInfo, int dtpId) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager.findDTPById", "dtpId=" + dtpId);
		return drugTherapyProblemDao.find(dtpId);
	}
	
	public int saveNewDrugTherapyProblem( LoggedInInfo loggedInInfo, DrugTherapyProblem drugTherapyProblem ) {
		LogAction.addLogSynchronous(loggedInInfo, " DrugTherapyProblemManager", 
				"Saved new drug therapy problem for demographicNo: " + drugTherapyProblem.getDemographicNo() );
		
		drugTherapyProblemDao.persist(drugTherapyProblem);
		return drugTherapyProblem.getId();
	}

	public List<DrugTherapyProblemStatus> getStatusList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemStatusDao.findAll(null, null);
	}

	public List<DrugTherapyProblemPriority> getPriorityList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemPriorityDao.findAll(null, null);
	}
	
	public List<DrugTherapyProblemPharmacistAction> getPharmacistActionList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemPharmacistActionDao.findAll(null, null);
	}
	
	public List<DrugTherapyProblemSeverity> getSeverityList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemSeverityDao.findAll(null, null);
	}
	
	public List<DrugTherapyProblemIssue> getIssueList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemIssueDao.findAll(null, null);
	}
	
	public List<DrugTherapyProblemProbability> getProbabilityList( LoggedInInfo loggedInInfo ) {
		return drugTherapyProblemProbabilityDao.findAll(null, null);
	}
	
	public int saveDrugTherapyProblemComment( LoggedInInfo loggedInInfo, DrugTherapyProblemComment drugTherapyProblemComment ) {
		drugTherapyProblemCommentDao.persist( drugTherapyProblemComment );
		return drugTherapyProblemComment.getId();
	}

	public int saveDrugTherapyProblemEvent(LoggedInInfo loggedInInfo, DrugTherapyProblemEvent drugTherapyProblemEvent) {
		List<DrugTherapyProblemEvent> drugTherapyProblemEventList = drugTherapyProblemEventDao.findByEvent(drugTherapyProblemEvent.getEvent());
		if(drugTherapyProblemEventList != null && drugTherapyProblemEventList.size() > 0) {
			/*
			 * returns only exact matches. Take the first match if there are
			 * multiples
			 */
			drugTherapyProblemEvent = drugTherapyProblemEventList.get(0);
		} else {
			/*
			 * create a new event if no exact matches are found.
			 */
			drugTherapyProblemEventDao.persist(drugTherapyProblemEvent);
		}
		return drugTherapyProblemEvent.getId();
	}

	public List<DrugTherapyProblemEvent> searchEvent(LoggedInInfo loggedInInfo, String keyword) {
		return drugTherapyProblemEventDao.searchByEvent(keyword);
	}

	public void saveDrugTherapyProblemPharmacistAction(LoggedInInfo loggedInInfo, CtlDrugTherapyProblemPharmacistAction ctlDrugTherapyProblemPharmacistAction) {
		ctlDrugTherapyProblemPharmacistActionDao.persist(ctlDrugTherapyProblemPharmacistAction);
	}

	public void saveBean( LoggedInInfo loggedInInfo, DrugTherapyProblem drugTherapyProblem, DrugTherapyProblemFormBean drugTherapyProblemForm ) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
		int drugId = drugTherapyProblemForm.getDrugId();
		drugTherapyProblem.setDrugId( null );
		Drug selectedDrug = null;
		Date today = new Date(System.currentTimeMillis());
		String dateEntered = drugTherapyProblemForm.getDateEntered();
		String dateIdentified = drugTherapyProblemForm.getDateIdentified();
		String dateResponse = drugTherapyProblemForm.getDateResponse();
		boolean hasEvent = drugTherapyProblemForm.isHasEvent();

		if( drugId > 0 ) {
			selectedDrug = prescriptionManager.findDrugById( loggedInInfo, drugTherapyProblemForm.getDrugId() );
		}

		if( selectedDrug != null ) {
			drugTherapyProblem.setDIN( selectedDrug.getRegionalIdentifier() );
			drugTherapyProblem.setDrugId( selectedDrug.getId() );
		}

		drugTherapyProblem.setDemographicNo( Integer.parseInt(drugTherapyProblemForm.getDemographicNo()) );
		drugTherapyProblem.setPriorityId( drugTherapyProblemForm.getPriorityId() );
		drugTherapyProblem.setProbabilityId( drugTherapyProblemForm.getProbabilityId() );
		drugTherapyProblem.setIssueId( drugTherapyProblemForm.getIssueId() );
		drugTherapyProblem.setSeverityId( drugTherapyProblemForm.getSeverityId() );
		drugTherapyProblem.setStatusId( drugTherapyProblemForm.getStatusId() );
		drugTherapyProblem.setCommentId( 0 );
		drugTherapyProblem.setEventId( null );
		drugTherapyProblem.setDateUpdated( today );

		if( ! drugTherapyProblemForm.getComment().isEmpty() ) {
			DrugTherapyProblemComment drugTherapyProblemComment = new DrugTherapyProblemComment();
			drugTherapyProblemComment.setComment( drugTherapyProblemForm.getComment() );
			int commentId = saveDrugTherapyProblemComment( loggedInInfo, drugTherapyProblemComment );
			drugTherapyProblem.setCommentId( commentId );
		}

		// if insert event is indicated.
		if(hasEvent) {
			DrugTherapyProblemEvent drugTherapyProblemEvent = new DrugTherapyProblemEvent();
			drugTherapyProblemEvent.setEvent(drugTherapyProblemForm.getEvent());
			int eventId = saveDrugTherapyProblemEvent(loggedInInfo, drugTherapyProblemEvent);
			drugTherapyProblem.setEventId(eventId);
		}

		if( dateIdentified != null && ! dateIdentified.isEmpty() ) {
			try {
				drugTherapyProblem.setDateIdentified( dateFormat.parse(dateIdentified) );
				drugTherapyProblem.setDateEntered( dateFormat.parse(dateIdentified)  );
			} catch (ParseException e) {
				MiscUtils.getLogger().error("Error parsing DTP DateIdentified: " + dateIdentified, e);
			}
		}

		if( dateEntered != null && ! dateEntered.isEmpty() ) {
			try {
				drugTherapyProblem.setDateEntered( dateFormat.parse( dateEntered ) );
			} catch (ParseException e) {
				MiscUtils.getLogger().error("Error parsing DTP DateEntered: " + dateEntered, e);
			}
		}

		if( dateResponse != null && ! dateResponse.isEmpty() ) {
			try {
				drugTherapyProblem.setDateResponse( dateFormat.parse(dateResponse) );
			} catch (ParseException e) {
				MiscUtils.getLogger().error("Error parsing DTP Date: " + dateResponse, e);
			}
		}

		int drugTherapyProblemId = saveNewDrugTherapyProblem(loggedInInfo, drugTherapyProblem );

		// set up the Pharmacist Actions
		List<CtlDrugTherapyProblemPharmacistAction> ctlPharmacistActionList = drugTherapyProblemForm.getCtlPharmacistActionId();
		for(CtlDrugTherapyProblemPharmacistAction ctlPharmacistAction : ctlPharmacistActionList) {
			ctlPharmacistAction.setDtpId(drugTherapyProblemId);
			saveDrugTherapyProblemPharmacistAction(loggedInInfo, ctlPharmacistAction);
		}
	}

	public void setBean( LoggedInInfo loggedInInfo, DrugTherapyProblemFormBean drugTherapyProblemFormBean ) {

		String demographicNo = drugTherapyProblemFormBean.getDemographicNo();

		if( demographicNo != null ) {
			List<Drug> medications = prescriptionManager.getMedicationsByDemographicNo(loggedInInfo, Integer.parseInt(demographicNo), null );
			drugTherapyProblemFormBean.setMedications( medications );
		}

		drugTherapyProblemFormBean.setPriorityList( getPriorityList(loggedInInfo) );
		drugTherapyProblemFormBean.setStatusList( getStatusList(loggedInInfo) );
		drugTherapyProblemFormBean.setIssueList( getIssueList(loggedInInfo) );
		drugTherapyProblemFormBean.setSeverityList( getSeverityList(loggedInInfo) );
		drugTherapyProblemFormBean.setPharmacistActionList( getPharmacistActionList(loggedInInfo) );
		drugTherapyProblemFormBean.setProbabilityList( getProbabilityList(loggedInInfo) );
	}

}

class UpdateDateComparator implements Comparator<DrugTherapyProblem> {
    @Override
    public int compare(DrugTherapyProblem a, DrugTherapyProblem b) {
        if (a.getDateUpdated() == null || b.getDateUpdated() == null) {
        	return 0;
        }
            
        return a.getDateUpdated().compareTo( b.getDateUpdated() );
    }
}
