/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */

package org.oscarehr.managers;

import java.util.Date;

import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemPreferenceManager {
	
	@Autowired
	private SecurityInfoManager securityInfoManager;
	@Autowired
	private SystemPreferencesDao systemPreferencesDao; 

	public SystemPreferenceManager() {
		// default stub
	}
	
	public String getPreferenceValueByName(LoggedInInfo loggedInInfo, Enum<?> preferenceName) {
		if(!securityInfoManager.hasPrivilege(loggedInInfo, "_admin", SecurityInfoManager.READ, null)) {
			throw new RuntimeException("Access Denied");
		}
		String preference = null;
		SystemPreferences systemPreferences = getPreferenceByName(loggedInInfo, preferenceName);
		if(systemPreferences != null)
		{
			preference = systemPreferences.getValue();
		}
		return preference;
	}
	
	public SystemPreferences getPreferenceByName(LoggedInInfo loggedInInfo, Enum<?> preferenceName) {
		if(!securityInfoManager.hasPrivilege(loggedInInfo, "_admin", SecurityInfoManager.READ, null)) {
			throw new RuntimeException("Access Denied");
		}

		return systemPreferencesDao.findPreferenceByName(preferenceName.name());
	}
	
	public Integer setPreferenceValueByName(LoggedInInfo loggedInInfo, Enum<?> preferenceName, String value) {
		if(!securityInfoManager.hasPrivilege(loggedInInfo, "_admin", SecurityInfoManager.WRITE, null)) {
			throw new RuntimeException("Access Denied");
		}

		SystemPreferences systemPreferences = getPreferenceByName(loggedInInfo, preferenceName);
		if(systemPreferences != null)
		{
			systemPreferences.setValue(value);
			systemPreferences.setUpdateDate(new Date());
			systemPreferencesDao.merge(systemPreferences);
		}
		else
		{
			systemPreferences = new SystemPreferences(preferenceName.name(), value);
			systemPreferencesDao.persist(systemPreferences);
		}
		
		return systemPreferences.getId();
	}

}
