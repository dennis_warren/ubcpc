package org.oscarehr.util;

import oscar.OscarProperties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Fetches images from OSCAR's eform image library.
 */
public class ImageLibraryAction extends HttpServlet {

    private String imageDirectory;

    public void init() throws ServletException {
        imageDirectory = OscarProperties.getInstance().getProperty("eform_image");
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        String logoImage = request.getPathInfo();
        Path imagePath = null;
        String contentType = null;

        if(logoImage != null) {
            imagePath = Paths.get(imageDirectory, URLDecoder.decode(logoImage, "UTF-8"));
        }

        // Get content type by filename.
        if(imagePath != null && Files.exists(imagePath))
        {
            contentType = getServletContext().getMimeType(imagePath.getFileName().toString());
        }

        if (contentType != null && contentType.startsWith("image"))
        {
            response.reset();
            response.setContentType(contentType);
            response.setHeader("Content-Length", String.valueOf(imagePath.toFile().length()));

            // Write image content to response.
            Files.copy(imagePath, response.getOutputStream());
        }
    }

    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return;
    }

}
