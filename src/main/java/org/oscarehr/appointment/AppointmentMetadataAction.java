/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */

package org.oscarehr.appointment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.model.LookupList;
import org.oscarehr.common.model.LookupListItem;
import org.oscarehr.common.model.SystemPreferences.APPOINTMENT_METADATA_LOOKUP_LIST;
import org.oscarehr.managers.LookupListManager;
import org.oscarehr.managers.SystemPreferenceManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

/**
 * 
 * An action class that handles requests for and edits to
 * the various Appointment meta data.  Such as Appointment: 
 * reason
 * type
 * location
 * Service Mode
 * Status 
 * 
 * This enables users to add icons and/or colours to each data 
 * element.
 * 
 */
public class AppointmentMetadataAction extends DispatchAction {
	
	private LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
	private SystemPreferenceManager systemPreferenceManager = SpringUtils.getBean(SystemPreferenceManager.class);	
	private static enum ListName { REASON, STATUS, TYPE, SERVICE, LOCATION, REFERRAL }
	
	/**
	 * Return a list reference enum for the given appointment meta data request.
	 * 
	 * Note: status is routed through through its original Actionclass: /appointment/appointmentstatuscontrol.jsp - not here
	 * status is just a placeholder for future development.
	 * Type is also a placeholder for future development. 
	 * 
	 * @param listname
	 * @return APPOINTMENT_METADATA_LOOKUP_LIST enum
	 */
	private APPOINTMENT_METADATA_LOOKUP_LIST getListReferenceType(ListName listname) {
		
		switch(listname)
		{
			case LOCATION	: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_location_lookuplist;
			case REASON		: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_reason_lookuplist;
			case SERVICE	: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_service_lookuplist;
			case STATUS		: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_status_lookuplist;
			case TYPE		: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_type_lookuplist;
			case REFERRAL	: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_referralsource_lookuplist;
			default			: return APPOINTMENT_METADATA_LOOKUP_LIST.appointment_status_lookuplist;
		}
	}

	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return viewList(mapping, form, request, response);
	}

	@SuppressWarnings("unused")
	public ActionForward viewList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		String listName = request.getParameter("listName");
		
		/*
		 * default is the appointment status page. 
		 */
		if(listName == null) 
		{
			listName = "STATUS";
		}
		
		/*
		 * Get all look up lists so the user has an option to change the current list.
		 */
		List<LookupList> lookupLists = lookupListManager.findAllActiveLookupLists(loggedInInfo);
		request.setAttribute("lookupLists", lookupLists);
		
		/*
		 * Get the currently set lookup list from SystemPreferences.
		 */
		ListName listNameEnum = ListName.valueOf(listName.toUpperCase());
		APPOINTMENT_METADATA_LOOKUP_LIST lookupListName = getListReferenceType(listNameEnum);
		LookupList selectedLookupList = getLookupListItemsBySystemPreferenceName(loggedInInfo, lookupListName);
		
		request.setAttribute("selectedLookupList", selectedLookupList);
		request.setAttribute("listName", listNameEnum.name().toLowerCase());

		return mapping.findForward("success");
	}
	
	/**
	 * Set/edit which lookup list will be used for appointment reasons in the system preferences.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unused")
	public ActionForward setList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		String listName = request.getParameter("listName");
		String lookupListSelection = request.getParameter("lookupListSelection");
		
		/*
		 * Set the selected lookup list into the system preferences table. 
		 */
		ListName listNameEnum = ListName.valueOf(listName.toUpperCase());
		APPOINTMENT_METADATA_LOOKUP_LIST lookupListName = getListReferenceType(listNameEnum);
	
		systemPreferenceManager.setPreferenceValueByName(loggedInInfo, lookupListName, lookupListSelection);
		
		/*
		 * Send back to viewList for the result
		 */
		ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward("view"));
		actionRedirect.addParameter("listName", listNameEnum.name().toLowerCase());

		return actionRedirect;
	}
	
	/**
	 * Edits the current reason list with custom colours and/or icons.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unused")
	public ActionForward updateIcon(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		
		String lookupListItemId = request.getParameter("ID");
		String icon = request.getParameter("selectedIcon");
		String listName = request.getParameter("listName");
		
		LookupListItem lookupListItem = null;

		if(lookupListItemId != null && icon != null)
		{
			lookupListItem = lookupListManager.updateLookupListItemIcon(loggedInInfo, Integer.parseInt(lookupListItemId), icon);
		}	

		request.setAttribute("lookupListItem", lookupListItem);
		
		/*
		 * Send back to viewList with the listName parameter set in order to view the result.
		 */
		ListName listNameEnum = ListName.valueOf(listName.toUpperCase());
		ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward("view"));
		actionRedirect.addParameter("listName", listNameEnum.name().toLowerCase());

		return actionRedirect;
	}
	
	@SuppressWarnings("unused")
	public ActionForward updateColour(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		
		String lookupListItemId = request.getParameter("ID");
		String colour = request.getParameter("apptColor");
		String listName = request.getParameter("listName");
		
		LookupListItem lookupListItem = null;
		
		if(lookupListItemId != null && colour != null)
		{
			lookupListItem = lookupListManager.updateLookupListItemColour(loggedInInfo, Integer.parseInt(lookupListItemId), colour);
		}
		
		request.setAttribute("lookupListItem", lookupListItem);
		
		/*
		 * Send back to viewList for the result
		 */
		ListName listNameEnum = ListName.valueOf(listName.toUpperCase());
		ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward("view"));
		actionRedirect.addParameter("listName", listNameEnum.name().toLowerCase());

		return actionRedirect;
	}
	
	private LookupList getLookupListItemsBySystemPreferenceName(LoggedInInfo loggedInInfo, Enum<?> preferenceName) {
		String lookupList = systemPreferenceManager.getPreferenceValueByName(loggedInInfo, preferenceName);
		return lookupListManager.findLookupListByName(loggedInInfo, lookupList);
	}
	
}
