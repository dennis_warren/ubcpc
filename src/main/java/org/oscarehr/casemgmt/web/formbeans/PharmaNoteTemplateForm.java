/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.casemgmt.web.formbeans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.Dxresearch;
import org.oscarehr.common.model.Provider;


/**
 * This is a back form for the Pharmacist Encounter Smart Template.
 * Used with Action class org.oscarehr.casemgmt.web.noteTemplates.PharmaNoteTemplate
 * and JSP PharmaNoteTemplate
 * @author dennis warren @ colcamex
 * 
 */
public class PharmaNoteTemplateForm extends ActionForm implements java.io.Serializable {

	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy");
	private static final String DEFAULT_ENCOUNTER_TYPE = "DTP Assessment";
	
	private List<Drug> medications;
	private List<Allergy> allergies;
	private List<Dxresearch> dxResearch;
	private List<DxResearchTransfer> dxResearchComments;
	private Provider provider;
	private CaseManagementNote caseManagementNote;
	private String encounterType;
	private String encounterTypeHeading;
	private String chiefComplaint;
	private String chiefComplaintHeading;
	private String patientBarriers;
	private String patientBarriersHeading;
	private String assessmentSummary;
	private String assessmentSummaryHeading;
	private String followUpPlan;
	private String followUpPlanHeading;	
	private String currentMedicationsHeading;
	private String knownMedicalConditionsHeading;
	private String allergiesHeading;
	private String dtpAddressed;
	private String dtpAddressedHeading;
	private String dtpToComplete;
	private String dtpToCompleteHeading;

	private String demographicNo;
	private Date timeStamp;

	private Boolean signed = Boolean.FALSE;
	private Boolean includeIssueInNote = Boolean.FALSE;
	
	public PharmaNoteTemplateForm() {
		super();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		
		this.medications = ListUtils.lazyList(new ArrayList<Drug>(), new Factory() {
			public Drug create() {
				return new Drug();
			}
		});
		
		this.dxResearchComments = ListUtils.lazyList(new ArrayList<DxResearchTransfer>(), new Factory() {
			public DxResearchTransfer create() {
				return new DxResearchTransfer();
			}
		});

		setTimeStamp( new Date() );

	}

	public DxResearchTransfer getDxResearchComment(int index) {
		return getDxResearchComments().get(index);
	}
	
	public List<DxResearchTransfer> getDxResearchComments() {
		return dxResearchComments;
	}

	public void setDxResearchComments( List<DxResearchTransfer> dxResearchComments ) {
		this.dxResearchComments = dxResearchComments;
	}

	public Drug getMedication(int index) {
		return getMedications().get(index);
	}

	public List<Drug> getMedications() {
		return medications;
	}

	public void setMedications(List<Drug> medications) {
		this.medications = medications;
	}

	public List<Allergy> getAllergies() {
		return allergies;
	}

	public void setAllergies(List<Allergy> allergies) {
		this.allergies = allergies;
	}

	public List<Dxresearch> getDxResearch() {
		return dxResearch;
	}

	public void setDxResearch(List<Dxresearch> dxResearch) {
		this.dxResearch = dxResearch;
	}

	public String getChiefComplaint() {
		if( chiefComplaint == null ) {
			return "";
		}
		return chiefComplaint;
	}

	public void setChiefComplaint(String chiefComplaint) {
		this.chiefComplaint = chiefComplaint;
	}

	public String getPatientBarriers() {
		if( patientBarriers == null ) {
			return "";
		}
		return patientBarriers;
	}

	public void setPatientBarriers(String patientBarriers) {
		this.patientBarriers = patientBarriers;
	}

	public String getAssessmentSummary() {
		if( assessmentSummary == null ) {
			return "";
		}
		return assessmentSummary;
	}

	public void setAssessmentSummary(String assessmentSummary) {
		this.assessmentSummary = assessmentSummary;
	}

	public String getFollowUpPlan() {
		if( followUpPlan == null ) {
			return "";
		}
		return followUpPlan;
	}

	public void setFollowUpPlan(String followUpPlan) {
		this.followUpPlan = followUpPlan;
	}

	public String getEncounterTypeHeading() {
		if( encounterTypeHeading == null ) {
			return "";
		}
		return encounterTypeHeading;
	}

	public void setEncounterTypeHeading(String encounterTypeHeading) {
		this.encounterTypeHeading = encounterTypeHeading;
	}

	public String getChiefComplaintHeading() {
		if( chiefComplaintHeading == null ) {
			return "";
		}
		return chiefComplaintHeading;
	}

	public void setChiefComplaintHeading(String chiefComplaintHeading) {
		this.chiefComplaintHeading = chiefComplaintHeading;
	}

	public String getPatientBarriersHeading() {
		if( patientBarriersHeading == null ) {
			return "";
		}
		return patientBarriersHeading;
	}

	public void setPatientBarriersHeading(String patientBarriersHeading) {
		this.patientBarriersHeading = patientBarriersHeading;
	}

	public String getAssessmentSummaryHeading() {
		if( assessmentSummaryHeading == null ) {
			return "";
		}
		return assessmentSummaryHeading;
	}

	public void setAssessmentSummaryHeading(String assessmentSummaryHeading) {
		this.assessmentSummaryHeading = assessmentSummaryHeading;
	}

	public String getFollowUpPlanHeading() {
		if( followUpPlanHeading == null ) {
			return "";
		}
		return followUpPlanHeading;
	}

	public void setFollowUpPlanHeading(String followUpPlanHeading) {
		this.followUpPlanHeading = followUpPlanHeading;
	}
	
	public String getCurrentMedicationsHeading() {
		return currentMedicationsHeading;
	}

	public void setCurrentMedicationsHeading(String currentMedicationsHeading) {
		this.currentMedicationsHeading = currentMedicationsHeading;
	}

	public String getKnownMedicalConditionsHeading() {
		return knownMedicalConditionsHeading;
	}

	public void setKnownMedicalConditionsHeading(String knownMedicalConditionsHeading) {
		this.knownMedicalConditionsHeading = knownMedicalConditionsHeading;
	}

	public String getAllergiesHeading() {
		return allergiesHeading;
	}

	public void setAllergiesHeading(String allergiesHeading) {
		this.allergiesHeading = allergiesHeading;
	}

	public String getDtpAddressed() {
		return dtpAddressed;
	}

	public void setDtpAddressed(String dtpAddressed) {
		this.dtpAddressed = dtpAddressed;
	}

	public String getDtpAddressedHeading() {
		return dtpAddressedHeading;
	}

	public void setDtpAddressedHeading(String dtpAddressedHeading) {
		this.dtpAddressedHeading = dtpAddressedHeading;
	}

	public String getDtpToComplete() {
		return dtpToComplete;
	}

	public void setDtpToComplete(String dtpToComplete) {
		this.dtpToComplete = dtpToComplete;
	}

	public String getDtpToCompleteHeading() {
		return dtpToCompleteHeading;
	}

	public void setDtpToCompleteHeading(String dtpToCompleteHeading) {
		this.dtpToCompleteHeading = dtpToCompleteHeading;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}
	
	public String getTimeStampString() {
		return simpleDateFormat.format( getTimeStamp() );
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getEncounterType() {
		if( encounterType == null ) {
			return DEFAULT_ENCOUNTER_TYPE;
		}
		return encounterType;
	}

	public void setEncounterType(String encounterType) {
		this.encounterType = encounterType;
	}

	public String getDemographicNo() {
		return demographicNo;
	}

	public void setDemographicNo(String demographicNo) {
		this.demographicNo = demographicNo;
	}

	public Boolean getSigned() {
		if( signed == null) {
			return Boolean.FALSE;
		}
		return signed;
	}

	public void setSigned(Boolean signed) {
		this.signed = signed;
	}

	public Boolean getIncludeIssueInNote() {
		return includeIssueInNote;
	}

	public void setIncludeIssueInNote(Boolean includeIssueInNote) {
		this.includeIssueInNote = includeIssueInNote;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	private String getMedicationComments() {
		
		StringBuilder medicationComments = new StringBuilder();		
		if( getMedications() != null ) {

			medicationComments.append( getCurrentMedicationsHeading() );
	
			medicationComments.append("\n");
			
			String medicationComment;
			
			for( Drug medication : getMedications() ) {
				medicationComment = medication.getComment();
				if( medicationComment == null ) {
					medicationComment = "";
				}
				medicationComments.append( " *  " + WordUtils.capitalizeFully( medication.getGenericName() ) );
				medicationComments.append(": ");
				medicationComments.append( medicationComment.replaceAll("\\[.*?\\] ?", "") );
				medicationComments.append("\n");
			}
		}
		
		return medicationComments.toString();
	}
	
	private String getDxComments() {
		
		StringBuilder dxComments = new StringBuilder();		
		if( getDxResearchComments()  != null ) {
			dxComments.append( getKnownMedicalConditionsHeading() );
			
			dxComments.append("\n");

			for( DxResearchTransfer dx : getDxResearchComments() ) {

				dxComments.append( " *  " + WordUtils.capitalizeFully( dx.getDescription() ) );
				dxComments.append(": ");
				dxComments.append( dx.getNote() );
				dxComments.append("\n");
			}
		}
		
		return dxComments.toString();
	}

	private String getNotes() {
		
		StringBuilder stamp = new StringBuilder();
		stamp.append("[");
		stamp.append( getTimeStampString() );
		stamp.append(" .: ");
		stamp.append( getEncounterType() );
		stamp.append("] \n");
		
		StringBuilder notes = new StringBuilder();
		
		notes.append( stamp );	
	
		notes.append( getChiefComplaintHeading());
	
		notes.append("\n");
		notes.append( " " + getChiefComplaint());		
		notes.append("\n");
		notes.append("\n");

		notes.append( getDxComments() );		
		notes.append("\n");
				
		notes.append( getMedicationComments() );
		notes.append("\n");	
	
		notes.append( getPatientBarriersHeading() );
		
		notes.append("\n");
		notes.append( " " + getPatientBarriers());
		notes.append("\n");
		notes.append("\n");
		
	
		notes.append( getAssessmentSummaryHeading() );
	
		notes.append("\n");
		notes.append( " " + getAssessmentSummary());
		notes.append("\n");
		notes.append("\n");
		
		notes.append( getDtpAddressedHeading());

		notes.append("\n");
		notes.append(  " " + getDtpAddressed());
		notes.append("\n");
		notes.append("\n");
		
		notes.append( getDtpToCompleteHeading());

		notes.append("\n");
		notes.append(  " " + getDtpToComplete());
		notes.append("\n");
		notes.append("\n");

		notes.append( getFollowUpPlanHeading());

		notes.append("\n");
		notes.append(  " " + getFollowUpPlan());
		notes.append("\n");
		notes.append("\n");
		
		if( getSigned() ) {
			notes.append("[Verified and Signed on ");
			notes.append( getTimeStamp() );
			notes.append( " by " );
			notes.append( getProvider().getFormattedName() );
			notes.append("]");
		}
		
		return notes.toString().trim();
	}

	/**
	 * Returns a new CaseManagement Case Note object that contains all
	 * the properties of this class. 
	 * @return CaseManagementNote
	 */
	public CaseManagementNote getCaseManagementNote() {
		
		if( this.caseManagementNote == null ) {
			
			this.caseManagementNote = new CaseManagementNote();
			this.caseManagementNote.setUuid( UUID.randomUUID().toString() );

		} 
				
/*		String previousHistory = caseManagementNote.getHistory();
		
		if( previousHistory == null ) {
			previousHistory = "";
		}
		
		if( ! previousHistory.isEmpty() ) {
			previousHistory = previousHistory + "\n";
		}*/

		caseManagementNote.setProviderNo( getProvider().getProviderNo() );
		caseManagementNote.setSigning_provider_no( getProvider().getProviderNo() );
		caseManagementNote.setEncounter_type( getEncounterType() );
		caseManagementNote.setDemographic_no( getDemographicNo() + "" );
		caseManagementNote.setNote( getNotes() );
		caseManagementNote.setHistory( "  ----------------History Record----------------  \n"  + getNotes() );
		caseManagementNote.setCreate_date( getTimeStamp() );
		caseManagementNote.setObservation_date( getTimeStamp() );
		caseManagementNote.setUpdate_date( getTimeStamp() );
		caseManagementNote.setSigned( getSigned() );
		caseManagementNote.setIncludeissue( getIncludeIssueInNote() );		
		caseManagementNote.setBilling_code("");
		
		return caseManagementNote;
	}

	public void setCaseManagementNote(CaseManagementNote caseManagementNote) {
		this.caseManagementNote = caseManagementNote;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
