/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */
package org.oscarehr.casemgmt.web.formbeans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.model.*;

public class DrugTherapyProblemFormBean extends ActionForm implements java.io.Serializable  {	
	
	private List<Drug> medications;
	private List<DrugTherapyProblemPriority> priorityList;
	private List<DrugTherapyProblemStatus> statusList;
	private List<DrugTherapyProblemSeverity> severityList;
	private List<DrugTherapyProblemPharmacistAction> pharmacistActionList;
	private List<DrugTherapyProblemProbability> probabilityList;
	private List<DrugTherapyProblemIssue> issueList;
	
	private String demographicNo;
	private List<DrugTherapyProblem> drugTherapyProblems;
	private DrugTherapyProblem drugTherapyProblem;
	private int dtpId;
	private String trackerId;
	private int issueId;
	private Integer eventId;
	private int drugId;
	private String event;
	private int priorityId;
	private int severityId;
	private int probabilityId;
	private int statusId;
	private String[] pharmacistActionId;
	private String DIN;
	private String dateEntered;
	private String dateIdentified;
	private Date dateUpdated;
	private String dateResponse;
	private String comment;

	private boolean hasEvent;
	
	@Override
	@SuppressWarnings("unchecked")
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		
		this.drugTherapyProblems = ListUtils.lazyList( new ArrayList<DrugTherapyProblem>(), new Factory() {
			public DrugTherapyProblem create() {
				return new DrugTherapyProblem();
			}
		});
		
		setDrugId( 0 );
		setTrackerId( null);
		setDemographicNo( null );
		setDtpId( 0 );
		setDIN( null );
		setIssueId( 0 );
		setPriorityId( 0 );
		setStatusId( 0 );
//		setPharmacistActionId(new Integer[0]);
		setProbabilityId( 0 );
		setSeverityId( 0 );
		setComment( null );
		setEvent(null);
		setDateEntered(null);
		setDateIdentified( null );	
		setDateResponse( null );

	}
	
	public DrugTherapyProblem getDrugTherapyProblem(int index) {
		return getDrugTherapyProblems().get(index);
	}

	public List<DrugTherapyProblem> getDrugTherapyProblems() {
		return drugTherapyProblems;
	}

	public void setDrugTherapyProblems(List<DrugTherapyProblem> drugTherapyProblems) {
		this.drugTherapyProblems = drugTherapyProblems;
	}

	public List<DrugTherapyProblemPriority> getPriorityList() {
		return priorityList;
	}

	public void setPriorityList(List<DrugTherapyProblemPriority> priorityList) {
		this.priorityList = priorityList;
	}

	public List<DrugTherapyProblemStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<DrugTherapyProblemStatus> statusList) {
		this.statusList = statusList;
	}

	public List<Drug> getMedications() {
		if(medications == null ) {
			this.medications = new ArrayList<>();
		}
		return medications;
	}
	public void setMedications(List<Drug> medications) {
		this.medications = medications;
	}

	public int getDtpId() {
		return dtpId;
	}

	public void setDtpId(int dtpId) {
		this.dtpId = dtpId;
	}

	public String getTrackerId() {
		return trackerId;
	}

	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}

	public String getDemographicNo() {
		return demographicNo;
	}

	public void setDemographicNo(String demographicNo) {
		this.demographicNo = demographicNo;
	}

	public int getIssueId() {
		return issueId;
	}

	public void setIssueId(int issueId) {
		this.issueId = issueId;
	}

	public int getDrugId() {
		return drugId;
	}

	public void setDrugId(int drugId) {
		this.drugId = drugId;
	}

	public int getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}

	public int getSeverityId() {
		return severityId;
	}

	public void setSeverityId(int severityId) {
		this.severityId = severityId;
	}

	public int getProbabilityId() {
		return probabilityId;
	}

	public void setProbabilityId(int probabilityId) {
		this.probabilityId = probabilityId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getDIN() {
		return DIN;
	}

	public void setDIN(String dIN) {
		DIN = dIN;
	}

	public String getDateEntered() {
		return dateEntered;
	}

	public void setDateEntered(String dateEntered) {
		this.dateEntered = dateEntered;
	}

	public String getDateIdentified() {
		return dateIdentified;
	}

	public void setDateIdentified(String dateIdentified) {
		this.dateIdentified = dateIdentified;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getDateResponse() {
		return dateResponse;
	}

	public void setDateResponse( String dateResponse ) {
		this.dateResponse = dateResponse;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public DrugTherapyProblem getDrugTherapyProblem() {
		return drugTherapyProblem;
	}

	public void setDrugTherapyProblem(DrugTherapyProblem drugTherapyProblem) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
		Integer drugId = drugTherapyProblem.getDrugId();
		if( drugId == null ) {
			drugId = 0;
		}
		setDrugId( drugId );
		setTrackerId( drugTherapyProblem.getTrackerId() );
		setDemographicNo( drugTherapyProblem.getDemographicNo() + "" );
		setDtpId( drugTherapyProblem.getId() );
		setDIN( drugTherapyProblem.getDIN() );
		setIssueId( drugTherapyProblem.getIssueId() );
		setEventId(drugTherapyProblem.getEventId());
		setPriorityId( drugTherapyProblem.getPriorityId() );
		setStatusId( drugTherapyProblem.getStatusId() );
		setCtlPharmacistActionId( drugTherapyProblem.getDrugTherapyProblemPharmacistAction() );
		setProbabilityId( drugTherapyProblem.getProbabilityId() );
		setSeverityId( drugTherapyProblem.getSeverityId() );
		setEvent(drugTherapyProblem.getDrugTherapyProblemEvent().getEvent());
		setComment( drugTherapyProblem.getDrugTherapyProblemComment().getComment() );	
		Date dateEntered = drugTherapyProblem.getDateEntered();
		Date dateIdentified = drugTherapyProblem.getDateIdentified();
		Date dateResponse = drugTherapyProblem.getDateResponse();
		Date dateUpdated = drugTherapyProblem.getDateUpdated();
		
		// date updated is intentionally set as a timestamp
		setDateUpdated( dateUpdated );
		
		if( dateEntered != null ) {
			setDateEntered( dateFormat.format(dateEntered) );
		}
		
		if( dateIdentified != null ) {
			setDateIdentified( dateFormat.format(dateIdentified) );
		}
		
		if( dateResponse != null ) {	
			setDateResponse( dateFormat.format(dateResponse) );
		}

		this.drugTherapyProblem = drugTherapyProblem;
	}

	public List<DrugTherapyProblemSeverity> getSeverityList() {
		return severityList;
	}

	public void setSeverityList(List<DrugTherapyProblemSeverity> severityList) {
		this.severityList = severityList;
	}

	public List<DrugTherapyProblemPharmacistAction> getPharmacistActionList() {
		return pharmacistActionList;
	}

	public void setPharmacistActionList(List<DrugTherapyProblemPharmacistAction> pharmacistActionList) {
		this.pharmacistActionList = pharmacistActionList;
	}

	public List<DrugTherapyProblemIssue> getIssueList() {
		return issueList;
	}

	public void setIssueList(List<DrugTherapyProblemIssue> issueList) {
		this.issueList = issueList;
	}

	public List<DrugTherapyProblemProbability> getProbabilityList() {
		return probabilityList;
	}

	public void setProbabilityList(List<DrugTherapyProblemProbability> probabilityList) {
		this.probabilityList = probabilityList;
	}

	public void setPharmacistActionId(String[] pharmacistActionId) {
		this.pharmacistActionId = pharmacistActionId;
	}

	public String[] getPharmacistActionId() {
		return this.pharmacistActionId;
	}
	public List<CtlDrugTherapyProblemPharmacistAction> getCtlPharmacistActionId() {
		List<CtlDrugTherapyProblemPharmacistAction> ctlDrugTherapyProblemPharmacistActionList = Collections.emptyList();
		if(pharmacistActionId != null && pharmacistActionId.length > 0) {
			ctlDrugTherapyProblemPharmacistActionList = new ArrayList<>(pharmacistActionId.length);
			for(String pharmacistAction : pharmacistActionId) {
				CtlDrugTherapyProblemPharmacistAction ctlDrugTherapyProblemPharmacistAction = new CtlDrugTherapyProblemPharmacistAction();
				ctlDrugTherapyProblemPharmacistAction.setDtpId(getDtpId());
				ctlDrugTherapyProblemPharmacistAction.setDtpActionId(Integer.parseInt(pharmacistAction));
				ctlDrugTherapyProblemPharmacistActionList.add(ctlDrugTherapyProblemPharmacistAction);
			}
		}
		return ctlDrugTherapyProblemPharmacistActionList;
	}

	public void setCtlPharmacistActionId(List<CtlDrugTherapyProblemPharmacistAction> ctlDrugTherapyProblemPharmacistActionList) {
		List<String> pharmacistActionIdList = new ArrayList<>(ctlDrugTherapyProblemPharmacistActionList.size());
		for(CtlDrugTherapyProblemPharmacistAction ctlDrugTherapyProblemPharmacistAction : ctlDrugTherapyProblemPharmacistActionList) {
			pharmacistActionIdList.add(ctlDrugTherapyProblemPharmacistAction.getDtpActionId() + "");
		}
		setPharmacistActionId( pharmacistActionIdList.toArray( new String[pharmacistActionIdList.size()] ) );
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public boolean isHasEvent() {
		return hasEvent;
	}

	public void setHasEvent(boolean hasEvent) {
		this.hasEvent = hasEvent;
	}
}
