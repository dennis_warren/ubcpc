/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.casemgmt.web.noteTemplates;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.model.ProgramProvider;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.casemgmt.web.formbeans.DxResearchTransfer;
import org.oscarehr.casemgmt.web.formbeans.PharmaNoteTemplateForm;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.Dxresearch;
import org.oscarehr.managers.AllergyManager;
import org.oscarehr.managers.DxResearchManager;
import org.oscarehr.managers.PrescriptionManager;
import org.oscarehr.managers.ProgramManager2;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;


/**
 * PharmaNoteTemplate action class
 * depends on: PharmaNoteTemplateForm
 * @author dennis warren @ colcamex
 *
 */
public class PharmaNoteTemplate extends DispatchAction {
	
	private PrescriptionManager prescriptionManager = SpringUtils.getBean( PrescriptionManager.class );
	private DxResearchManager dxResearchManager = SpringUtils.getBean( DxResearchManager.class );
	private AllergyManager allergyManager = SpringUtils.getBean( AllergyManager.class );
	private ProgramManager2 programManager = SpringUtils.getBean( ProgramManager2.class );
	
	public ActionForward fetch(ActionMapping mapping, 
			ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {
		
		PharmaNoteTemplateForm pharmaForm = (PharmaNoteTemplateForm) form;
		String demographicNo = request.getParameter("demographicNo");

		if( demographicNo != null ) {
			demographicNo = demographicNo.trim();
			pharmaForm.setDemographicNo( demographicNo );
		}

		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		List<Drug> medications = prescriptionManager.getActiveMedications( loggedInInfo, demographicNo );
		List<Allergy> allergies = allergyManager.getActiveAllergies( loggedInInfo, Integer.parseInt(demographicNo) );
		
		List<Dxresearch> dxResearch = dxResearchManager.findAllActiveByDemographicNoWithCodeDescription( loggedInInfo, Integer.parseInt(demographicNo) );

		List<DxResearchTransfer> dxResearchComments = new ArrayList<DxResearchTransfer>();
		DxResearchTransfer description;
		for(Dxresearch dx : dxResearch) {
			description = new DxResearchTransfer(); 
			description.setDescription( dx.getCodeDescription() + "(" + dx.getDxresearchCode() + ")" );
			dxResearchComments.add(description);
		}
		pharmaForm.setProvider( loggedInInfo.getLoggedInProvider() );
		pharmaForm.setAllergies(allergies);
		pharmaForm.setMedications(medications);
		pharmaForm.setDxResearchComments(dxResearchComments);

		return mapping.findForward("success");
	}
	
	public ActionForward save(ActionMapping mapping, 
			ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {
		
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession( request );
		PharmaNoteTemplateForm pharmaForm = ( PharmaNoteTemplateForm ) form;
		CaseManagementManager cmm = SpringUtils.getBean( CaseManagementManager.class );
		ProgramProvider programProvider = programManager.getCurrentProgramInDomain(loggedInInfo, loggedInInfo.getLoggedInProviderNo());
		String programNumber = "";
		String programRole = "";

		if( programProvider != null ) {
			programNumber = programProvider.getProgramId() + "";
			programRole = programProvider.getRoleId() + "";
		}
				
		// updateMedications( loggedInInfo, pharmaForm.getMedications() );
		
		pharmaForm.setProvider( loggedInInfo.getLoggedInProvider() );		
		pharmaForm.getCaseManagementNote().setProgram_no( programNumber );
		pharmaForm.getCaseManagementNote().setReporter_caisi_role( programRole );		
		pharmaForm.getCaseManagementNote().setReporter_program_team("0");

		cmm.saveNoteSimple( pharmaForm.getCaseManagementNote() );
	
		return mapping.findForward("success");
	}
	
	/*private void updateMedications( LoggedInInfo loggedInInfo, List<Drug> medicationList ) {
		Drug drug;
		String medicationComment;
		String drugComment;
		for( Drug medication : medicationList ) {
			
			drug = prescriptionManager.findDrugById( loggedInInfo, medication.getId() );
			medicationComment = medication.getComment();
			drugComment = drug.getComment();
			if( drugComment == null ){
				drugComment = "";
			}
			
			if( medicationComment != null && ! medicationComment.equals( drugComment ) ) {
				drug.setComment( drugComment + "[" + new Date() + ": DTP] " +  medicationComment );				
				prescriptionManager.saveModifiedDrug(loggedInInfo, drug);
			}

		}
	}*/

}
