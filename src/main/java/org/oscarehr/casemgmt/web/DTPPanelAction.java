/**
 * Copyright (c) 2015-2019. The Pharmacists Clinic, Faculty of Pharmaceutical Sciences, University of British Columbia. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * The Pharmacists Clinic
 * Faculty of Pharmaceutical Sciences
 * University of British Columbia
 * Vancouver, British Columbia, Canada
 */

package org.oscarehr.casemgmt.web;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.casemgmt.web.formbeans.DrugTherapyProblemFormBean;
import org.oscarehr.common.model.DrugTherapyProblem;
import org.oscarehr.common.model.DrugTherapyProblemEvent;
import org.oscarehr.common.web.JSONAction;
import org.oscarehr.managers.DrugTherapyProblemManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

public class DTPPanelAction extends JSONAction {

	private static DrugTherapyProblemManager drugTherapyProblemManager = SpringUtils.getBean( DrugTherapyProblemManager.class );
	
	@SuppressWarnings("unused")
	public ActionForward fetchAll(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {
		
		String demographicNo = request.getParameter("demographicNo");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		DrugTherapyProblemFormBean drugTherapyProblemFormBean = (DrugTherapyProblemFormBean) form;
		drugTherapyProblemFormBean.setDemographicNo( demographicNo );

		drugTherapyProblemManager.setBean( loggedInInfo, drugTherapyProblemFormBean );

		List<DrugTherapyProblem> drugTherapyProblems = drugTherapyProblemManager.findAllDTPByDemographicNo(loggedInInfo, Integer.parseInt( demographicNo ) );
		drugTherapyProblemFormBean.setDrugTherapyProblems( drugTherapyProblems );

		return mapping.findForward("fetchAll");
	}
	
	@SuppressWarnings("unused")
	public ActionForward fetchSingle(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {

		String dtpId = request.getParameter("dtpId");
		
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		DrugTherapyProblemFormBean drugTherapyProblemFormBean = (DrugTherapyProblemFormBean) form;
		DrugTherapyProblem drugTherapyProblem = drugTherapyProblemManager.findDTPById( loggedInInfo, Integer.parseInt(dtpId) );
		int demographicNo = drugTherapyProblem.getDemographicNo();
		List<DrugTherapyProblem> drugTherapyProblemHistory = drugTherapyProblemManager.findDTPandHistoryByTrackerId(loggedInInfo, demographicNo, drugTherapyProblem.getTrackerId() );
		
		drugTherapyProblemFormBean.setDemographicNo( demographicNo  + "");

		drugTherapyProblemManager.setBean( loggedInInfo, drugTherapyProblemFormBean );

		drugTherapyProblemFormBean.setDrugTherapyProblem( drugTherapyProblem );
		drugTherapyProblemFormBean.setDrugTherapyProblems( drugTherapyProblemHistory );
		
		return mapping.findForward("fetchSingle");
	}
	
	@SuppressWarnings("unused")
	public ActionForward create( ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {
		
		String demographicNo = request.getParameter("demographicNo");
		DrugTherapyProblemFormBean drugTherapyProblemFormBean = (DrugTherapyProblemFormBean) form;
		drugTherapyProblemFormBean.setDemographicNo( demographicNo );

		drugTherapyProblemManager.setBean( LoggedInInfo.getLoggedInInfoFromSession(request), drugTherapyProblemFormBean);

		return mapping.findForward("create");
	}
	
	@SuppressWarnings("unused")
	public void update(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		DrugTherapyProblemFormBean drugTherapyProblemFormBean = (DrugTherapyProblemFormBean) form;
		DrugTherapyProblem drugTherapyProblem = new DrugTherapyProblem();
		
		drugTherapyProblem.setTrackerId( drugTherapyProblemFormBean.getTrackerId() );

		drugTherapyProblemManager.saveBean( loggedInInfo, drugTherapyProblem, drugTherapyProblemFormBean );
	}
	
	@SuppressWarnings("unused")
	public void save(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) {

		DrugTherapyProblemFormBean drugTherapyProblemForm = (DrugTherapyProblemFormBean) form;
		DrugTherapyProblem drugTherapyProblem = new DrugTherapyProblem();
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);

		Date today = new Date(System.currentTimeMillis());
		drugTherapyProblem.setTrackerId( new UUID( today.getTime(), drugTherapyProblemForm.getIssueId() ).toString() );
		drugTherapyProblemManager.saveBean( loggedInInfo, drugTherapyProblem, drugTherapyProblemForm );
	}

	public void searchEvent(ActionMapping mapping, ActionForm form, HttpServletRequest request,
					   HttpServletResponse response) {
		String keyword = request.getParameter("term");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		List<DrugTherapyProblemEvent> drugTherapyProblemEvents = drugTherapyProblemManager.searchEvent(loggedInInfo, keyword);
		jsonResponse(response, drugTherapyProblemEvents);
	}

}
