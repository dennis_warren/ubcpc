package org.oscarehr.ws;
/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import oscar.oscarLab.FileUploadCheck;
import oscar.oscarLab.ca.all.upload.HandlerClassFactory;
import oscar.oscarLab.ca.all.upload.handlers.MessageHandler;

@WebService
@Component
public class HL7LabUploadWs extends AbstractWs {

		@Autowired
		private ProviderDao providerDao;
	
		public HL7LabUploadWs() {
			// default constructor
		}
		
		/**
		 * This is a wrapper method for a web service interface.
		 * Sends file path and path data for entry into a log database.
		 * 
		 * @param savedPath
		 * @return a file id to reference the file later.
		 * @throws Exception
		 */
		@WebMethod(action = "http://ws.oscarehr.org/saveHL7") 
		public int saveHL7(@WebParam(name="savedPath") String savedPath, 
				@WebParam(name="providerNumber")  String providerNumber) throws Exception {
			
			//confirm the save. Scripts borrowed from Oscar's insideLabUploadAction class. 
			File file = new File(savedPath);
			InputStream inputStream = new FileInputStream(file);
		
			int fileId = FileUploadCheck.addFile(
					file.getName(), 
					inputStream, 
					providerNumber
			);
			
			inputStream.close();
			inputStream = null;
		
			return fileId;
		
		}
		
		/**
		 * Sends the command for Oscar to parse the downloaded HL7 file.
		 * @param className
		 * @param savedPath
		 * @param fileId
		 * @param labType
		 * @return
		 */
		@WebMethod
		public boolean parseHL7(@WebParam(name="className")  String className, 
				@WebParam(name="savedPath")  String savedPath, 
				@WebParam(name="fileId")  int fileId, 
				@WebParam(name="labType") String labType) {
			
			MessageHandler msgHandler = HandlerClassFactory.getHandler(labType);
			LoggedInInfo loggedInInfo = new LoggedInInfo();
			loggedInInfo.setLoggedInProvider(providerDao.getProvider("-1"));
			
			if(msgHandler.parse(loggedInInfo, className, savedPath, fileId, "localhost") != null) {
				return true;
			}
			
			return false;
		}

}

