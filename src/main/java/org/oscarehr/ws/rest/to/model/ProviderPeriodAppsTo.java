/**
 * Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


package org.oscarehr.ws.rest.to.model;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.oscarehr.common.model.Appointment;

public class ProviderPeriodAppsTo {
	
	private int id;
	private String providerNo;
	private Date appointmentDate;
	private Integer demographicNo;
	private String notes;
	private String location;
	private String resources;
	private String status;
	private String lastUpdateUser;
	private Long updateDatetime;
	private Date createDateTime;
	private String creator;
	private String name;
	private Date startTime;
	private Date endTime;
	private String reason;
	private String type;
	private int duration;	
	private String urgency;
	private Integer reasonCode;
	private String importedStatus;
	
	public ProviderPeriodAppsTo() {
		super();
	}

	public ProviderPeriodAppsTo(Appointment a) {
		super();
		this.id = a.getId();
		this.providerNo = a.getProviderNo();
	
		Calendar appointmentDate = Calendar.getInstance(); 
		appointmentDate.setTime(a.getAppointmentDate());
		this.appointmentDate = appointmentDate.getTime();

		this.demographicNo = a.getDemographicNo();
		this.notes = a.getNotes();
		this.location = a.getLocation();
		this.resources = a.getResources();
		this.status = a.getStatus();
		this.lastUpdateUser = a.getLastUpdateUser();
		this.updateDatetime = a.getUpdateDateTime().getTime()/1000;
		this.createDateTime = a.getCreateDateTime();
		this.creator = a.getCreator();
		this.name = a.getName();
		
		Long diff = Math.abs(a.getEndTime().getTime() - a.getStartTime().getTime());	
		diff = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);			
		this.duration = diff.intValue() + 1;
		
		Calendar startTime = Calendar.getInstance();		
		startTime.setTime(this.appointmentDate);
		Calendar timeMerge = Calendar.getInstance();
		timeMerge.setTime(a.getStartTime());
		
		startTime.set(Calendar.HOUR_OF_DAY, timeMerge.get(Calendar.HOUR_OF_DAY));
		startTime.set(Calendar.MINUTE, timeMerge.get(Calendar.MINUTE));
		startTime.set(Calendar.SECOND, 00);
		
		this.startTime = startTime.getTime();
		
		Calendar endtime = Calendar.getInstance();
		endtime.setTime(this.startTime);	
		endtime.add(Calendar.MINUTE, this.duration);
		this.endTime = endtime.getTime();
		
		this.reason = a.getReason();
		this.type = a.getType();
		this.urgency = a.getUrgency();
		this.reasonCode = a.getReasonCode();
		this.importedStatus = a.getImportedStatus();
	}

	public String getProviderNo() {
		return providerNo;
	}

	public void setProviderNo(String providerNo) {
		this.providerNo = providerNo;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public Integer getDemographicNo() {
		return demographicNo;
	}

	public void setDemographicNo(Integer demographicNo) {
		this.demographicNo = demographicNo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getResources() {
		return resources;
	}

	public void setResources(String resources) {
		this.resources = resources;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(String lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	public Long getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(Long updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public Integer getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(Integer reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getImportedStatus() {
		return importedStatus;
	}

	public void setImportedStatus(String importedStatus) {
		this.importedStatus = importedStatus;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Override
	public String toString() {
		return "ProviderPeriodAppsTo [id=" + id + ", providerNo=" + providerNo + ", appointmentDate=" + appointmentDate + ", demographicNo=" + demographicNo + ", notes=" + notes + ", location=" + location + ", resources=" + resources + ", status=" + status + ", lastUpdateUser=" + lastUpdateUser + ", updateDatetime=" + updateDatetime + ", createDateTime=" + createDateTime + ", creator=" + creator + ", name=" + name + ", startTime=" + startTime + ", endTime=" + endTime + ", reason=" + reason + ", type="
		        + type + ", duration=" + duration + ", urgency=" + urgency + ", reasonCode=" + reasonCode + ", importedStatus=" + importedStatus + "]";
	}
	
}
