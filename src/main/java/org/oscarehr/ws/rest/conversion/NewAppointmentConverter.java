/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest.conversion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.ws.rest.to.model.NewAppointmentTo1;

/**
 * Conversion from the web's mininal new appt. This converter adds some defaults.
 * 
 * @author marc
 *
 */
public class NewAppointmentConverter extends AbstractConverter<Appointment, NewAppointmentTo1> {

	private DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);

	@Override
	public Appointment getAsDomainObject(LoggedInInfo loggedInInfo, NewAppointmentTo1 t) throws ConversionException {
		Appointment d = new Appointment();

		String startTime = t.getStartTime();
		Long appointmentDateMillis = null;	
		Long startTimeMillis = null;
		String format = null;
		String appointmentDate = t.getAppointmentDate();
		int duration = t.getDuration();
		
		if(startTime != null) 
		{
			try {
				startTimeMillis = Long.parseLong(startTime);
			} catch (NumberFormatException e) {
				format = "yyyy-MM-dd HH:mm";
			}
		} 
		
		if(appointmentDate != null) 
		{
			try {
				appointmentDateMillis = Long.parseLong(appointmentDate);
			} catch (NumberFormatException e) {
				format = "yyyy-MM-dd hh:mm a";
			}
		}

		if(startTimeMillis != null && appointmentDateMillis != null) 
		{
			d.setAppointmentDate(new Date(appointmentDateMillis));
			d.setStartTime(new Date(startTimeMillis));
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(startTimeMillis);
			calendar.add(Calendar.MINUTE, duration);
			calendar.add(Calendar.MINUTE, -1);
			d.setEndTime(calendar.getTime());
		}
		else if(format != null)
		{
			SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
	
			try {
				Date date = dateFormatter.parse(appointmentDate + " " + (startTime != null ? startTime : t.getStartTime12hWithMedian()));
				d.setAppointmentDate(date);
				d.setStartTime(date);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.MINUTE, duration);
				cal.add(Calendar.MINUTE, -1);
				d.setEndTime(cal.getTime());
	
			} catch (ParseException e) {
				MiscUtils.getLogger().warn("Cannot parse new appointment dates");
				throw new ConversionException("Could not parse date/times of appointment. please check format");
			}
		} 
		else 
		{
			throw new ConversionException("Could not parse date/times of appointment. please check format");
		}
		
		// look for the patient this appointment will be assigned to.  note: 0 demographic number is a system appointment. For instance: DO NOT BOOK blocks				
		Integer demographicNo = t.getDemographicNo();
		Demographic demographic = null;
		
		if(demographicNo != 0) {
			demographic = demographicDao.getDemographicById(demographicNo);
			if (demographic == null) {
				throw new ConversionException("Unable to find patient");
			}
		}
		
		String demographicFormattedName = t.getName();
		
		if(demographic != null) {
			demographicFormattedName = demographic.getFormattedName();
		}
	
		d.setName(demographicFormattedName);
		d.setCreateDateTime(new Date());
		d.setCreator(loggedInInfo.getLoggedInProviderNo());
		d.setDemographicNo(demographicNo);
		d.setLastUpdateUser(loggedInInfo.getLoggedInProviderNo());
		d.setLocation(t.getLocation());
		d.setNotes(t.getNotes());
		d.setProgramId(0);
		d.setProviderNo(t.getProviderNo());
		d.setReason(t.getReason());
		d.setRemarks("");
		d.setResources(t.getResources());
		d.setStatus(t.getStatus());
		d.setType(t.getType());
		d.setUrgency(t.getUrgency());
		d.setUpdateDateTime(d.getCreateDateTime());
		d.setReasonCode(t.getReasonCode());
		d.setImportedStatus(t.getImportedStatus());

		return d;
	}

	@Override
	public NewAppointmentTo1 getAsTransferObject(LoggedInInfo loggedInInfo, Appointment d) throws ConversionException {
		return null;
	}

}
