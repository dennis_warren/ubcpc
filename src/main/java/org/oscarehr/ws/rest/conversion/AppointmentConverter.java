/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest.conversion;


import java.util.Calendar;

import java.util.concurrent.TimeUnit;

import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.ws.rest.to.model.AppointmentTo1;
import org.springframework.beans.BeanUtils;

public class AppointmentConverter extends AbstractConverter<Appointment, AppointmentTo1> {

	private boolean includeDemographic;
	private boolean includeProvider;
	
	private DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	private ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	
	public AppointmentConverter() {
		
	}
	
	public AppointmentConverter(boolean includeDemographic, boolean includeProvider) {
		this.includeDemographic = includeDemographic;
		this.includeProvider = includeProvider;
	}
	
	@Override
	public Appointment getAsDomainObject(LoggedInInfo loggedInInfo, AppointmentTo1 t) throws ConversionException {
		Appointment d = new Appointment();		
		BeanUtils.copyProperties(t, d);
		
		d.setLastUpdateUser(loggedInInfo.getLoggedInProviderNo());

		Calendar calendtime = Calendar.getInstance();
		calendtime.setTime(d.getEndTime());
		calendtime.add(Calendar.MINUTE, -1);
		d.setEndTime(calendtime.getTime());
		
		return d;
    }

	@Override
    public AppointmentTo1 getAsTransferObject(LoggedInInfo loggedInInfo, Appointment a) throws ConversionException {
	   AppointmentTo1 t = new AppointmentTo1();
	   	   
		t.setId(a.getId());
		t.setProviderNo(a.getProviderNo());
	
		Calendar appointmentDate = Calendar.getInstance(); 
		appointmentDate.setTime(a.getAppointmentDate());
		t.setAppointmentDate(appointmentDate.getTime());

		t.setDemographicNo(a.getDemographicNo());
		t.setNotes(a.getNotes());
		t.setLocation(a.getLocation());
		t.setResources(a.getResources());
		t.setStatus(a.getStatus());
		t.setLastUpdateUser(a.getLastUpdateUser());
		t.setCreateDateTime(a.getCreateDateTime());
		t.setCreator(a.getCreator());
		t.setName(a.getName());
		
		Long diff = Math.abs(a.getEndTime().getTime() - a.getStartTime().getTime());	
		diff = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);			
		t.setDuration(diff.intValue() + 1);
		
		Calendar startTime = Calendar.getInstance();		
		startTime.setTime(t.getAppointmentDate());
		Calendar timeMerge = Calendar.getInstance();
		timeMerge.setTime(a.getStartTime());
		
		startTime.set(Calendar.HOUR_OF_DAY, timeMerge.get(Calendar.HOUR_OF_DAY));
		startTime.set(Calendar.MINUTE, timeMerge.get(Calendar.MINUTE));
		startTime.set(Calendar.SECOND, 00);
		
		t.setStartTime(startTime.getTime());
		
		Calendar endtime = Calendar.getInstance();
		endtime.setTime(t.getStartTime());	
		endtime.add(Calendar.MINUTE, t.getDuration());
		t.setEndTime(endtime.getTime());
		
		t.setReason(a.getReason());
		t.setType(a.getType());
		t.setUrgency(a.getUrgency());
		t.setReasonCode(a.getReasonCode());
		t.setImportedStatus(a.getImportedStatus());
	   
	   if(includeDemographic && t.getDemographicNo() > 0) {
		   t.setDemographic(demographicDao.getDemographicById(t.getDemographicNo()));
	   }
	   
	   if(includeProvider && t.getProviderNo() != null) {
		   t.setProvider(providerDao.getProvider(t.getProviderNo()));
	   }
	  
	   return t;
    }

	
}
